@extends('panel.layouts.apps')
@include('panel.faq.submenu')
@section('content')
<link rel="stylesheet" href={!! asset("jsUpload/css/jquery.fileupload.css") !!}>
<link rel="stylesheet" href={!! asset("jsUpload/css/jquery.fileupload-ui.css") !!}>
@if ($errors->any())
<div class="alert alert-danger">
    @foreach ($errors->all() as $error)
    <ul>
        <li>{{ $error }}</li>
    </ul>
    @endforeach
</div>
@endif
<form class="kt-form kt-form--label-right row" id="f_faq" action="{{ route('faq.store') }}" method="POST"
    enctype="multipart/form-data">
    {{csrf_field()}}
    <div class="col-12">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body">
                <div class="form-group form-group-last">
                    <div class="alert alert-secondary" role="alert">
                        <div class="alert-icon"><i class="flaticon-add kt-font-brand"></i></div>
                        <div class="alert-text">
                            Tambah Faq
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-2">Pertanyaan *</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="" id="title" name="title"
                            placeholder="Pertanyaan">
                    </div>
                </div>
 

                <div class="form-group row">
                    <label class="col-form-label col-2">Jawaban</label>
                    <div class="col-10">
                        <textarea rows="4" class="form-control" name="answer"
                            placeholder="Jawaban"></textarea>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button class="btn btn-success" type="submit">Submit</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 
</form>
@endsection
 