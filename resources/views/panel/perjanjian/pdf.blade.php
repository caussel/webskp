<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('mazer') }}/assets/css/main/app.css">
    <link href="{!! asset('/front/css/bootstrap.min.css') !!}" rel="stylesheet" type="text/css" media="all" />
    <style>
        @page {
            size: A4;
            margin: 0;
        }
        body {
            background-color: #ffffff;
        }
    </style>
    <title>Rekap Data Perjanjian</title>
</head>

<body onload="window.print()">

    <div class="p-4">
        <div class="row">
            <div class="col-md-12 text-center">
                <h4 class="text-capitalize">Rekap Data Perjanjian</h4>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
               <div class="container-fluid">
                <table class="table table-bordered w-100">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Kartu Identitas</th>
                            <th>Alamat</th>
                            <th>Tanggal</th>
                            <th>Jam</th>
                            <th>Tujuan</th>
                            <th>Komentar</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $item)
                        <tr>
                            <td>{{ $item->nama }}</td>
                            <td><img src="{{ asset('files/'. $item->kartu_identitas) }}" alt="" width="100"></td>
                            <td>{{ $item->alamat }}</td>
                            <td>{{ $item->tanggal }}</td>
                            <td>{{ $item->jam }}</td>
                            <td>{{ $item->tujuan }}</td>
                            <td>{{ $item->komentar ?? '-' }}</td>
                        </tr>
                        @endforeach
            
                    </tbody>
                </table>
               </div>
            </div>
        </div>
    </div>

 
</body>
</html>