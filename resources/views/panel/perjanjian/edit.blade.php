@extends('panel.layouts.apps')
@section('content')
<link rel="stylesheet" href={!! asset("jsUpload/css/jquery.fileupload.css") !!}>
<link rel="stylesheet" href={!! asset("jsUpload/css/jquery.fileupload-ui.css") !!}>
@if ($errors->any())
<div class="alert alert-danger">
    @foreach ($errors->all() as $error)
    <ul>
        <li>{{ $error }}</li>
    </ul>
    @endforeach
</div>
@endif
<form class="kt-form kt-form--label-right row" id="f_perjanjian" action="{{ route('perjanjian.update',$data->id) }}" method="POST"
    enctype="multipart/form-data">
    {{csrf_field()}}
    @method('patch')
    <div class="col-8">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body">
                <div class="form-group form-group-last">
                    <div class="alert alert-secondary" role="alert">
                        <div class="alert-icon"><i class="flaticon-add kt-font-brand"></i></div>
                        <div class="alert-text">
                            Konfirmasi Perjanjian
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-2">Nama *</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="{{ $data->nama }}" id="judul" name="judul"
                            placeholder="Judul Data Indeks" readonly disabled>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-2">Tanggal *</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="{{ $data->tanggal }}" id="judul" name="judul"
                            placeholder="Judul Data Indeks" readonly disabled>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-2">Jam *</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="{{ $data->jam }}" id="judul" name="judul"
                            placeholder="Judul Data Indeks" readonly disabled>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-2">Tujuan *</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="{{ $data->tujuan }}" id="judul" name="judul"
                            placeholder="Judul Data Indeks" readonly disabled>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-2">Alamat</label>
                    <div class="col-10">
                        <textarea rows="4" class="form-control" name="deskripsi"
                        placeholder="Deskripsi" readonly disabled>{{ $data->alamat }}</textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-2">Komentar</label>
                    <div class="col-10">
                        <textarea rows="4" class="form-control" name="komentar"
                        placeholder="Komentar"></textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-2">Status</label>
                    <div class="col-10">
                       <select name="status" id="" class="form-control">
                        <option value="0">Belum Dikonfirmasi</option>
                        <option value="1">Konfirmasi</option>
                       </select>
                    </div>
                </div>
                
            </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button class="btn btn-success" type="submit">Submit</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Kartu Identitas
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="form-group row">
                    <div class="col-12">
                        <div class="preview-pic">
                            <img src="{{ asset('files/'. $data->kartu_identitas) }}" alt="" width="100%">  
                        </div>
                        <hr />
                    </div>
                </div>
                {{-- <div class="form-group">
                    <textarea class="form-control" name="ket_gambar" id="ket_gambar" cols="30" rows="3" placeholder="Keterangan Gambar"></textarea>
                </div> --}}
            </div>
        </div>
   
    </div>
</form>
@endsection
@push('script')
<script src="{!! asset("/js/jquery-ui.js") !!}"></script>
<script src="{!! asset("/js/jquery.fileupload.js") !!}"></script>

<script>
    $(document).on('change', '#position', function(e) {
        let posVal = $('#position').val();
        switch (posVal) {
            case "1":
                $('.ordering').addClass('d-none');
                break;
            case "2":
                $('.ordering').addClass('d-none');
                break;
            default:
                $('.ordering').removeClass('d-none');
                break;
        }
    });
    $(document).ready(function() {
        $("#f_iklan").validate({
            rules: {
                title: {
                    required: !0
                },
                position: {
                    required: !0
                }
            },
            errorPlacement: function(e, r) {
                var i = r.closest(".input-group");
                i.length ? i.after(e.addClass("invalid-feedback")) : r.after(e.addClass(
                    "invalid-feedback"))
            },
            invalidHandler: function(e, r) {
                KTUtil.scrollTop();
            }
        });
    });
    $(function() {
        $('#fileupload').fileupload({
            url: '{!! route("media.ajaxstore") !!}',
            dataType: 'json',
            done: function(e, data) {
                var dataRes = data.result;
                if (dataRes.status == 1) {
                    $('.uploadnew').addClass('d-none');
                    $('.changeupload').removeClass('d-none');
                    $('.preview-pic').html('<img width="100%" src="{!! url("files") !!}/' + dataRes
                        .path + '" />');
                    $('fileinput-button').html('Ganti Gambar');
                    $('#imageIdnews').val(dataRes.imageId);
                    $('.pilihgambar').addClass('d-none');
                    $('.hapusgambar').removeClass('d-none');
                    toastr.success(dataRes.message, "Success");
                } else {
                    toastr.error(dataRes.message, "Failed");
                }
            }
        });
    });
    $(document).on('click', '.pilihgambar', function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var $modal = $('#modal-full');
        $modal.find(".modal-title").html("Pilih Gambar");
        $modal.find(".modal-body").html("");
        $.ajax({
            url: "{{ route('media.modal') }}",
            type: "GET",
            data: {},
            success: function(e) {
                $modal.find(".modal-body").html(e);
            }
        });
        $modal.find(".modal-footer").html('');
        $modal.modal('show');
    });
    $(document).on('click', '.choosepicture', function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var $modal = $('#modal-full');
        var id_gambar = $(this).attr('id-gambar');
        var path_gambar = $(this).attr('path-gambar');
        $('.preview-pic').html('<img width="100%" src="{!! url("files") !!}/' + path_gambar + '" />');
        $('#imageIdnews').val(id_gambar);
        $('.pilihgambar').addClass('d-none');
        $('.hapusgambar').removeClass('d-none');
        $modal.modal('hide');
    });
    $(document).on('click', '.hapusgambar', function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        $('.preview-pic').html('');
        $('#imageIdnews').val('');
        $('.pilihgambar').removeClass('d-none');
        $('.hapusgambar').addClass('d-none');
        $('.uploadnew').removeClass('d-none');
        $('.changeupload').addClass('d-none');
    });
</script>
@endpush