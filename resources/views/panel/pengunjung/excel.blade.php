<table class="">
    <thead>
        <tr>
            <th>Nama</th>
            <th>Alamat</th>
            <th>Tanggal</th>
            <th>Jam</th>
            <th>Tujuan</th>
            <th>Komentar</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $item)
        <tr>
            <td>{{ $item->nama }}</td>
            <td>{{ $item->alamat }}</td>
            <td>{{ $item->tanggal }}</td>
            <td>{{ $item->jam }}</td>
            <td>{{ $item->tujuan }}</td>
            <td>{{ $item->komentar ?? '-' }}</td>
        </tr>
        @endforeach

    </tbody>
</table>