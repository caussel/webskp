@extends('panel.layouts.apps')
@include('panel.pengunjung.submenu')
@section('content')
@if(session()->has('message'))
<div class="alert alert-success">
    {{ session()->get('message') }}
</div>
@endif
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-menu-2"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                List Data Pengunjung
            </h3>
        </div>
        <div class="text-right d-flex align-items-center">
            <a href="{{ route('pengunjung.pdf') }}" class="btn btn-warning btn-sm text-white mr-3">Export Pdf</a>
            <a href="{{ route('pengunjung.excel') }}" class="btn btn-success btn-sm">Export Excel</a>
        </div>
    </div>
    <div class="kt-portlet__body">
        <table class="table table-striped" id="t_menu">
            <thead>
                <tr>
                    <th>Nama</th>
                    <th>Kartu Identitas</th>
                    <th>Alamat</th>
                    <th>Tanggal</th>
                    <th>Jam</th>
                    <th>Tujuan</th>
                    <th>Komentar</th>
                    <th style="text-align:center">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data as $item)
                <tr>
                    <td>{{ $item->nama }}</td>
                    <td><img src="{{ asset('files/'. $item->kartu_identitas) }}" alt="" width="200"></td>
                    <td>{{ $item->alamat }}</td>
                    <td>{{ $item->tanggal }}</td>
                    <td>{{ $item->jam }}</td>
                    <td>{{ $item->tujuan }}</td>
                    <td>{{ $item->komentar ?? '-' }}</td>
                    <td width="20%" style="text-align:center">
                        <a class="btn btn-primary btn-icon" href="{{ route('pengunjung.edit',$item->id) }}"><i
                                class="fas fa-pencil-alt"></i></a>
                        <button type="button" class="btn btn-danger destroy deletepengunjung btn-icon" id-pengunjung="{{ $item->id }}"><i
                                class="fas fa-trash"></i></button>
                        <form action="{{ route('pengunjung.destroy',$item->id) }}" style="display:none"
                            id="destroy_{{ $item->id }}" method="POST">
                            @method('delete')
                            @csrf
                        </form>
                    </td>
                </tr>
                @endforeach

            </tbody>
        </table>
        <div class="kt-pagination  kt-pagination--brand">
            {{ $data->links() }}
            <div class="kt-pagination__toolbar">
                <span class="pagination__desc">
                    Displaying {{ ($data->total() <= 10)?$data->total():($data->currentpage()-1)*$data->perpage()+1 }}
                    {{ ($data->currentpage()*$data->perpage() < 10)?"to ".$data->currentpage()*$data->perpage():""}} of
                    {{$data->total()}} records
                </span>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    $(document).on('click', '.deletepengunjung', function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var $modal = $('#modal-normal');
        var valuepengunjung = $(this).attr('id-pengunjung');
        $modal.find(".modal-title").html("Hapus Slide");
        $modal.find(".modal-body").html("Apakah Anda Yakin Ingin menghapus pengunjung ini?");
        $modal.find(".modal-footer").html(
            '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button><button type="button" class="btn btn-primary yeshapupengunjung" valueid="' +
            valuepengunjung + '">OK</button>');
        $modal.modal('show');
    });
    $(document).on('click', '.yeshapupengunjung', function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var $modal = $('#modal-normal');
        var id_pengunjung = $(this).attr('valueid');
        $('#destroy_' + id_pengunjung).trigger('submit');
        $modal.modal('hide');
    });
</script>
@endpush