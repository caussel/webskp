@extends('home._app')
@push('header')
<title>Faq - {!! CMS::getSetting('tagline') !!}</title>
<meta name="description" content="Faq PMI Musi Banyuasin" />
<meta name="robots" content="index,nofollow" />
<meta property="og:type" content="article" />
<meta property="og:site_name" content="{!!CMS::getSetting(" title")!!}" />
<meta property="og:title" content="Faq - {!!CMS::getSetting(" title")!!} - {!! CMS::getSetting('tagline') !!}" />
<meta name="twitter:title" content="Faq - {!!CMS::getSetting(" title")!!} - {!! CMS::getSetting('tagline') !!}" />
<meta property="og:url" content="{!!url()->current()!!}" />
<meta property="og:image" content="{!!asset(CMS::getSetting('defaultimage'))!!}" />
<meta property="og:image:type" content="image/jpeg" />
<style>
    .thumb {
        max-height: 300px !important;
    }
</style>
@endpush
@section('content')
<section class="inner-header divider parallax layer-overlay layer-pattern">
    <div class="container pt-10 pb-20">
        <div class="section-content pt-10">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title text-white">Faq</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container  ">
        <section class="position-inherit">
            <div class="container">
              <div class="row">
                <div class="col-md-3 scrolltofixed-container">
                  <div class="list-group scrolltofixed z-index-0">
                    @foreach ($data as $faq)
                    <a href="#section{{ $faq->id }}" class="list-group-item smooth-scroll-to-target">{{ $faq->title }}</a>
                    @endforeach
                  </div>
                </div>
                <div class="col-md-9">
                    @foreach ($data as $faq2)
                  <div id="section{{ $faq->id }}" class="mb-50">
                    <h3>{{ $faq2->title }}</h3>
                    <hr>
                    <p class="mb-20">
                        {{ $faq2->answer }}
                    </p>
                  </div>
                  @endforeach
                </div>
              </div>
            </div>
          </section>
    </div>
</section>
@endsection