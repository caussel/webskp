@extends('home._app')
@push('header')
    <title>Buku Tamu</title>
    
@endpush
@section('content')
    <section class="inner-header divider parallax layer-overlay layer-pattern">
        <div class="container pt-10 pb-20">
            <div class="section-content pt-10">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="title text-white">Buku Tamu</h3>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container mt-30 mb-30 pt-30 pb-30">
            <div class="row">

                <div class="col-md-9">
                    @if (session()->has('message'))
                        <div class="alert alert-success">
                            {{ session()->get('message') }}
                        </div>
                    @endif
                    <div class="bg-lightest border-1px p-30 mb-0">
                        <h3 class="text-theme-colored mt-0 pt-5"> Isi Formulir Perjanjian</h3>
                        <hr>
                      
                        <form action="{!! route('home.perjanjian.store') !!}" method="post" enctype="multipart/form-data"
                            novalidate="novalidate">
                            @csrf
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Nama Lengkap <small>*</small></label>
                                        <input name="nama" type="text" placeholder="Nama Lengkap" required=""
                                            class="form-control" aria-required="true">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Kartu Identitas <small>*</small></label>
                                        <input name="kartu_identitas" class="form-control required" type="file"
                                            placeholder="Kartu Identitas" aria-required="true">
                                    </div>
                                </div>
                            </div>
                            <div class="row">                   
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Tanggal <small>*</small></label>
                                        <input name="tanggal" class="form-control required" type="date"
                                            placeholder="tanggal" aria-required="true">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Jam <small>*</small></label>
                                        <input name="jam" class="form-control required" type="time" placeholder="Jam"
                                            aria-required="true">
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Alamat <small>*</small></label>
                                        <input name="alamat" class="form-control required" type="text"
                                            placeholder="Alamat" aria-required="true">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Tujuan <small>*</small></label>
                                <textarea name="tujuan" class="form-control required" rows="5" placeholder="Tujuan" aria-required="true"></textarea>
                            </div>

                             

                            <div class="form-group">
                                <button type="submit"
                                    class="btn btn-block btn-dark btn-theme-colored btn-sm mt-20 pt-10 pb-10">Kirim</button>
                            </div>
                        </form>
                        <!-- Job Form Validation-->

                    </div>
                </div>
                <div class="col-md-3">
                    @include('home.sidebar')
                </div>
            </div>
        </div>
    </section>
@endsection

