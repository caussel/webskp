@extends('home._app')
@push('header')
<title>Layanan - {!! CMS::getSetting('tagline') !!}</title>
<meta name="description" content="Layanan PMI Musi Banyuasin" />
<meta name="robots" content="index,nofollow" />
<meta property="og:type" content="article" />
<meta property="og:site_name" content="{!!CMS::getSetting(" title")!!}" />
<meta property="og:title" content="Layanan - {!!CMS::getSetting(" title")!!} - {!! CMS::getSetting('tagline') !!}" />
<meta name="twitter:title" content="Layanan - {!!CMS::getSetting(" title")!!} - {!! CMS::getSetting('tagline') !!}" />
<meta property="og:url" content="{!!url()->current()!!}" />
<meta property="og:image" content="{!!asset(CMS::getSetting('defaultimage'))!!}" />
<meta property="og:image:type" content="image/jpeg" />
<style>
    .thumb {
        max-height: 300px !important;
    }
</style>
@endpush
@section('content')
<section class="inner-header divider parallax layer-overlay layer-pattern">
    <div class="container pt-10 pb-20">
        <div class="section-content pt-10">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title text-white">Layanan</h3>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container  ">
        <section class="position-inherit">
            <div class="container">
           <!-- Section: Causes -->
    <section class="bg-lighter">
        <div class="container">
          <div class="section-content">
            <div class="row multi-row-clearfix">
            @foreach ($data as $layanan)
            <div class="col-sm-6 col-md-3 col-lg-3">
                <div class="causes bg-white maxwidth500 mb-30">
                  <div class="thumb">
                    <img src="{{ $layanan->media_path }}" alt="" class="img-fullwidth" style="	width:263px;height:197px;object-fit:cover">
                    <div class="overlay-donate-now">
                      <a href="{{ $layanan->link }}" target="_blank" class="btn btn-dark btn-theme-colored btn-flat btn-sm pull-left mt-10">Lihat <i class="flaticon-charity-make-a-donation font-16 ml-5"></i></a>
                    </div>
                  </div>
                  <div class="causes-details clearfix  border-bottom-theme-color-1px p-15 pt-10 pb-10">
                    <h5 class="font-weight-600 font-16"><a href="{{ $layanan->link }}" target="_blank">{{ $layanan->nama }}</a></h5>
                  </div>
                </div>
              </div>
            @endforeach
            </div>
          </div>
        </div>
      </section>
            </div>
          </section>
    </div>
</section>
@endsection