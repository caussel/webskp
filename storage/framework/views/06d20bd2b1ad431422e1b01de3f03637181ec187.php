<?php echo $__env->make('panel.slider.submenu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->startSection('content'); ?>

<link rel="stylesheet" href=<?php echo asset("jsUpload/css/jquery.fileupload.css"); ?>>
<link rel="stylesheet" href=<?php echo asset("jsUpload/css/jquery.fileupload-ui.css"); ?>>
<div class="kt-portlet kt-portlet--mobile">
    <?php if($errors->any()): ?>
    <div class="alert alert-danger">
        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <ul>
            <li><?php echo e($error); ?></li>
        </ul>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <?php endif; ?>
    <form class="kt-form kt-form--label-right parsley-validated" id="f_slider" action="<?php echo e(route('slider.store')); ?>"
        method="POST" enctype="multipart/form-data">
        <?php echo e(csrf_field()); ?>

        <div id="tags_group"></div>
        <div class="kt-portlet__body">
            <div class="form-group form-group-last">
                <div class="alert alert-secondary" role="alert">
                    <div class="alert-icon"><i class="flaticon-add kt-font-brand"></i></div>
                    <div class="alert-text">
                        Tambah Slide
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-2">Slide Title</label>
                <div class="col-10">
                    <input class="form-control" type="text" value="" id="title" name="title" placeholder="Title"
                        required>
                </div>
            </div>

            <div class="form-group row">
                <input type="hidden" id="imageIdnews" value="" name="id_media">
                <div class="offset-2 col-10">
                    <div class="preview-pic"></div>
                    <hr />
                    <div class="offset-4">
                        <span class="btn btn-success fileinput-button">
                            <i class="fa fa-plus"></i>
                            <span class="uploadnew">Unggah Gambar</span>
                            <span class="changeupload d-none">Ganti Gambar</span>
                            <input id="fileupload" type="file" name="files">
                        </span>
                        <a href="#" class="btn btn-primary fileinput-button pilihgambar">
                            <i class="fa fa-external-link-alt"></i>
                            Pilih Gambar
                        </a>
                    </div>

                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-2">Text 1</label>
                <div class="col-10">
                    <input class="form-control" type="text" value="" name="desc[]">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-2">Text 2</label>
                <div class="col-10">
                    <input class="form-control" type="text" value="" name="desc[]">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-2">Text 3</label>
                <div class="col-10">
                    <input class="form-control" type="text" value="" name="desc[]">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-2">Urutan</label>
                <div class="col-10">
                    <input class="form-control" type="number" value="" name="order" required>
                </div>
            </div>
        </div>

        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-10">
                        <button class="btn btn-success" type="submit">Submit</button>
                        <button type="reset" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('script'); ?>
<script src="<?php echo asset("/js/jquery-ui.js"); ?>"></script>
<script src="<?php echo asset("/js/jquery.fileupload.js"); ?>"></script>
<script>
    $(function() {
        $('#fileupload').fileupload({
            url: '<?php echo route("media.ajaxstore"); ?>',
            dataType: 'json',
            done: function(e, data) {
                console.log(data.result);
                var dataRes = data.result;
                if (dataRes.status == 1) {
                    $('.uploadnew').addClass('d-none');
                    $('.changeupload').removeClass('d-none');
                    $('.preview-pic').html('<img width="100%" src="<?php echo url("files"); ?>/' + dataRes
                        .path + '" />');
                    $('fileinput-button').html('Ganti Gambar');
                    $('#imageIdnews').val(dataRes.imageId);
                    toastr.success(dataRes.message, "Success");
                } else {
                    toastr.error(dataRes.message, "Failed");
                }
            }
        });
    });
    $(document).on('click', '.pilihgambar', function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var $modal = $('#modal-full');
        $modal.find(".modal-title").html("Pilih Gambar");
        $modal.find(".modal-body").html("");
        $.ajax({
            url: "<?php echo e(route('media.modal')); ?>",
            type: "GET",
            data: {},
            success: function(e) {
                $modal.find(".modal-body").html(e);
            }
        });
        $modal.find(".modal-footer").html('');
        $modal.modal('show');
    });
    $(document).on('click', '.choosepicture', function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var $modal = $('#modal-full');
        var id_gambar = $(this).attr('id-gambar');
        var path_gambar = $(this).attr('path-gambar');
        $('.preview-pic').html('<img width="100%" src="<?php echo url("files"); ?>/' + path_gambar + '" />');
        $('#imageIdnews').val(id_gambar);
        $modal.modal('hide');
    });
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('panel.layouts.apps', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\COMPRO\resources\views/panel/slider/create.blade.php ENDPATH**/ ?>