<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__body">
        <div class="row kt-widget">
            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="col-md-2 mx-auto" style="margin-bottom:10px">
                <div class="kt-avatar kt-avatar--outline" id="kt_contacts_edit_avatar">
                    <div class="kt-avatar__holder"
                        style="background-image: url('<?php echo e(asset("files/".$item->path)); ?>');width:100px;height:100px"></div>
                    <label class="kt-avatar__upload choosepicture" id-gambar="<?php echo e($item->id); ?>" path-gambar="<?php echo e($item->path); ?>" data-toggle="kt-tooltip"
                        title="" data-original-title="Pilih Gambar">
                        <i class="fas fa-check"></i>
                    </label>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <hr>
        <div class="kt-pagination  kt-pagination--brand">
            <?php echo e($data->links()); ?>

            <div class="kt-pagination__toolbar">
                <span class="pagination__desc">
                    Displaying <?php echo e(($data->total() <= 10)?$data->total():($data->currentpage()-1)*$data->perpage()+1); ?>

                    <?php echo e(($data->currentpage()*$data->perpage() < 10)?"to ".$data->currentpage()*$data->perpage():""); ?> of
                    <?php echo e($data->total()); ?> records
                </span>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).on('ajaxComplete ajaxReady ready', function () {
        var $modal = $('#modal-full');
        $('ul.kt-pagination__links li a').off('click').on('click', function (e) {
            $modal.modal('show');
            $('.modal-body').load($(this).attr('href'));
            e.preventDefault();
        });
    });
</script><?php /**PATH C:\xampp\htdocs\COMPRO\resources\views/panel/media/modal.blade.php ENDPATH**/ ?>