
<?php echo $__env->make('panel.faq.submenu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->startSection('content'); ?>
<?php if(session()->has('message')): ?>
<div class="alert alert-success">
    <?php echo e(session()->get('message')); ?>

</div>
<?php endif; ?>
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-menu-2"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                List Faq
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <table class="table table-striped" id="t_menu">
            <thead>
                <tr>
                    <th>Judul</th>
                    <th>Jawaban</th>
                    <th style="text-align:center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($item->title); ?></td>
                    
                    <td><?php echo e($item->answer); ?></td>
                    <td width="20%" style="text-align:center">
                        <a class="btn btn-primary btn-icon" href="<?php echo e(route('faq.edit',$item->id)); ?>"><i
                                class="fas fa-pencil-alt"></i></a>
                        <button type="button" class="btn btn-danger destroy deletefaq btn-icon" id-faq="<?php echo e($item->id); ?>"><i
                                class="fas fa-trash"></i></button>
                        <form action="<?php echo e(route('faq.destroy',$item->id)); ?>" style="display:none"
                            id="destroy_<?php echo e($item->id); ?>" method="POST">
                            <?php echo method_field('delete'); ?>
                            <?php echo csrf_field(); ?>
                        </form>
                    </td>
                </tr>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            </tbody>
        </table>
        <div class="kt-pagination  kt-pagination--brand">
            <?php echo e($data->links()); ?>

            <div class="kt-pagination__toolbar">
                <span class="pagination__desc">
                    Displaying <?php echo e(($data->total() <= 10)?$data->total():($data->currentpage()-1)*$data->perpage()+1); ?>

                    <?php echo e(($data->currentpage()*$data->perpage() < 10)?"to ".$data->currentpage()*$data->perpage():""); ?> of
                    <?php echo e($data->total()); ?> records
                </span>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('script'); ?>
<script>
    $(document).on('click', '.deletefaq', function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var $modal = $('#modal-normal');
        var valuefaq = $(this).attr('id-faq');
        $modal.find(".modal-title").html("Hapus Slide");
        $modal.find(".modal-body").html("Apakah Anda Yakin Ingin menghapus faq ini?");
        $modal.find(".modal-footer").html(
            '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button><button type="button" class="btn btn-primary yeshapufaq" valueid="' +
            valuefaq + '">OK</button>');
        $modal.modal('show');
    });
    $(document).on('click', '.yeshapufaq', function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var $modal = $('#modal-normal');
        var id_faq = $(this).attr('valueid');
        $('#destroy_' + id_faq).trigger('submit');
        $modal.modal('hide');
    });
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('panel.layouts.apps', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\COMPRO\resources\views/panel/faq/index.blade.php ENDPATH**/ ?>