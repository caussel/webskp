<?php $__env->startPush('meta'); ?>
    <title><?php echo CMS::getSetting('title'); ?></title>
    <meta name="description" content="<?php echo CMS::getSetting('description'); ?>" />
    <meta name="keywords" content="<?php echo CMS::getSetting('keywords'); ?>" />
    <meta name="author" content="yudz." />
    <meta property="og:site_name" content="<?php echo CMS::getSetting('title'); ?>" />
    <meta property="og:url" content="<?php echo url()->current(); ?>" />
    <meta property="og:image" content="<?php echo asset(CMS::getSetting('defaultimage')); ?>" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:title" content="<?php echo CMS::getSetting('title'); ?> - <?php echo CMS::getSetting('tagline'); ?>" />
    <meta property="og:description" content="<?php echo CMS::getSetting('description'); ?>" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="<?php echo CMS::getSetting('title'); ?> - <?php echo e(CMS::getSetting('tagline')); ?>" />
    <meta name="twitter:description" content="<?php echo CMS::getSetting('description'); ?>" />
    <meta name="twitter:image:src" content="<?php echo asset(CMS::getSetting('defaultimage')); ?>" />
    <style>
        .portfolio-view {
            left: 42% !important;
            right: 0px !important;
            top: 50% !important;
        }

        .gallery-isotope .gallery-item .thumb {
            max-height: 230px !important;
        }
    </style>
<?php $__env->stopPush(); ?>
<?php $__env->startSection('preloader'); ?>
    <div id="preloader">
        <div id="spinner">
            <div class="preloader-dot-loading">
                <div class="cssload-loading"><i></i><i></i><i></i><i></i></div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
    <!-- Section: home -->
    <section id="home">
        <div class="container-fluid p-0">
            <div class="rev_slider_wrapper">
                <div class="rev_slider rev_slider_default" data-version="5.0">
                    <ul>
                        <?php $__currentLoopData = CMS::getSlider(CMS::getSetting('slideshow')); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $keySlide => $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li data-index="rs-<?php echo $keySlide; ?>" data-transition="slidingoverlayhorizontal"
                                data-slotamount="default" data-easein="default" data-easeout="default"
                                data-masterspeed="default" data-thumb="<?php echo CMS::getImage($slide->id_media); ?>" data-rotate="0"
                                data-saveperformance="off" data-title="<?php echo $slide->title; ?>" data-description="">
                                <img src="<?php echo CMS::getImage($slide->id_media); ?>" alt="" data-bgposition="center center"
                                    data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-bgparallax="10"
                                    data-no-retina>
                            </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 p-3">
                    <h3 class="line-bottom border-bottom mt-0 text-center" sty>Layanan</h3>
                    <div id="grid" class="gallery-isotope grid-5 masonry gutter-10 clearfix">
                            <?php $__currentLoopData = CMS::getLayanan(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $layanan): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="gallery-item breakfast">
                                <div class="causes bg-white maxwidth500 mb-30">
                                  <div class="thumb">
                                    <img src="<?php echo e($layanan->media_path); ?>" alt="" class="img-fullwidth" style="	width:263px;height:197px;object-fit:cover">
                                    <div class="overlay-donate-now">
                                      <a href="<?php echo e($layanan->link); ?>" target="_blank" class="btn btn-dark btn-theme-colored btn-flat btn-sm pull-left mt-10">Lihat <i class="flaticon-charity-make-a-donation font-16 ml-5"></i></a>
                                    </div>
                                  </div>
                                  <div class="causes-details clearfix  border-bottom-theme-color-1px p-15 pt-10 pb-10">
                                    <h5 class="font-weight-600 font-12"><a href="<?php echo e($layanan->link); ?>" target="_blank"><?php echo e($layanan->nama); ?></a></h5>
                                  </div>
                                </div>
                              </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 p-3">
                <h3 class="line-bottom border-bottom mt-0 text-center" sty>STATISTIK KINERJA</h3>
                    <div class="row">
                    <?php
                        $dataindek = CMS::getDataIndeks();
                    ?>
                    <div class="row">
                        <?php if(isset($dataindek[0])): ?>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="fluid-width-video-wrapper"><img src="<?php echo e($dataindek[0]->media_path); ?>" alt="" srcset=""></div>
                            <div class="image-box-details text-center p-20 pt-30 pb-30 bg-lighter">
                                <h3 class="title mt-0"><?php echo e($dataindek[0]->judul); ?></h3>
                                <p class="desc mb-20"><?php echo e($dataindek[0]->deskripsi); ?></p>
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="col-md-4"></div>
                        <?php if(isset($dataindek[1])): ?>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="fluid-width-video-wrapper"><img src="<?php echo e($dataindek[1]->media_path); ?>" alt="" srcset=""></div>
                            <div class="image-box-details text-center p-20 pt-30 pb-30 bg-lighter">
                                <h3 class="title mt-0"><?php echo e($dataindek[1]->judul); ?></h3>
                                <p class="desc mb-20"><?php echo e($dataindek[1]->deskripsi); ?></p>
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="col-md-4"></div>
                        <?php if(isset($dataindek[2])): ?>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="fluid-width-video-wrapper"><img src="<?php echo e($dataindek[2]->media_path); ?>" alt="" srcset=""></div>
                            <div class="image-box-details text-center p-20 pt-30 pb-30 bg-lighter">
                                <h3 class="title mt-0"><?php echo e($dataindek[2]->judul); ?></h3>
                                <p class="desc mb-20"><?php echo e($dataindek[2]->deskripsi); ?></p>
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="col-md-4"></div>
                    </div>
                    <div class="row">
                        <?php if(isset($dataindek[3])): ?>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="fluid-width-video-wrapper"><img src="<?php echo e($dataindek[3]->media_path); ?>" alt="" srcset=""></div>
                            <div class="image-box-details text-center p-20 pt-30 pb-30 bg-lighter">
                                <h3 class="title mt-0"><?php echo e($dataindek[3]->judul); ?></h3>
                                <p class="desc mb-20"><?php echo e($dataindek[3]->deskripsi); ?></p>
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="col-md-4"></div>
                        <?php if(isset($dataindek[4])): ?>
                        <div class="col-xs-12 col-sm-4 col-md-4">
                            <div class="fluid-width-video-wrapper"><img src="<?php echo e($dataindek[4]->media_path); ?>" alt="" srcset=""></div>
                            <div class="image-box-details text-center p-20 pt-30 pb-30 bg-lighter">
                                <h3 class="title mt-0"><?php echo e($dataindek[4]->judul); ?></h3>
                                <p class="desc mb-20"><?php echo e($dataindek[4]->deskripsi); ?></p>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
  

    <section id="about">
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-8">
                        <h3 class="line-bottom border-bottom mt-0">Berita</h3>
                        <?php $__currentLoopData = CMS::getNews(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $berita): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <article
                                class="post clearfix mb-sm-30 border-bottom-theme-color-2px bg-silver-light sm-maxwidth400 border-bottom mt-5 mb-0 pt-10 pb-15">
                                <div class="col-md-4">
                                    <div class="entry-header">
                                        <div class="post-thumb thumb">
                                            <a
                                                href="<?php echo e(route('home.showPost', ['id' => $berita->id, 'post' => $berita->slug])); ?>">
                                                <img src="<?php echo CMS::getImage($berita->id_media, true); ?>" alt=""
                                                    class="img-responsive img-fullwidth">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div style="background: #f7f8fa">
                                        <div class="entry-meta media mt-0 no-bg no-border">
                                            <div
                                                class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                                                <ul>
                                                    <li class="font-16 text-white font-weight-600 border-bottom">
                                                        <?php echo $berita->publish_date->format('d'); ?></li>
                                                    <li class="font-12 text-white text-uppercase">
                                                        <?php echo $berita->publish_date->format('M'); ?></li>
                                                </ul>
                                            </div>
                                            <div class="media-body pl-15">
                                                <div class="event-content pull-left flip">
                                                    <h4 class="entry-title text-white text-uppercase m-0 mt-5"><a
                                                            href="<?php echo route('home.showPost', [$berita->id, $berita->slug]); ?>"><?php echo $berita->judul; ?></a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="mt-10"><?php echo CMS::excerpt($berita->content); ?></p>
                                        <a href="<?php echo route('home.showPost', [$berita->id, $berita->slug]); ?>"
                                            class="btn btn-default btn-sm btn-theme-colored mt-10" style="float:right">Read
                                            more</a>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </article>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <h3 class="line-bottom border-bottom mt-0">Agenda Kegiatan</h3>
                        <div class="event media sm-maxwidth400 border-bottom mt-5 mb-0 pt-10 pb-15">
                            <?php $__currentLoopData = CMS::getAgenda(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agenda): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="row mt-10">
                                    <div class="col-xs-2 col-md-3 pr-0">
                                        <div
                                            class="event-date text-center bg-theme-colored border-1px p-0 pt-10 pb-10 sm-custom-style">
                                            <ul>
                                                <li class="font-28 text-white font-weight-700"><?php echo $agenda->start->format('d'); ?>

                                                </li>
                                                <li class="font-18 text-white text-center text-uppercase">
                                                    <?php echo $agenda->start->format('M'); ?></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col-xs-9 pr-10 pl-10">
                                        <div class="event-content mt-10 p-5 pb-0 pt-0">
                                            <h5 class="media-heading font-16 font-weight-600"><a
                                                    href="<?php echo route('home.showAgenda', [$agenda->id, $agenda->slug]); ?>"><?php echo $agenda->title; ?></a>
                                            </h5>
                                            <ul class="list-inline font-weight-600 text-gray-dimgray">
                                                <?php echo !empty($agenda->time_start) && !empty($agenda->time_end)
                                                    ? '<li><i
                                                                                                class="fa fa-clock-o text-theme-colored"></i> ' .
                                                        $agenda->time_start .
                                                        ' -
                                                                                            ' .
                                                        $agenda->time_end .
                                                        '</li>'
                                                    : ''; ?>

                                                <?php echo !empty($agenda->time_start) && empty($agenda->time_end)
                                                    ? '<li><i
                                                                                                class="fa fa-clock-o text-theme-colored"></i> ' .
                                                        $agenda->time_start .
                                                        ' -
                                                                                            Selesai</li>'
                                                    : ''; ?>

                                                <li> <i class="fa fa-map-marker text-theme-colored"></i>
                                                    <?php echo $agenda->lokasi; ?>

                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="bg-silver-light">
        <div class="container">
            <div class="section-content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="owl-carousel owl-theme owl-rtl owl-loaded">
                            <div class="img-hover-border">
                                <?php $__currentLoopData = CMS::advertisement(1); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $theAds): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <a href="<?php echo $theAds->tautan; ?>" target="_blank">
                                        <img class="img-fullwidth" src="<?php echo e(CMS::getImage($theAds->id_media)); ?>"
                                            alt="">
                                    </a>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3 class="text-uppercase title line-bottom mt-0 mb-40"><i
                                class="fa fa-calendar text-gray-darkgray mr-10"></i>Informasi <span
                                class="text-theme-colored">& Pengumuman</span></h3>
                        <?php $__currentLoopData = CMS::getAnnouncement(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pengumuman): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="icon-box border-bottom clearfix">
                                <a href="<?php echo route('home.showPengumuman', [$pengumuman->id, $pengumuman->slug]); ?>">
                                    <div class="ml-0 mr-sm-0">
                                        <h5 class="icon-box-title mb-10 text-uppercase letter-space-1">
                                            <?php echo e($pengumuman->judul); ?>

                                        </h5>
                                        <p class=""><?php echo e(CMS::excerpt($pengumuman->content, 10)); ?></p>
                                    </div>
                                </a>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="divider parallax layer-overlay overlay-theme-colored-8" data-bg-img="<?php echo asset("
        front/images/header.jpg"); ?>"
        data-parallax-ratio="0.7">
        <div class="container pt-80 pb-80">

        </div>
    </section>

    <!-- <section class="">
        <div class="container">
            <div class="section-title text-center">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <h2 class="text-uppercase line-bottom-center mt-0">Photo <span
                                class="text-theme-colored font-weight-600">Gallery</span></h2>
                        <div class="title-icon">
                            <i class="flaticon-charity-hand-holding-a-heart"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div id="grid" class="gallery-isotope grid-5 masonry gutter-10 clearfix">
                        <?php $__currentLoopData = CMS::getGallery(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $gallery): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="gallery-item breakfast">
                                <div class="thumb">
                                    <img class="img-fullwidth" src="<?php echo CMS::getImage($gallery->gallerymedias->first()->id); ?>" alt="project">
                                    <div class="overlay-shade"></div>
                                    <div class="portfolio-upper-part">
                                        <h4 class="font-22 mb-0"><?php echo e($gallery->title); ?></h4>
                                        <h5 class="font-14 mt-0"><?php echo e($gallery->gallerymedias->count()); ?> Photo</h5>
                                    </div>
                                    <div class="portfolio-view">
                                        <a class="" title="<?php echo e($gallery->title); ?>"
                                            href="<?php echo e(route('home.showGallery', $gallery->id)); ?>">
                                            <i class="fa fa-camera font-40 text-theme-colored"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
   
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 p-3">
                    <h3 class="line-bottom border-bottom mt-0 text-center" sty>Link Terkait</h3>
                    <div class="row">
                        <div class="owl-carousel-5col clients-logo text-center owl-nav-top" data-nav="true">
                            <?php $__currentLoopData = CMS::getLinkTerkait(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $linkterkait): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <div class="item"> <a href="<?php echo e($linkterkait->link); ?>" target="_blank"><img
                                            src="<?php echo e($linkterkait->media_path); ?>"
                                            alt=""><?php echo e($linkterkait->nama); ?></a></div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12 ">
                    <h3 class="line-bottom border-bottom mt-0 text-center">Faq</h3>
                    <div id="accordion1" class="panel-group accordion">
                        <?php $__currentLoopData = CMS::getFaq(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $faq): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="panel">
                                <div class="panel-title"> <a data-parent="#accordion1" data-toggle="collapse"
                                        href="#accordion1<?php echo e($faq->id); ?>" class="collapsed" aria-expanded="false">
                                        <span class="open-sub"></span> <strong><?php echo e($faq->title); ?></strong></a> </div>
                                <div id="accordion1<?php echo e($faq->id); ?>" class="panel-collapse collapse" role="tablist"
                                    aria-expanded="false">
                                    <div class="panel-content">
                                        <?php echo e($faq->answer); ?>

                                    </div>
                                </div>
                            </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('footer'); ?>
    <script>
        $(document).ready(function(e) {
            $(".rev_slider_default").revolution({
                sliderType: "standard",
                sliderLayout: "auto",
                dottedOverlay: "none",
                delay: 5000,
                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    onHoverStop: "off",
                    touch: {
                        touchenabled: "on",
                        swipe_threshold: 75,
                        swipe_min_touches: 1,
                        swipe_direction: "horizontal",
                        drag_block_vertical: false
                    },
                    arrows: {
                        style: "zeus",
                        enable: true,
                        hide_onmobile: true,
                        hide_under: 600,
                        hide_onleave: true,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        tmp: '<div class="tp-title-wrap">    <div class="tp-arr-imgholder"></div> </div>',
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 30,
                            v_offset: 0
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 30,
                            v_offset: 0
                        }
                    },
                    bullets: {
                        enable: true,
                        hide_onmobile: true,
                        hide_under: 600,
                        style: "metis",
                        hide_onleave: true,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        direction: "horizontal",
                        h_align: "center",
                        v_align: "bottom",
                        h_offset: 0,
                        v_offset: 30,
                        space: 5,
                        tmp: '<span class="tp-bullet-img-wrap">  <span class="tp-bullet-image"></span></span><span class="tp-bullet-title">TItle</span>'
                    }
                },
                responsiveLevels: [1280, 1024, 778],
                visibilityLevels: [1280, 1024, 778],
                gridwidth: [1260, 1024, 778, 480],
                gridheight: [580, 768, 960, 720],
                lazyType: "none",
                parallax: {
                    origo: "slidercenter",
                    speed: 1000,
                    levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 100, 55],
                    type: "scroll"
                },
                shadow: 0,
                spinner: "off",
                stopLoop: "on",
                stopAfterLoops: 0,
                stopAtSlide: -1,
                shuffle: "off",
                autoHeight: "off",
                fullScreenAutoWidth: "on",
                fullScreenAlignForce: "off",
                fullScreenOffsetContainer: "",
                fullScreenOffset: "0",
                hideThumbsOnMobile: "off",
                hideSliderAtLimit: 0,
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLilmit: 0,
                debugMode: false,
                fallbacks: {
                    simplifyAll: "off",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: false,
                }
            });
        });
    </script>

    <?php if(!empty($iklanFloat->id_media) || !empty($iklanFloat->script)): ?>
        <!--Start Walldown-->
        <div class="floatingads">
            <div class="mfp-bg mfp-ready"></div>
            <div class="wallfloat">
                <div class="walldown-banner">
                    <span id="stopBtn" class="shbutton"><img src="/front/close.gif" /></span>
                    <div class="walldownads">
                        <div class="clearfix"></div>
                        <div class="walldownads">
                            <?php if(!empty($iklanFloat->script)): ?>
                                <?php echo $iklanFloat->script; ?>

                            <?php else: ?>
                                <span class="walldownimg">
                                    <a href="<?php echo e($iklanFloat->tautan); ?>" class="clickedAds" target="blank">
                                        <img src="<?php echo e(CMS::getImage($iklanFloat->id_media)); ?>" width="100%" />
                                    </a>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <style>
            .walldown-banner {
                margin: auto;
                width: 80%;
                position: relative;
            }

            .shbutton {
                float: right;
            }

            .wallfloat {
                position: fixed;
                top: 10%;
                width: 100%;
                margin: auto;
                z-index: 1043
            }
        </style>
        <script>
            $(document).ready(function() {
                $(".shbutton").on('click', function() {
                    $('.floatingads').remove();
                });
            });
            var player;

            function onYouTubePlayerAPIReady() {
                player = new YT.Player('video', {
                    events: {
                        'onReady': onPlayerReady
                    }
                });
            }

            function onPlayerReady(event) {
                var playButton = document.getElementById("play-button");
                playButton.addEventListener("click", function() {
                    player.playVideo();
                });
                var pauseButton = document.getElementById("stopBtn");
                pauseButton.addEventListener("click", function() {
                    player.pauseVideo();
                });
            }
            var tag = document.createElement('script');
            tag.src = "//www.youtube.com/player_api";
            var firstScriptTag = document.getElementsByTagName('script')[0];
            firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
            $(document).on('click', '.clickedAds', function() {
                $('.shbutton').trigger('click');
            });
            $(document).mouseup(function(e) {
                var container = $(".walldownads");
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    $('.floatingads').remove();
                }
            });
        </script>
        <!--Stop Walldown-->
    <?php endif; ?>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('home._app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\COMPRO\resources\views/home/index.blade.php ENDPATH**/ ?>