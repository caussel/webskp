<?php echo $__env->make('panel.halaman.submenu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->startSection('content'); ?>
<link rel="stylesheet" href=<?php echo asset("jsUpload/css/jquery.fileupload.css"); ?>>
<?php if($errors->any()): ?>
<div class="alert alert-danger">
    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <ul>
        <li><?php echo e($error); ?></li>
    </ul>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
<?php endif; ?>
<form class="kt-form kt-form--label-right row" id="f_halaman" action="<?php echo e(route('halaman.update',$data->id)); ?>"
    method="POST" enctype="multipart/form-data">
    <?php echo method_field("patch"); ?>
    <?php echo e(csrf_field()); ?>

    <div class="col-8">
        <div class="kt-portlet kt-portlet--mobile">
            <input type="hidden" name="lastState" value="<?php echo e(url()->previous()); ?>">
            <div class="kt-portlet__body">
                <div class="form-group form-group-last">
                    <div class="alert alert-secondary" role="alert">
                        <div class="alert-icon"><i class="flaticon-edit kt-font-brand"></i></div>
                        <div class="alert-text">
                            Edit halaman
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-12">
                        <input class="form-control" type="text" value="<?php echo e($data->judul); ?>" id="judul" name="title"
                            placeholder="isi Judul">
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-12">
                        <textarea class="form-control" name="content" id="content"><?php echo e($data->content); ?></textarea>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button class="btn btn-success" type="submit">Submit</button>
                            <a href="<?php echo e(url()->previous()); ?>" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Media
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="form-group row">
                    <input type="hidden" id="imageIdnews" value="<?php echo $data->id_media; ?>" name="id_media">
                    <div class="col-12">
                        <div class="preview-pic">
                            <?php if(!empty($data->id_media)): ?>
                                <?php if(!empty($data->media->path)): ?>
                                    <img src="<?php echo e(asset("files/".$data->media->path_220)); ?>" alt="" width="100%">
                                <?php else: ?>
                                    Photo Telah Dihapus
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                        <hr />
                    </div>
                    <div class="col-6 text-center">
                        <span class="btn btn-success fileinput-button col-12">
                            <span class="uploadnew <?php echo (!empty($data->id_media))?" d-none":""; ?>">Upload</span>
                            <span class="changeupload <?php echo (!empty($data->id_media))?"":" d-none"; ?>">Ganti</span>
                            <input id="fileupload" type="file" name="files">
                        </span>
                    </div>
                    <div class="col-6 text-center">
                        <a href="#"
                            class="btn btn-primary fileinput-button pilihgambar col-12 <?php echo (!empty($data->id_media))?"
                            d-none":""; ?>">
                            Pilih
                        </a>
                        <a href="#"
                            class="btn btn-danger fileinput-button hapusgambar <?php echo (!empty($data->id_media))?"":"
                            d-none"; ?> col-12">
                            Hapus
                        </a>
                    </div>
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="ket_gambar" id="ket_gambar" cols="30" rows="3"
                        placeholder="Keterangan Gambar"><?php echo e($data->ket_photo); ?></textarea>
                </div>
            </div>
        </div>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Status
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <select class="form-control select2" name="status" id="status">
                    <option value="1" <?php echo ($data->status == 1)?"selected":""; ?>>Publish</option>
                    <option value="0" <?php echo ($data->status == 0)?"selected":""; ?>>Draft</option>
                </select>
            </div>
        </div>
    </div>
</form>

<?php $__env->stopSection(); ?>
<?php $__env->startPush('script'); ?>
<script src="<?php echo asset("/js/jquery-ui.js"); ?>"></script>
<script src="<?php echo asset("/js/jquery.fileupload.js"); ?>"></script>
<script>
    $(document).ready(function() {
        $("#f_halaman").validate({
            rules: {
                title: {
                    required: !0
                }
            },
            errorPlacement: function(e, r) {
                var i = r.closest(".input-group");
                i.length ? i.after(e.addClass("invalid-feedback")) : r.after(e.addClass(
                    "invalid-feedback"));
            },
            invalidHandler: function(e, r) {
                KTUtil.scrollTop();
            }
        });
    });
    tinymce.init({
        selector: '#content',
        height: 400,
        body_class: 'form-control',
        menubar: false,
        branding: false,
        plugins: "paste",
        paste_as_text: true,
    });
    $(function() {
        $('#fileupload').fileupload({
            url: '<?php echo route("media.ajaxstore"); ?>',
            dataType: 'json',
            done: function(e, data) {
                console.log(data.result);
                var dataRes = data.result;
                if (dataRes.status == 1) {
                    $('.uploadnew').addClass('d-none');
                    $('.changeupload').removeClass('d-none');
                    $('.preview-pic').html('<img width="100%" src="<?php echo url("files"); ?>/' + dataRes
                        .path + '" />');
                    $('fileinput-button').html('Ganti Gambar');
                    $('#imageIdnews').val(dataRes.imageId);
                    $('.pilihgambar').addClass('d-none');
                    $('.hapusgambar').removeClass('d-none');
                    toastr.success(dataRes.message, "Success");
                } else {
                    toastr.error(dataRes.message, "Failed");
                }
            }
        });
    });
    $(document).on('click', '.pilihgambar', function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var $modal = $('#modal-full');
        $modal.find(".modal-title").html("Pilih Gambar");
        $modal.find(".modal-body").html("");
        $.ajax({
            url: "<?php echo e(route('media.modal')); ?>",
            type: "GET",
            data: {},
            success: function(e) {
                $modal.find(".modal-body").html(e);
            }
        });
        $modal.find(".modal-footer").html('');
        $modal.modal('show');
    });
    $(document).on('click', '.choosepicture', function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var $modal = $('#modal-full');
        var id_gambar = $(this).attr('id-gambar');
        var path_gambar = $(this).attr('path-gambar');
        $('.preview-pic').html('<img width="100%" src="<?php echo url("files"); ?>/' + path_gambar + '" />');
        $('#imageIdnews').val(id_gambar);
        $('.pilihgambar').addClass('d-none');
        $('.hapusgambar').removeClass('d-none');
        $modal.modal('hide');
    });
    $(document).on('click', '.hapusgambar', function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        $('.preview-pic').html('');
        $('#imageIdnews').val('');
        $('.pilihgambar').removeClass('d-none');
        $('.hapusgambar').addClass('d-none');
        $('.uploadnew').removeClass('d-none');
        $('.changeupload').addClass('d-none');
    });
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('panel.layouts.apps', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\COMPRO\resources\views/panel/halaman/edit.blade.php ENDPATH**/ ?>