<?php $__env->startPush('header'); ?>
<title><?php echo $data->judul; ?> - <?php echo CMS::getSetting('tagline'); ?></title>
<meta name="description" content="<?php echo CMS::excerpt($data->content,20); ?>" />
<?php
    $keyw = explode(" ",strtolower($data->judul));
?>
<meta name="keywords" content="<?php echo implode(" , ",$keyw); ?>" />
<meta name="robots" content="index,follow" />
<meta property="og:type" content="article" />
<meta property="og:site_name" content="PMI Musi Banyuasin" />
<meta property="og:title" content="<?php echo $data->judul; ?>" />
<meta property="og:description" content="<?php echo CMS::excerpt($data->content,20); ?>" />
<meta property="og:url" content="<?php echo route('home.showPost',['id'=>$data->id,'post'=>$data->slug]); ?>" />
<meta property="og:image" content="<?php echo CMS::getImage($data->id_media); ?>" />
<meta property="og:image:type" content="image/jpeg" />
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="<?php echo $data->judul; ?>" />
<meta name="twitter:description" content="<?php echo CMS::excerpt($data->content,20); ?>" />
<meta name="twitter:image:src" content="<?php echo CMS::getImage($data->id_media); ?>" />
<?php $__env->stopPush(); ?>
<?php $__env->startSection('content'); ?>

<section>
    <div class="container mt-30 mb-30 pt-30 pb-30">
        <div class="row">
            <div class="col-md-9">
                <div class="blog-posts single-post">
                    <article class="post clearfix mb-0">
                        <div class="entry-header mb-20">
                            <div class="post-thumb thumb mb-10">
                                <?php if(!empty($data->yt_video)): ?>
                                <?php
                                    $pattern = '%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/\s]{11})%i';
                                    if (preg_match($pattern, $data->yt_video, $match)) {
                                        $video_id = $match[1];
                                    }
                                ?>
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php echo e($video_id); ?>"
                                    frameborder="0"
                                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>
                                <?php else: ?>
                                <img src="<?php echo CMS::getImage($data->id_media); ?>" alt=""
                                    class="img-responsive img-fullwidth" />
                                <?php endif; ?>

                            </div>
                            <span class="pl-10"><em><?php echo $data->ket_photo; ?></em></span>
                        </div>
                        <div class="entry-content">
                            <div class="entry-meta media no-bg no-border mt-15 pb-20">
                                <div
                                    class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                                    <ul>
                                        <li class="font-16 text-white font-weight-600">
                                            <?php echo $data->created_at->format('d'); ?></li>
                                        <li class="font-12 text-white text-uppercase">
                                            <?php echo $data->created_at->format('M'); ?></li>
                                    </ul>
                                </div>
                                <div class="media-body pl-15">
                                    <div class="event-content pull-left flip">
                                        <h4 class="entry-title text-black text-uppercase m-0"><?php echo e($data->judul); ?></h4>
                                        <span class="mb-10 text-gray-darkgray mr-10 font-13">
                                            <i class="fa fa-commenting-o mr-5 text-theme-colored"></i>
                                            <span class="disqus-comment-count"
                                                data-disqus-url="<?php echo route('home.showPost',['id'=>$data->id,'post'=>$data->slug]); ?>">
                                                Comments
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <?php echo $data->content; ?>

                            <div class="tagline p-0 pt-20 mt-5">
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="tags">
                                            <p class="mb-0"> <?php echo CMS::getTags($data->id); ?> </p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </article>
                    <div class="mt-10 mb-0">
                        <h5 class="pull-left mt-10 mr-20 text-theme-colored">Share:</h5>
                        <div class="addthis_inline_share_toolbox_35fm"></div>
                    </div>

                    <div class="comments-area">
                        <h5 class="comments-title">Comments</h5>
                        <div id="disqus_thread"></div>
                    </div>

                </div>
            </div>
            <div class="col-md-3">
                <?php echo $__env->make('home.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('footer'); ?>
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-570e568148d64f3c"></script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('home._app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\COMPRO\resources\views/home/single.blade.php ENDPATH**/ ?>