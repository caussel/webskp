<div class="sidebar sidebar-left mt-sm-30">
    <div class="widget">
        <h5 class="widget-title line-bottom">Cari Berita</h5>
        <div class="search-form">
            <form action="/search?" type="get">
                <div class="input-group">
                    <input type="text" placeholder="Click to Search" class="form-control search-input" value=""
                        name="q">
                    <span class="input-group-btn">
                        <button type="submit" class="btn search-button"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </form>
        </div>
    </div>
    <?php echo CMS::getSidebarTopAds(3); ?>

    <div class="widget">
        <h5 class="widget-title line-bottom">Kategori</h5>
        <div class="categories">
            <ul class="list list-border angle-double-right">
                <?php echo CMS::getCategories(); ?>

            </ul>
        </div>
    </div>
    <div class="widget">
        <h5 class="widget-title line-bottom">AGENDA</h5>
        <div class="event media sm-maxwidth400 border-bottom mt-5 mb-0 pt-10 pb-15">
            <?php $__currentLoopData = CMS::getAgenda(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $agenda): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="row mt-10 pl-5">
                <div class="col-xs-2 col-md-3 pr-0">
                    <div class="event-date text-center bg-theme-colored border-1px p-0 pt-10 pb-10 sm-custom-style">
                        <ul>
                            <li class="font-28 text-white font-weight-700"><?php echo $agenda->start->format('d'); ?></li>
                            <li class="font-18 text-white text-center text-uppercase"><?php echo $agenda->start->format('M'); ?>

                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xs-9 pr-10 pl-10">
                    <div class="event-content mt-10 p-5 pb-0 pt-0">
                        <h5 class="media-heading font-16 font-weight-600"><a
                                href="<?php echo route('home.showAgenda',[$agenda->id,$agenda->slug]); ?>"><?php echo $agenda->title; ?></a>
                        </h5>
                        <ul class="list-inline font-weight-600 text-gray-dimgray">
                            <?php echo (!empty($agenda->time_start) && !empty($agenda->time_end))?'<li><i
                                    class="fa fa-clock-o text-theme-colored"></i> '.$agenda->time_start.' -
                                '.$agenda->time_end.'</li>':""; ?>

                            <li> <i class="fa fa-map-marker text-theme-colored"></i> <?php echo $agenda->lokasi; ?> </li>
                        </ul>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
    <div class="widget">
        <h5 class="widget-title line-bottom">Latest News</h5>
        <div class="latest-posts">
            <?php $__currentLoopData = CMS::getLastNews(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lastNews): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <article class="post media-post clearfix pb-0 mb-10">
                <a class="post-thumb"
                    href="<?php echo route('home.showPost',['id'=>$lastNews->id,'post'=>$lastNews->slug]); ?>"><img
                        src="<?php echo CMS::getImage($lastNews->id_media,true); ?>" width="75" /></a>
                <div class="post-right">
                    <h5 class="post-title mt-0"><a
                            href="<?php echo route('home.showPost',['id'=>$lastNews->id,'post'=>$lastNews->slug]); ?>"><?php echo $lastNews->title; ?></a>
                    </h5>
                    <p><?php echo CMS::excerpt($lastNews->content,6); ?></p>
                </div>
            </article>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
    </div>
    <?php echo CMS::getSidebarTopAds(4); ?>

</div><?php /**PATH C:\xampp\htdocs\COMPRO\resources\views/home/sidebar.blade.php ENDPATH**/ ?>