<?php echo $__env->make('panel.slider.submenu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->startSection('content'); ?>
<link rel="stylesheet" href=<?php echo asset("jsUpload/css/jquery.fileupload.css"); ?>>
<link rel="stylesheet" href=<?php echo asset("jsUpload/css/jquery.fileupload-ui.css"); ?>>
<div class="kt-portlet kt-portlet--mobile">
    <?php if($errors->any()): ?>
    <div class="alert alert-danger">
        <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <ul>
            <li><?php echo e($error); ?></li>
        </ul>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
    <?php endif; ?>
    <form class="kt-form kt-form--label-right parsley-validated" id="f_slider" action="<?php echo e(route('slider.update',$data->id)); ?>"
        method="POST" enctype="multipart/form-data">
        <?php echo method_field('patch'); ?>
        <?php echo csrf_field(); ?>
        <div id="tags_group"></div>
        <div class="kt-portlet__body">
            <div class="form-group form-group-last">
                <div class="alert alert-secondary" role="alert">
                    <div class="alert-icon"><i class="flaticon-edit kt-font-brand"></i></div>
                    <div class="alert-text">
                        Edit Slide
                    </div>
                </div>
            </div>

            <div class="form-group row">
                <label class="col-form-label col-2">Slide Title</label>
                <div class="col-10">
                    <input class="form-control" type="text" value="<?php echo e($data->title); ?>" id="title" name="title"
                        placeholder="Title" required>
                </div>
            </div>

            <div class="form-group row">
                <input type="hidden" id="imageIdnews" value="<?php echo e($data->id_media); ?>" name="id_media">
                <div class="offset-2 col-10">
                    <div class="preview-pic">
                        <?php if(!empty($data->id_media)): ?>
                        <?php if(!empty($data->media->path)): ?>
                        <img src="<?php echo e(asset("files/".$data->media->path)); ?>" width="100%" alt="">
                        <?php else: ?>
                        Photo Telah Dihapus
                        <?php endif; ?>

                        <?php endif; ?>
                    </div>
                    <hr />
                    <div class="offset-4">
                        <span class="btn btn-success fileinput-button">
                            <i class="fa fa-upload"></i>
                            <span class="changeupload <?php echo (!empty($data->id_media))?"":" d-none"; ?>">Ganti
                                Gambar</span>
                            <span class="uploadnew <?php echo (!empty($data->id_media))?" d-none":""; ?>">Unggah Gambar</span>

                            <input id="fileupload" type="file" name="files">
                        </span>
                        <a href="#" class="btn btn-primary fileinput-button pilihgambar">
                            <i class="fa fa-external-link-alt"></i>
                            Pilih Gambar
                        </a>

                    </div>

                </div>
            </div>

            <?php $__currentLoopData = json_decode($data->desc); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $keys => $desc): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="form-group row">
                <label class="col-form-label col-2">Text <?php echo e($keys+1); ?></label>
                <div class="col-10">
                    <input class="form-control" type="text" value="<?php echo e($desc); ?>" name="desc[]">
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

            <div class="form-group row">
                <label class="col-form-label col-2">Urutan</label>
                <div class="col-10">
                    <input class="form-control" type="number" value="<?php echo e($data->order); ?>" name="order" required>
                </div>
            </div>
        </div>

        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-10">
                        <button class="btn btn-success" type="submit">Submit</button>
                        <button type="reset" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('script'); ?>
<script src="<?php echo asset("/js/jquery-ui.js"); ?>"></script>
<script src="<?php echo asset("/js/jquery.fileupload.js"); ?>"></script>
<script>
    $(function() {
        $('#fileupload').fileupload({
            url: '<?php echo route("media.ajaxstore"); ?>',
            // type:"POST",
            dataType: 'json',
            done: function(e, data) {
                var dataRes = data.result;
                if (dataRes.status == 1) {
                    $('.uploadnew').addClass('d-none');
                    $('.changeupload').removeClass('d-none');
                    $('.preview-pic').html('<img width="100%" src="<?php echo url("files"); ?>/' + dataRes
                        .path + '" />');
                    $('fileinput-button').html('Ganti Gambar');
                    $('#imageIdnews').val(dataRes.imageId);
                    toastr.success(dataRes.message, "Success");
                } else {
                    toastr.error(dataRes.message, "Failed");
                }
            }
        });
    });
    $(document).on('click', '.pilihgambar', function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var $modal = $('#modal-full');
        $modal.find(".modal-title").html("Pilih Gambar");
        $modal.find(".modal-body").html("");
        axios.get("<?php echo route('media.modal'); ?>")
        .then(response => {
            $modal.find(".modal-body").html(response.data);
        });
        $modal.find(".modal-footer").html('');
        $modal.modal('show');
    });
    $(document).on('click', '.choosepicture', function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var $modal = $('#modal-full');
        var id_gambar = $(this).attr('id-gambar');
        var path_gambar = $(this).attr('path-gambar');
        $('.preview-pic').html('<img width="100%" src="<?php echo url("files"); ?>/' + path_gambar + '" />');
        $('#imageIdnews').val(id_gambar);
        $modal.modal('hide');
    });
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('panel.layouts.apps', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\COMPRO\resources\views/panel/slider/edit.blade.php ENDPATH**/ ?>