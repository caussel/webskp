<?php $__env->startPush('header'); ?>
<title><?php echo e($header); ?> - <?php echo CMS::getSetting('tagline'); ?></title>
<meta name="description" content="<?php echo e($header); ?> - PMI Musi Banyuasin" />
<meta name="robots" content="index,nofollow" />
<meta property="og:type" content="article" />
<meta property="og:site_name" content="PMI Musi Banyuasin" />
<meta property="og:title" content="<?php echo e($header); ?> - <?php echo CMS::getSetting('tagline'); ?>" />
<meta name="twitter:title" content="<?php echo e($header); ?> - <?php echo CMS::getSetting('tagline'); ?>" />
<meta property="og:url" content="<?php echo url()->current(); ?>" />
<meta property="og:image" content="<?php echo asset(CMS::getSetting('defaultimage')); ?>" />
<meta property="og:image:type" content="image/jpeg" />
<style>
    .thumb {
        max-height: 300px !important;
    }
</style>
<?php $__env->stopPush(); ?>
<?php $__env->startSection('content'); ?>

<section class="inner-header divider parallax layer-overlay layer-pattern">
    <div class="container pt-10 pb-20">
        <div class="section-content pt-10">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="title text-white"><?php echo e($header); ?> </h3>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container pb-0">
        <div class="section-content">
            <div class="row">
                <div class="col-md-9">
                    <?php if(empty($data)): ?>
                    <div class="icon-box mb-0 p-0">
                        <h3 class="icon-box-title pt-15 mt-0 mb-40">No Result Found</h3>
                        <hr>
                    </div>
                    <?php endif; ?>
                    <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-sm-12 col-md-12 col-lg-12 mb-50">
                        <div class="schedule-box maxwidth500 clearfix mb-30">
                            <div class="col-md-5">
                                <div class="thumb">
                                    <img class="img-fullwidth" alt="" src="<?php echo e(CMS::getImage($item->id_media)); ?>">
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="schedule-details clearfix p-15 pt-30">
                                    <div class="text-center pull-left flip bg-theme-colored p-10 pt-5 pb-5 mr-10">
                                        <ul>
                                            <li class="font-24 text-white font-weight-600 border-bottom ">
                                                <?php echo e($item->created_at->format('d')); ?></li>
                                            <li class="font-18 text-white text-uppercase">
                                                <?php echo e($item->created_at->format('M')); ?></li>
                                        </ul>
                                    </div>
                                    <h3 class="title mt-0 font-20"><a
                                            href="<?php echo e(route('home.showPost',[$item->id,$item->slug])); ?>"><?php echo e($item->judul); ?></a>
                                    </h3>
                                    <span class="mb-10 text-gray-darkgray mr-10 font-13"><i
                                            class="fa fa-commenting-o mr-5 text-theme-colored"></i> <span
                                            class="disqus-comment-count"
                                            data-disqus-url="<?php echo route('home.showPost',['id'=>$item->id,'post'=>$item->slug]); ?>">Comments</span></span>
                                    <span class="mb-10 text-gray-darkgray mr-10 font-13"><i
                                            class="fa fa-tag mr-5 text-theme-colored"></i><?php echo e($item->kategories->name); ?></span>
                                    <div class="clearfix"></div>
                                    <p class="mt-10"><?php echo e(CMS::excerpt($item->content)); ?></p>
                                    <div class="mt-10">
                                        <a href="<?php echo e(route('home.showPost',[$item->id,$item->slug])); ?>"
                                            class="btn btn-default btn-theme-colored mt-10 font-16 btn-sm">Selengkapnya</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
                <div class="col-md-3">
                    <?php
                        $data->id = null;
                    ?>
                    <?php echo $__env->make('home.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('home._app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\COMPRO\resources\views/home/archive.blade.php ENDPATH**/ ?>