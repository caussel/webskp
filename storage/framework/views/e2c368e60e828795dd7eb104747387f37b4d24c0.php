<?php $__env->startSection('content'); ?>
<?php if($errors->any()): ?>
<div class="alert alert-danger">
    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <ul>
        <li><?php echo $error; ?></li>
    </ul>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
<?php endif; ?>
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-gear"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                Settings
            </h3>
        </div>
    </div>
    <div class="kt-portlet__body">
        <form action="<?php echo e(route('setting.store')); ?>" id="f_setting" class="parsley-validated form-horizontal"
            method="POST" enctype="multipart/form-data" parsley-validated="true">
            <?php echo e(csrf_field()); ?>

            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="form-group row">
                <label class="control-label col-sm-2"><?php echo $item['name']; ?></label>
                <div class="col-sm-10">
                    <div class="col-sm-12 <?php echo (!empty($dataSetting[$key]))?'value-'.$dataSetting[$key]['id']:''; ?>"
                        data-name="<?php echo $item['value']; ?>" data-type="<?php echo $item['type']; ?>">
                        <?php switch($item['type']):
                        case ('textarea'): ?>
                        <textarea name="sets[text][<?php echo $item['value']; ?>]" id="" rows="4"
                            class="form-control"><?php echo (!empty($dataSetting[$key])?$dataSetting[$key]['val']:""); ?></textarea>
                        <?php break; ?>
                        <?php case ('file'): ?>
                        <?php if(!empty($dataSetting[$key])): ?>
                        <img class="img-fluid" alt="" id="preview_<?php echo $item['value']; ?>"
                            src="<?php echo asset('files/'.$dataSetting[$key]['val']); ?>" width="145">
                        <button type="button" value-id="<?php echo $dataSetting[$key]['id']; ?>" value-set="<?php echo $item['value']; ?>"
                            class="btn btn-secondary delete_photo" id="btn_<?php echo $item['value']; ?>">Delete</button>
                        <?php else: ?>
                        <input name="sets[file][<?php echo $item['value']; ?>]" type="<?php echo $item['type']; ?>" value=""
                            class="filestyle parsley-validated" data-buttonname="btn-secondary"
                            parsley-filetype-message="File type not allowed" parsley-filetype="jpg|JPG|png|PNG"
                            parsley-error-container="#<?php echo $item['value']; ?>" />
                        <span id="<?php echo $item['value']; ?>"></span>
                        <?php endif; ?>
                        <?php break; ?>
                        <?php case ('select'): ?>
                        <select name="sets[text][<?php echo $item['value']; ?>]" id="" class="form-control select2">
                            <option value="">Pilih</option>
                            <?php $__currentLoopData = $item['options']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $keyOpt => $option): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo $keyOpt; ?>" <?php echo (!empty($dataSetting[$key]) &&
                                $dataSetting[$key]['val']==$keyOpt)?'selected':""; ?>><?php echo $option; ?>

                            </option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>

                        <?php break; ?>
                        <?php case ('hr'): ?>
                        <hr>
                        <h4><?php echo $item['name']; ?></h4>
                        <?php break; ?>
                        <?php default: ?>
                        <input name="sets[text][<?php echo $item['value']; ?>]" type="<?php echo $item['type']; ?>"
                            value="<?php echo (!empty($dataSetting[$key])?$dataSetting[$key]['val']:""); ?>"
                            class="form-control" />
                        <?php endswitch; ?>
                    </div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            <button class="btn btn-info" type="submit">Submit</button>
        </form>

    </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('script'); ?>
<script src="<?php echo asset('/js/parsley.min.js'); ?>"></script>
<script>
    $(function() {
        $('#f_setting').parsley({
            validators: {
                filemaxsize: function() {
                    return {
                        validate: function(val, max_megabytes, parsleyField) {
                            if (!Modernizr.fileapi) {
                                return true;
                            }
                            var file_input = $(parsleyField.element);
                            if (file_input.is(':not(input[type="file"])')) {
                                console.log(
                                    "Validation on max file size only works on file input types"
                                );
                                return true;
                            }
                            var max_bytes = max_megabytes * BYTES_PER_MEGABYTE,
                                files = file_input.get(0).files;
                            if (files.length == 0) {
                                return true;
                            }
                            return files.length == 1 && files[0].size <= max_bytes;
                        },
                        priority: 3
                    };
                },
                filetype: function() {
                    return {
                        validate: function(val, requirement) {
                            var fileExtension = val.split('.').pop();
                            var fileExtensionExplode = requirement.split('|');
                            var checkExt = fileExtensionExplode.indexOf(fileExtension);
                            return fileExtension === fileExtensionExplode[checkExt];
                        },
                        priority: 2
                    };
                }
            },
            messages: {
                filetype: "The File Type not Allowed.",
                requiredfile: "File Required"
            },
            excluded: 'input[type=hidden], :disabled'
        });
    });
    $(document).on('click', '.delete_photo', function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        let idValue = $(this).attr('value-id');
        $('.value-' + idValue).empty();
        axios({
            url: `<?php echo route('setting.deleted'); ?>`,
            data: {
                'data': idValue
            },
            method: "delete"
        }).then(response => {
            if (response.data.status == 1) {
                toastr.success(response.data.message, 'Success :)');
                location.replace("<?php echo route('setting.index'); ?>");
            } else {
                toastr.error(error.data.message, 'Error :(');
            }
        })
    });
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('panel.layouts.apps', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\COMPRO\resources\views/panel/setting/index.blade.php ENDPATH**/ ?>