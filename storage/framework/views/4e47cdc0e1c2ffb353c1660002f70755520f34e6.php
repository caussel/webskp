<?php $__env->startSection('submenu'); ?>
<span class="kt-subheader__separator kt-hidden"></span>
<a href="<?php echo e(route('media.create')); ?>" class="btn btn-label-primary btn-bold btn-sm btn-icon-h kt-margin-l-10">
    <i class="fas fa-plus"></i> Tambah Gambar
</a>
<?php $__env->stopSection(); ?><?php /**PATH C:\xampp\htdocs\COMPRO\resources\views/panel/media/submenu.blade.php ENDPATH**/ ?>