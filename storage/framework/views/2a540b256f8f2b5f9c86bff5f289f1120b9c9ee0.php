<?php echo $__env->make('panel.dataindek.submenu', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->startSection('content'); ?>
<link rel="stylesheet" href=<?php echo asset("jsUpload/css/jquery.fileupload.css"); ?>>
<link rel="stylesheet" href=<?php echo asset("jsUpload/css/jquery.fileupload-ui.css"); ?>>
<?php if($errors->any()): ?>
<div class="alert alert-danger">
    <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <ul>
        <li><?php echo e($error); ?></li>
    </ul>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
<?php endif; ?>
<form class="kt-form kt-form--label-right row" id="f_dataindek" action="<?php echo e(route('dataindek.store')); ?>" method="POST"
    enctype="multipart/form-data">
    <?php echo e(csrf_field()); ?>

    <div class="col-8">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__body">
                <div class="form-group form-group-last">
                    <div class="alert alert-secondary" role="alert">
                        <div class="alert-icon"><i class="flaticon-add kt-font-brand"></i></div>
                        <div class="alert-text">
                            Tambah Data Indek
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-2">Judul Data Indek *</label>
                    <div class="col-10">
                        <input class="form-control" type="text" value="" id="judul" name="judul"
                            placeholder="Judul Data Indek">
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-form-label col-2">Deskripsi</label>
                    <div class="col-10">
                        <textarea rows="4" class="form-control" name="deskripsi"
                        placeholder="Deskripsi"></textarea>
                    </div>
                </div>

                
            </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button class="btn btn-success" type="submit">Submit</button>
                            <button type="reset" class="btn btn-secondary">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Media
                    </h3>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="form-group row">
                    <input type="hidden" id="imageIdnews" value="" name="id_media">
                    <div class="col-12">
                        <div class="preview-pic"></div>
                        <hr />
                    </div>
                    <div class="col-6 text-center">
                        <span class="btn btn-success fileinput-button col-12">
                            <span class="uploadnew ">Upload</span>
                            <span class="changeupload d-none">Ganti</span>
                            <input id="fileupload" type="file" name="files">
                        </span>
                    </div>
                    <div class="col-6 text-center">
                        <a href="#" class="btn btn-primary fileinput-button pilihgambar col-12">
                            Pilih
                        </a>
                        <a href="#" class="btn btn-danger fileinput-button hapusgambar d-none col-12">
                            Hapus
                        </a>
                    </div>
                </div>
                
            </div>
        </div>
   
    </div>
</form>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('script'); ?>
<script src="<?php echo asset("/js/jquery-ui.js"); ?>"></script>
<script src="<?php echo asset("/js/jquery.fileupload.js"); ?>"></script>

<script>
    $(document).on('change', '#position', function(e) {
        let posVal = $('#position').val();
        switch (posVal) {
            case "1":
                $('.ordering').addClass('d-none');
                break;
            case "2":
                $('.ordering').addClass('d-none');
                break;
            default:
                $('.ordering').removeClass('d-none');
                break;
        }
    });
    $(document).ready(function() {
        $("#f_iklan").validate({
            rules: {
                title: {
                    required: !0
                },
                position: {
                    required: !0
                }
            },
            errorPlacement: function(e, r) {
                var i = r.closest(".input-group");
                i.length ? i.after(e.addClass("invalid-feedback")) : r.after(e.addClass(
                    "invalid-feedback"))
            },
            invalidHandler: function(e, r) {
                KTUtil.scrollTop();
            }
        });
    });
    $(function() {
        $('#fileupload').fileupload({
            url: '<?php echo route("media.ajaxstore"); ?>',
            dataType: 'json',
            done: function(e, data) {
                var dataRes = data.result;
                if (dataRes.status == 1) {
                    $('.uploadnew').addClass('d-none');
                    $('.changeupload').removeClass('d-none');
                    $('.preview-pic').html('<img width="100%" src="<?php echo url("files"); ?>/' + dataRes
                        .path + '" />');
                    $('fileinput-button').html('Ganti Gambar');
                    $('#imageIdnews').val(dataRes.imageId);
                    $('.pilihgambar').addClass('d-none');
                    $('.hapusgambar').removeClass('d-none');
                    toastr.success(dataRes.message, "Success");
                } else {
                    toastr.error(dataRes.message, "Failed");
                }
            }
        });
    });
    $(document).on('click', '.pilihgambar', function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var $modal = $('#modal-full');
        $modal.find(".modal-title").html("Pilih Gambar");
        $modal.find(".modal-body").html("");
        $.ajax({
            url: "<?php echo e(route('media.modal')); ?>",
            type: "GET",
            data: {},
            success: function(e) {
                $modal.find(".modal-body").html(e);
            }
        });
        $modal.find(".modal-footer").html('');
        $modal.modal('show');
    });
    $(document).on('click', '.choosepicture', function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        var $modal = $('#modal-full');
        var id_gambar = $(this).attr('id-gambar');
        var path_gambar = $(this).attr('path-gambar');
        $('.preview-pic').html('<img width="100%" src="<?php echo url("files"); ?>/' + path_gambar + '" />');
        $('#imageIdnews').val(id_gambar);
        $('.pilihgambar').addClass('d-none');
        $('.hapusgambar').removeClass('d-none');
        $modal.modal('hide');
    });
    $(document).on('click', '.hapusgambar', function(e) {
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();
        $('.preview-pic').html('');
        $('#imageIdnews').val('');
        $('.pilihgambar').removeClass('d-none');
        $('.hapusgambar').addClass('d-none');
        $('.uploadnew').removeClass('d-none');
        $('.changeupload').addClass('d-none');
    });
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('panel.layouts.apps', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\COMPRO\resources\views/panel/dataindek/create.blade.php ENDPATH**/ ?>