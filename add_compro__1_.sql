-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 02 Jan 2023 pada 06.10
-- Versi server: 10.4.25-MariaDB
-- Versi PHP: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `add_compro`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `agendas`
--

CREATE TABLE `agendas` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start` timestamp NULL DEFAULT NULL,
  `end` timestamp NULL DEFAULT NULL,
  `time_start` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `lokasi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `agendas`
--

INSERT INTO `agendas` (`id`, `title`, `slug`, `start`, `end`, `time_start`, `time_end`, `lokasi`, `description`, `color`, `created_at`, `updated_at`) VALUES
(1, 'Rapat Internal', 'rapat-internal', '2022-11-15 16:00:00', '2022-11-16 16:00:00', NULL, NULL, NULL, '<p>Rapat internal</p>', '#7bd148', '2022-11-16 11:12:33', '2022-11-16 11:12:33');

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `author` int(11) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `id_media` int(11) DEFAULT NULL,
  `ket_photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yt_video` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `view` bigint(20) NOT NULL DEFAULT 0,
  `publish_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`id`, `title`, `slug`, `content`, `author`, `id_kategori`, `id_media`, `ket_photo`, `yt_video`, `status`, `view`, `publish_date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'INI BERITA BARU', 'ini-berita-baru', '<p>ini contoh halaman berita</p>', 1, 1, 2, NULL, NULL, 1, 8, '2022-12-05 01:15:45', '2022-11-16 11:10:21', '2022-12-05 00:15:45', NULL),
(2, 'asa', 'asa', '<p>wqsasas</p>', 1, 1, 2, 'aa', NULL, 1, 6, '2022-11-20 12:26:39', '2022-11-16 13:38:38', '2022-11-20 11:26:39', NULL),
(3, 'dd', 'dd', '<p><code>http://127.0.0.1:8000/files/pdf/tahapan-1-20221117021108.pdf</code></p>', 1, 2, NULL, NULL, NULL, 1, 2, '2022-11-16 22:15:43', '2022-11-16 18:18:16', '2022-11-16 21:15:43', '2022-11-16 21:15:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dataindeks`
--

CREATE TABLE `dataindeks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `judul` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_media` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `dataindeks`
--

INSERT INTO `dataindeks` (`id`, `judul`, `deskripsi`, `id_media`, `created_at`, `updated_at`) VALUES
(1, 'ssz', 'ssz', 3, '2022-11-29 13:44:03', '2022-11-29 13:46:50'),
(3, 'tesss', 'sss', 4, '2022-12-04 23:41:11', '2022-12-04 23:41:11'),
(4, 'dds', 'sdsd', 3, '2022-12-04 23:41:38', '2022-12-04 23:41:38'),
(5, 'dd', 'ss', 4, '2022-12-04 23:41:48', '2022-12-04 23:41:48'),
(6, 'dd', 'ss', 4, '2022-12-04 23:42:08', '2022-12-04 23:42:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `faqs`
--

CREATE TABLE `faqs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `faqs`
--

INSERT INTO `faqs` (`id`, `title`, `slug`, `answer`, `created_at`, `updated_at`) VALUES
(1, 'What ?', 'what', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia dolor rerum enim doloremque iusto eos atque tempora dignissimos similique, quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem quibusdam commodi inventore laborum libero officiis\r\n\r\nAccusantium a laboriosam cumque consequatur voluptates fuga assumenda corporis amet. Vitae placeat architecto ipsa cumque fugiat, atque molestiae perferendis quasi quaerat iste voluptate quas dicta corporis, incidunt quibusdam quia odit unde, rem harum quis! Optio debitis veniam quibusdam, culpa quia, aperiam cupiditate perspiciatis repellat similique saepe magnam quaerat iusto obcaecati doloremque, dolor praesentium.', NULL, NULL),
(2, 'Who', 'who', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam officia dolor rerum enim doloremque iusto eos atque tempora dignissimos similique, quae, maxime sit accusantium delectus, maiores officiis vitae fuga sunt repellendus. Molestiae quae, ducimus ut tenetur nobis id quam autem quibusdam commodi inventore laborum libero officiis\r\n\r\nAccusantium a laboriosam cumque consequatur voluptates fuga assumenda corporis amet. Vitae placeat architecto ipsa cumque fugiat, atque molestiae perferendis quasi quaerat iste voluptate quas dicta corporis, incidunt quibusdam quia odit unde, rem harum quis! Optio debitis veniam quibusdam, culpa quia, aperiam cupiditate perspiciatis repellat similique saepe magnam quaerat iusto obcaecati doloremque, dolor praesentium.', NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `galleries`
--

CREATE TABLE `galleries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `galleries`
--

INSERT INTO `galleries` (`id`, `title`, `created_at`, `updated_at`) VALUES
(2, 'gambar satu', '2022-11-20 08:54:41', '2022-11-20 08:54:41'),
(3, 'gambar dua', '2022-11-20 08:56:04', '2022-11-20 08:56:04'),
(4, 'gambar tiga', '2022-11-20 08:56:24', '2022-11-20 08:56:24'),
(5, 'gambar empat', '2022-11-20 08:57:23', '2022-11-20 08:57:23'),
(6, 'gambar lima', '2022-11-20 08:57:43', '2022-11-20 08:57:43');

-- --------------------------------------------------------

--
-- Struktur dari tabel `gallery_media`
--

CREATE TABLE `gallery_media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gallery_id` bigint(20) NOT NULL,
  `media_id` bigint(20) NOT NULL,
  `photo_desc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `gallery_media`
--

INSERT INTO `gallery_media` (`id`, `gallery_id`, `media_id`, `photo_desc`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 'gambar satu', NULL, NULL),
(2, 3, 3, 'gambar dua', NULL, NULL),
(3, 4, 3, 'gambar tiga', NULL, NULL),
(4, 5, 3, 'gambar empat', NULL, NULL),
(5, 6, 3, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `halamen`
--

CREATE TABLE `halamen` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `judul` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_media` int(11) DEFAULT NULL,
  `ket_photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `halamen`
--

INSERT INTO `halamen` (`id`, `judul`, `slug`, `content`, `id_media`, `ket_photo`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 'PROFIL LEMBAGA', 'profil-lembaga', '<p>KEMENTERIAN PERTANIAN</p>\r\n<p style=\"text-align: justify;\">Berdasarkan Peraturan Presiden Republik Indonesia Nomor 18 Tahun 2020 tentang Rencana Pembangunan Jangka Menengah Nasional (RPJMN) 2020 &ndash; 2024, ditetapkan Visi Presiden dan Wakil Presiden RI 2020 &ndash; 2024 adalah &ldquo;Terwujudnya Indonesia Maju yang Berdaulat,<br />Mandiri dan Berkepribadian berlandaskan Gotong Royong&rdquo;. Untuk mendukung Visi tersebut, maka Kementerian Pertanian menetapkan Visi Pertanian Tahun 2020 &ndash; 2024, yakni:</p>\r\n<p><strong>Pertanian yang Maju, Mandiri dan Modern untuk Terwujudnya Indonesia Maju yang Berdaulat, Mandiri dan Berkepribadian berlandaskan Gotong Royong</strong></p>\r\n<p>Dalam rangka mewujudkan visi ini maka misi Kementerian Pertanian adalah:<br />1. Mewujudkan ketahanan pangan<br />2. Meningkatkan Nilai Tambah dan Daya Saing Pertanian<br />3. Meningkatkan kualitas sumber daya manusia dan prasarana Kementerian Pertanian<br /><br />KARANTINA PERTANIAN</p>\r\n<p style=\"text-align: justify;\">Karantina Pertanian merupakan garda depan pertanian untuk melindungi kelangsungan sumber daya hayati hewani dan nabati. Keberadaan karantina yang strategis mutlak diperlukan karena negara Indonesia merupakan negara agraris dan kepulauan.</p>\r\n<p style=\"text-align: justify;\">Stasiun Karantina Pertanian Kelas II Mamuju Sulawesi Barat pertama kali dibentuk pada tahun 2008 &nbsp;berdasarkan Peraturan Menteri Pertanian Nomor: 22/Permentan/OT.140/4/2008 Tanggal 3 April 2008. Kala itu &nbsp;Stasiun Karantina Pertanian Kelas II Mamuju berkantor di Jl. Martadinata Kec. Simboro, Kab. Mamuju, di lahan dan bangunan yang merupakan hibah dari Pemerintah Provinsi Sulawesi Barat. &nbsp;Tahun 2009 Gedung Stasiun Karantina Pertanian Kelas II Mamuju pindah ke gedung yang baru di Jl. H. Abd. Malik Pattana Endeng, Kec. Simboro, Kab. Mamuju dan gedung itulah yang menjadi kantor induk Stasiun Karantina Pertanian Kelas II Mamuju hingga kini.Awal berdirinya, Stasiun Karantina Pertanian Kelas II Mamuju meliliki 2 Pejabat Struktural yaitu Kepala UPT (eselon IV) dan Kepala Urusan Tata Usaha (eselon V). Namun per Januari 2021 terjadi perubahan Struktur Organisasi lingkup Badan Karantina Pertanian berdasarkan &nbsp;Permentan 47 Tahun 2020 &nbsp;Tanggal 23 Desember 2020 Tentang Organisasi dan Tata Laksana UPT Lingkup Badan Karantina Pertanian , dimana UPT Stasiun Pertanian Kelas I dan Kelas II dirampingkan dan hanya memiliki 1 Pejabat Struktural saja yaitu Kepala UPT yang langsung membawahi Pejabat Fungsional Teknis maupun Non Teknis.</p>\r\n<p>&nbsp;</p>', NULL, NULL, 1, '2022-11-16 17:57:36', '2022-12-05 07:56:44', NULL),
(3, 'VISI DAN MISI', 'visi-dan-misi', '<p>VISI STASIUN KARANTINA PERTANIAN KELAS II MAMUJU</p>\r\n<p><strong>&nbsp;&rdquo; Pertanian yang Maju, Mandiri dan Modern untuk Terwujudnya Indonesia Maju yang Berdaulat, Mandiri dan Berkepribadian Berlandaskan Gotong Royong &ldquo;</strong></p>\r\n<p>MISI STASIUN KARANTINA PERTANIAN KELAS II MAMUJU<br />1. Peningkatan Kualitas Manusia Indonesia;<br />2. Struktur Ekonomi yang Produktif, Mandiri, dan Berdaya Saing;<br />3. Pembangunan yang Merata dan Berkeadilan;<br />4. Mencapai Lingkungan Hidup yang Berkelanjutan;<br />5. Kemajuan Budaya yang Mencerminkan Kepribadian Bangsa;<br />6. Penegakan Sistem Hukum yang Bebas Korupsi, Bermartabat, dan Terpercaya;<br />7.Perlindungan Bagi Segenap Bangsa dan Memberikan Rasa Aman Pada Seluruh Warga;<br />8. Pengelolaan Pemerintahan yang Bersih, Efektif, dan Terpercaya;<br />9. Sinergi Pemerintah dalam Kerangka Negara Kesatuan.</p>', NULL, NULL, 1, '2022-12-05 04:33:03', '2022-12-05 04:33:03', NULL),
(4, 'TUGAS POKOK DAN FUNGSI', 'tugas-pokok-dan-fungsi', '<p>DASAR HUKUM :</p>\r\n<p style=\"padding-left: 40px;\">1. Undang-Undang Nomor 21 Tahun 2019 Tentang Karantina Hewan, Ikan dan Tumbuhan.<br />2. Peraturan Pemerintah Nomor &nbsp;82 Tahun 2000 Tentang Karantina Hewan.<br />3. Peraturan Pemerintah Nomor 14 Tahun 2002 Tentang Karantina Tumbuhan.<br />4. Undang-Undang Nomor 25 Tahun 2009 Tentang Pelayanan Publik.<br />5. Undang &ndash; Undang No. 14 tahun 2008 tentang Keterbukaan Informasi Publik</p>\r\n<p>Stasiun Karantina Pertanian Kelas II Mamuju mempunyai tugas :</p>\r\n<p>&ldquo;melaksanakan kegiatan operasional perkarantinaan hewan dan tumbuhan serta pengawasan keamanan hayati hewani dan nabati&rdquo;</p>\r\n<p>Stasiun Karantina Pertanian Kelas II Mamuju menyelenggarakan fungsi :</p>\r\n<p>Penyusunan rencana, evaluasi dan pelaporan.</p>\r\n<p>Pelaksanaan pemeriksaan, pengasingan, pengamatan, perlakuan, penahanan, penolakan, pemusnahan dan pembebasan media pembawa organisme penganggu tumbuhan karantina (OPTK) dan media pembawa hama penyakit hewan karantina (HPHK).</p>\r\n<p style=\"padding-left: 40px;\">1. Pelaksanaan pemantauan daerah sebar HPHK dan OPTK<br />2. Pelaksanaan pembuatan koleksi HPHK dan OPTK.<br />3. Pelaksanaan pengawasan keamanan hayati hewani dan nabati.<br />4. Pelaksanaan pemberian pelayanan operasional karantina hewan dan tumbuhan.<br />5. Pelaksanaan pemberian pelayanan operasional pengawasan keamanan hayati hewani dan tumbuhan.<br />6. Pengelolaan sistem informasi, dokumentasi, dan sarana teknik karantina hewan dan tumbuhan.<br />7. Pelaksanaan pengawasan dan penindakan pelanggaran peraturan perundang-undangan di bidang&nbsp; &nbsp; &nbsp; karantina hewan, karantina tumbuhan dan keamanan hayati hewani dan nabati.<br />8. Sub Bagian Tata Usaha mempunyai tugas melaksanakan penyusunan rencana, evaluasi dan pelaporan, serta urusan tata usaha dan rumah tangga.<br />9. Seksi Karantina Hewan mempunyai tugas memberi pelayanan operasional karantina hewan, pengawasan keamanan hayati hewani dan sarana teknik serta pengelolaan sistem informasi dan dokumentasi serta pengawasan dan penindakan pelangaran peraturan perundang-undangan di bidang karantina hewan dan keamanan hayati.<br />10. Seksi Karantina Tumbuhan mempunyai tugas memberi pelayanan operasional karantina tumbuhan, pengawasan keamanan hayati nabati dan sarana teknik serta pengelolaan sistem informasi dan dokumentasi serta pengawasan dan penindakan pelangaran peraturan perundang-undangan di bidang karantinatumbuhan dan keamanan hayati nabati.</p>', NULL, NULL, 1, '2022-12-05 04:46:42', '2022-12-05 04:46:42', NULL),
(5, 'STRUKTUR ORGANISASI', 'struktur-organisasi', NULL, 7, NULL, 1, '2022-12-05 04:49:28', '2022-12-05 04:49:28', NULL),
(6, 'PROFIL PEJABAT STRUKTURAL', 'profil-pejabat-struktural', NULL, NULL, NULL, 1, '2022-12-05 04:55:25', '2022-12-05 04:55:25', NULL),
(7, 'WILAYAH KERJA', 'wilayah-kerja', NULL, NULL, NULL, 1, '2022-12-05 04:56:04', '2022-12-05 04:56:04', NULL),
(8, 'JALUR EVAKUASI KEADAAN DARURAT', 'jalur-evakuasi-keadaan-darurat', NULL, 8, NULL, 1, '2022-12-05 04:56:52', '2022-12-05 04:56:52', NULL),
(9, 'LHKPN DAN LHKSN', 'lhkpn-dan-lhksn', NULL, NULL, NULL, 1, '2022-12-05 04:57:43', '2022-12-05 04:57:43', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `iklans`
--

CREATE TABLE `iklans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tautan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_media` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` int(11) NOT NULL,
  `order` int(11) DEFAULT NULL,
  `script` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `iklans`
--

INSERT INTO `iklans` (`id`, `title`, `tautan`, `id_media`, `position`, `order`, `script`, `status`, `created_at`, `updated_at`) VALUES
(2, 'dd', 'dd', '3', 2, NULL, NULL, 1, '2022-11-16 21:57:13', '2022-11-16 22:08:44');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Tidak Berkategori', 'tidak-berkategori', NULL, NULL),
(2, 'kategori contoh', 'kategori-contoh', '2022-11-16 11:39:25', '2022-11-16 11:39:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `layanans`
--

CREATE TABLE `layanans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_media` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `layanans`
--

INSERT INTO `layanans` (`id`, `nama`, `link`, `id_media`, `created_at`, `updated_at`) VALUES
(1, 'Prosedur Layanan', 'https://mamuju.karantina.pertanian.go.id/prosedur-pelayanan/', 9, NULL, '2022-12-05 07:23:13'),
(2, 'Persyaratan', 'https://mamuju.karantina.pertanian.go.id/persyaratan-karantina-hewan-dan-tumbuhan/', 10, '2022-11-28 10:26:26', '2022-12-05 07:23:48'),
(3, 'Tarif PNBP', 'https://mamuju.karantina.pertanian.go.id/tarif-pnbp/', 11, '2022-12-05 05:02:47', '2022-12-05 07:24:03'),
(4, 'Produk Layanan', 'https://mamuju.karantina.pertanian.go.id/layanan-stasiun-karantina-pertanian-kelas-ii-mamuju/', 12, '2022-12-05 05:03:40', '2022-12-05 07:24:25'),
(5, 'Wilayah Kerja', 'https://mamuju.karantina.pertanian.go.id/wilayah-kerja/', 13, '2022-12-05 05:04:14', '2022-12-05 07:24:41'),
(6, 'MEDIA HPHK', 'https://mamuju.karantina.pertanian.go.id/media-hphk/', 14, '2022-12-05 05:18:40', '2022-12-05 07:25:28'),
(7, 'MEDIA OPTK', 'https://mamuju.karantina.pertanian.go.id/media-optk/', 15, '2022-12-05 05:18:50', '2022-12-05 07:26:07'),
(8, 'PPK Online', 'https://ppkonline.karantina.pertanian.go.id/', 16, '2022-12-05 05:19:02', '2022-12-05 07:26:53'),
(9, 'Pengaduan Masyarakat', 'https://mamuju.karantina.pertanian.go.id/tata-cara-pengaduan/', 17, '2022-12-05 05:19:14', '2022-12-05 07:27:41'),
(10, 'Survey Kepuasan Masyarakat', 'https://ikm.pertanian.go.id/?u=G50', 18, '2022-12-05 05:19:24', '2022-12-05 07:28:24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `layouts`
--

CREATE TABLE `layouts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `layouts`
--

INSERT INTO `layouts` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'beranda', 'beranda', '2022-11-16 11:06:06', '2022-11-20 09:06:35'),
(3, 'Sub Berita', 'sub-berita', '2022-11-20 21:44:01', '2022-11-20 21:44:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `link_terkaits`
--

CREATE TABLE `link_terkaits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_media` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `link_terkaits`
--

INSERT INTO `link_terkaits` (`id`, `nama`, `link`, `id_media`, `created_at`, `updated_at`) VALUES
(2, 'Bungas', 'ssssssxs', 2, '2022-11-28 23:44:45', '2022-11-29 04:33:17'),
(3, 'Bunga', 'ssssssx', 2, '2022-11-28 23:44:45', '2022-11-28 23:44:45'),
(4, 'Bunga', 'ssssssx', 2, '2022-11-28 23:44:45', '2022-11-28 23:44:45'),
(5, 'Bunga', 'ssssssx', 2, '2022-11-28 23:44:45', '2022-11-28 23:44:45'),
(6, 'Bunga', 'ssssssx', 2, '2022-11-28 23:44:45', '2022-11-28 23:44:45'),
(7, 'Bunga', 'ssssssx', 2, '2022-11-28 23:44:45', '2022-11-28 23:44:45'),
(8, 'Bunga', 'ssssssx', 2, '2022-11-28 23:44:45', '2022-11-28 23:44:45'),
(9, 'Bunga', 'ssssssx', 2, '2022-11-28 23:44:45', '2022-11-28 23:44:45');

-- --------------------------------------------------------

--
-- Struktur dari tabel `media`
--

CREATE TABLE `media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path_220` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `media`
--

INSERT INTO `media` (`id`, `path`, `path_220`, `mime`, `created_at`, `updated_at`) VALUES
(2, 'foto/Welcome-Scan_20221116214753.jpg', 'foto/Welcome-Scan_20221116214753-resize-220.jpg', 'image/jpeg', '2022-11-16 13:47:53', '2022-11-16 13:47:53'),
(3, 'foto/Welcome-Scan_20221117051256.jpg', 'foto/Welcome-Scan_20221117051256-resize-220.jpg', 'image/jpeg', '2022-11-16 21:12:58', '2022-11-16 21:12:58'),
(4, 'foto/bd29854437aa3d48d191d224ac131eb8_20221129112118.jpg', 'foto/bd29854437aa3d48d191d224ac131eb8_20221129112118-resize-220.jpg', 'image/jpeg', '2022-11-29 04:21:19', '2022-11-29 04:21:19'),
(5, 'foto/bd29854437aa3d48d191d224ac131eb8_20221130081925.jpg', 'foto/bd29854437aa3d48d191d224ac131eb8_20221130081925-resize-220.jpg', 'image/jpeg', '2022-11-30 01:19:25', '2022-11-30 01:19:25'),
(6, 'foto/1500x500_20221205122400.jpg', 'foto/1500x500_20221205122400-resize-220.jpg', 'image/jpeg', '2022-12-05 04:24:00', '2022-12-05 04:24:00'),
(7, 'foto/2-Struktur-Organisasi-scaled-1024x592-1_20221205124923.jpg', 'foto/2-Struktur-Organisasi-scaled-1024x592-1_20221205124923-resize-220.jpg', 'image/jpeg', '2022-12-05 04:49:23', '2022-12-05 04:49:23'),
(8, 'foto/Jalur-Evakuasi-768x557-1_20221205125648.jpg', 'foto/Jalur-Evakuasi-768x557-1_20221205125648-resize-220.jpg', 'image/jpeg', '2022-12-05 04:56:48', '2022-12-05 04:56:48'),
(9, 'foto/planning_20221205130014.png', 'foto/planning_20221205130014-resize-220.png', 'image/png', '2022-12-05 05:00:14', '2022-12-05 05:00:14'),
(10, 'foto/folder_20221205130135.png', 'foto/folder_20221205130135-resize-220.png', 'image/png', '2022-12-05 05:01:36', '2022-12-05 05:01:36'),
(11, 'foto/indonesian-rupiah_20221205130246.png', 'foto/indonesian-rupiah_20221205130246-resize-220.png', 'image/png', '2022-12-05 05:02:46', '2022-12-05 05:02:46'),
(12, 'foto/certificate_20221205130339.png', 'foto/certificate_20221205130339-resize-220.png', 'image/png', '2022-12-05 05:03:39', '2022-12-05 05:03:39'),
(13, 'foto/location_20221205130413.png', 'foto/location_20221205130413-resize-220.png', 'image/png', '2022-12-05 05:04:13', '2022-12-05 05:04:13'),
(14, 'foto/cow_20221205152508.png', 'foto/cow_20221205152508-resize-220.png', 'image/png', '2022-12-05 07:25:09', '2022-12-05 07:25:09'),
(15, 'foto/plant_20221205152605.png', 'foto/plant_20221205152605-resize-220.png', 'image/png', '2022-12-05 07:26:05', '2022-12-05 07:26:05'),
(16, 'foto/resume_20221205152640.png', 'foto/resume_20221205152640-resize-220.png', 'image/png', '2022-12-05 07:26:41', '2022-12-05 07:26:41'),
(17, 'foto/customer-review_20221205152740.png', 'foto/customer-review_20221205152740-resize-220.png', 'image/png', '2022-12-05 07:27:40', '2022-12-05 07:27:40'),
(18, 'foto/completed-task_20221205152821.png', 'foto/completed-task_20221205152821-resize-220.png', 'image/png', '2022-12-05 07:28:21', '2022-12-05 07:28:21'),
(19, 'foto/316204016_5130428887058508_6251009947810576252_n_20221205160226.jpg', 'foto/316204016_5130428887058508_6251009947810576252_n_20221205160226-resize-220.jpg', 'image/jpeg', '2022-12-05 08:02:26', '2022-12-05 08:02:26');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menus`
--

CREATE TABLE `menus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` bigint(20) UNSIGNED DEFAULT NULL,
  `order` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `layout_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `menus`
--

INSERT INTO `menus` (`id`, `title`, `slug`, `parent_id`, `order`, `layout_id`, `created_at`, `updated_at`) VALUES
(2, 'LAYANAN', 'layanan', NULL, 2, 1, '2022-11-16 11:29:15', '2022-11-16 21:25:40'),
(3, 'INFORMASI PUBLIK', 'informasi', NULL, 3, 1, '2022-11-16 11:29:49', '2022-11-16 21:25:16'),
(4, 'LEMBAGA', '#', NULL, 1, 1, '2022-11-16 11:32:11', '2022-11-16 21:25:40'),
(5, 'BERANDA', 'http://127.0.0.1:8000/', NULL, 0, 1, '2022-11-16 11:32:26', '2022-11-16 17:59:11'),
(6, 'REGULASI', '#', NULL, 4, 1, '2022-11-16 11:32:45', '2022-11-16 21:25:40'),
(7, 'PORTAL PPID', 'https://skp2mamuju-ppid.pertanian.go.id/', NULL, 5, 1, '2022-11-16 11:33:00', '2022-11-28 07:51:17'),
(10, 'PROFIL DAN SEJARAH LEMBAGA', 'http://127.0.0.1:8000/p/profil-lembaga/', 4, 0, 1, '2022-11-16 17:54:14', '2022-11-16 21:25:40'),
(13, 'BERITA', 'berita', 3, 1, 1, '2022-11-20 11:52:38', '2022-12-23 12:22:43'),
(14, 'Sub Berita', NULL, 13, 0, 2, '2022-11-20 11:53:12', '2022-11-20 11:53:12'),
(15, 'Satu', NULL, NULL, 0, 3, '2022-11-20 21:44:17', '2022-11-20 21:44:30'),
(17, 'tiga', NULL, NULL, 2, 3, '2022-11-20 21:44:28', '2022-11-20 21:44:30'),
(20, 'VISI DAN MISI', 'http://127.0.0.1:8000/p/visi-dan-misi', 4, 1, 1, '2022-12-05 04:27:17', '2022-12-05 04:30:54'),
(21, 'MOTTO DAN MAKLUMAT PELAYANAN', 'http://127.0.0.1:8000/p/motto-dan-maklumat-pelayanan', 4, 2, 1, '2022-12-05 04:35:07', '2022-12-05 04:35:18'),
(22, 'TUGAS POKOK DAN FUNGSI', 'http://127.0.0.1:8000/p/tugas-pokok-dan-fungsi', 4, 3, 1, '2022-12-05 04:42:45', '2022-12-05 04:47:25'),
(23, 'STRUKTUR ORGANISASI', 'http://127.0.0.1:8000/p/struktur-organisasi', 4, 4, 1, '2022-12-05 04:48:34', '2022-12-05 04:49:53'),
(24, 'PROFIL PEJABAT STRUKTURAL', 'http://127.0.0.1:8000/p/profil-pejabat-struktural', 4, 5, 1, '2022-12-05 04:51:09', '2022-12-05 04:52:11'),
(25, 'WILAYAH KERJA', 'http://127.0.0.1:8000/p/wilayah-kerja', 4, 6, 1, '2022-12-05 04:52:02', '2022-12-05 04:52:11'),
(26, 'JALUR EVAKUASI KEADAAN DARURAT', 'http://127.0.0.1:8000/p/jalur-evakuasi-keadaan-darurat', 4, 7, 1, '2022-12-05 04:53:17', '2022-12-05 04:53:34'),
(27, 'LHKPN DAN LHKSN', 'http://127.0.0.1:8000/p/lhkpn-dan-lhksn', 4, 8, 1, '2022-12-05 04:54:12', '2022-12-05 04:54:25'),
(28, 'PROSEDUR PELAYANAN', 'http://127.0.0.1:8000/p/prosedur-pelayanan', 2, 0, 1, '2022-12-05 07:31:43', '2022-12-05 07:32:35'),
(29, 'JAMINAN KEPASTIAN DAN KEAMANAN PELAYANAN', 'http://127.0.0.1:8000/p/jaminan-kepastian-dan-keamanan-pelayanan', 2, 1, 1, '2022-12-05 07:32:23', '2022-12-05 07:32:35'),
(30, 'KARANTINA HEWAN', NULL, 2, 2, 1, '2022-12-05 07:33:07', '2022-12-05 07:34:00'),
(31, 'PERSYARATAN', 'http://127.0.0.1:8000/p/persyaratan-karantina-hewan-dan-tumbuhan', 30, 0, 1, '2022-12-05 07:33:52', '2022-12-05 07:34:00'),
(32, 'LAYANAN', 'http://127.0.0.1:8000/p/layanan-stasiun-karantina-pertanian-kelas-ii-mamuju', 30, 1, 1, '2022-12-05 07:34:49', '2022-12-05 07:35:06'),
(33, 'TARIF PNBP', 'http://127.0.0.1:8000/p/visi-dan-misi', 30, 2, 1, '2022-12-05 07:35:52', '2022-12-05 07:40:32'),
(34, 'MEDIA HPHK', 'http://127.0.0.1:8000/p/media-hphk', 30, 3, 1, '2022-12-05 07:38:19', '2022-12-05 07:40:32'),
(35, 'INSTALASI KARANTINA HEWAN', 'http://127.0.0.1:8000/p/instalasi-karantina-hewan', 30, 4, 1, '2022-12-05 07:39:06', '2022-12-05 07:40:32'),
(36, 'DATA OPERASIONAL', 'http://127.0.0.1:8000/p/data-operasional-karantina-hewan', 30, 5, 1, '2022-12-05 07:39:45', '2022-12-05 07:40:32'),
(37, 'PERSYARATAN MASA PMK', 'http://127.0.0.1:8000/p/persyaratan-masa-pmhk', 30, 6, 1, '2022-12-05 07:40:28', '2022-12-05 07:41:02'),
(38, 'KARANTINA TUMBUHAN', NULL, 2, 3, 1, '2022-12-05 07:42:02', '2022-12-05 07:43:11'),
(39, 'PERSYARATAN', 'http://127.0.0.1:8000/p/persyaratan-karantina-hewan-dan-tumbuhan', 38, 0, 1, '2022-12-05 07:43:02', '2022-12-05 07:43:11'),
(40, 'LAYANAN', 'http://127.0.0.1:8000/p/layanan-stasiun-karantina-pertanian-kelas-ii-mamuju', 38, 1, 1, '2022-12-05 07:44:31', '2022-12-05 07:45:25'),
(41, 'TARIF PNBP', 'http://127.0.0.1:8000/p/tarif-pnbp', 38, 2, 1, '2022-12-05 07:45:18', '2022-12-05 07:45:25'),
(42, 'MEDIA OPTK', 'http://127.0.0.1:8000/p/media-optk', 38, 3, 1, '2022-12-05 07:46:02', '2022-12-05 07:46:54'),
(43, 'DATA OPERASIONAL', 'http://127.0.0.1:8000/p/data-operasional-karantina-tumbuhan', 38, 4, 1, '2022-12-05 07:46:47', '2022-12-05 07:46:54'),
(44, 'LABORATORIUM', NULL, 2, 4, 1, '2022-12-05 07:47:25', '2022-12-05 07:48:16'),
(45, 'PROFIL LAB', 'http://127.0.0.1:8000/p/profil-lab', 44, 0, 1, '2022-12-05 07:48:09', '2022-12-05 07:48:16'),
(46, 'DATA PENGUJIAN KH', 'http://127.0.0.1:8000/p/data-pengujian-kh', 44, 1, 1, '2022-12-05 07:48:49', '2022-12-05 07:48:55'),
(47, 'DATA PENGUJIAN KT', 'http://127.0.0.1:8000/p/data-pengujian-kt', 44, 2, 1, '2022-12-05 07:49:25', '2022-12-05 07:50:04'),
(48, 'SI-MALAQBI', 'https://bbuskp.karantina.pertanian.go.id/simalaqbi/login', 44, 3, 1, '2022-12-05 07:49:55', '2022-12-05 07:50:04'),
(49, 'SURVEY KEPUASAN MASYARAKAT', 'https://ikm.pertanian.go.id/?u=G50', 2, 5, 1, '2022-12-05 07:50:51', '2022-12-05 07:52:29'),
(50, 'PENGADUAN MASYARAKAT', 'http://127.0.0.1:8000/p/tata-cara-pengaduan', 2, 6, 1, '2022-12-05 07:51:31', '2022-12-05 07:52:29'),
(51, 'DASAR HUKUM PELAYANAN', 'http://127.0.0.1:8000/p/dasar-hukum-pelayanan', 2, 7, 1, '2022-12-05 07:52:18', '2022-12-05 07:52:29'),
(52, 'AGENDA KEGIATAN', 'http://127.0.0.1:8000/p/agenda-kegiatan/', 3, 0, 1, '2022-12-23 12:16:11', '2022-12-23 12:22:43'),
(53, 'PENGUMUMAN', 'http://127.0.0.1:8000/p/pengumuman', 3, 2, 1, '2022-12-23 12:16:47', '2022-12-23 12:22:43'),
(54, 'DAFTAR INFORMASI PUBLIK', NULL, 3, 3, 1, '2022-12-23 12:17:23', '2022-12-23 12:22:43'),
(55, 'INFORMASI SETIAP SAAT', 'http://127.0.0.1:8000/p/informasi-setiap-saat', 54, 0, 1, '2022-12-23 12:18:21', '2022-12-23 12:22:43'),
(56, 'INFORMASI BERKALA', 'http://127.0.0.1:8000/p/informasi-berkala', 54, 1, 1, '2022-12-23 12:19:11', '2022-12-23 12:22:43'),
(57, 'INFORMASI SERTA MERTA', 'http://127.0.0.1:8000/p/informasi-serta-merta/', 54, 2, 1, '2022-12-23 12:19:49', '2022-12-23 12:22:43'),
(58, 'STANDAR PELAYANAN PUBLIK', 'http://127.0.0.1:8000/p/standar-pelayanan-publik', 3, 4, 1, '2022-12-23 12:20:32', '2022-12-23 12:22:43'),
(59, 'TATA CARA PENGADUAN', 'http://127.0.0.1:8000/p/tata-cara-pengaduan', 3, 5, 1, '2022-12-23 12:21:30', '2022-12-23 12:22:43'),
(60, 'PERINGATAN DINI DAN HIMBAUAN', 'http://127.0.0.1:8000/p/peringatan-dini-dan-himbauan', 3, 6, 1, '2022-12-23 12:22:20', '2022-12-23 12:22:43'),
(61, 'PERATURAN KEMENTERIAN PERTANIAN', 'https://perundangan.pertanian.go.id/', 6, 0, 1, '2022-12-23 12:23:23', '2022-12-23 12:26:41'),
(62, 'PERATURAN KARANTINA PERTANIAN (E-LEGISLASI)', 'https://karantina.pertanian.go.id/hukum/index.php', 6, 1, 1, '2022-12-23 12:23:51', '2022-12-23 12:26:41'),
(63, 'DAFTAR KEPUTUSAN', 'http://127.0.0.1:8000/p/daftar-keputusan', 6, 2, 1, '2022-12-23 12:24:34', '2022-12-23 12:26:41'),
(64, 'DAFTAR RANCANGAN KEPUTUSAN', 'http://127.0.0.1:8000/p/daftar-rancangan-keputusan', 6, 3, 1, '2022-12-23 12:25:10', '2022-12-23 12:26:41'),
(65, 'MOU', 'http://127.0.0.1:8000/p/memorandum-of-understanding-mou', 6, 4, 1, '2022-12-23 12:25:47', '2022-12-23 12:26:41'),
(66, 'SOP', 'http://127.0.0.1:8000/p/standar-operasional-prosedur-sop', 6, 5, 1, '2022-12-23 12:26:26', '2022-12-23 12:26:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_26_181129_create_berita_table', 1),
(5, '2019_12_27_062157_create_kategoris_table', 1),
(6, '2019_12_27_112036_create_tags_table', 1),
(7, '2019_12_28_091804_create_media_table', 1),
(8, '2019_12_28_113632_create_terms_table', 1),
(9, '2019_12_30_073935_create_pdfs_table', 1),
(10, '2019_12_30_150414_create_menus_table', 1),
(11, '2019_12_31_085432_create_layouts_table', 1),
(12, '2020_01_01_085128_create_agendas_table', 1),
(13, '2020_01_09_005724_create_sliders_table', 1),
(14, '2020_01_09_014855_create_halamen_table', 1),
(15, '2020_02_14_172344_create_pengumumen_table', 1),
(16, '2020_02_15_003716_create_iklans_table', 1),
(17, '2020_02_15_222932_create_pesans_table', 1),
(18, '2020_02_16_163552_create_galleries_table', 1),
(19, '2020_02_16_164302_create_gallery_media_table', 1),
(20, '2020_09_17_190715_create_settings_table', 1),
(24, '2022_11_28_115928_create_faqs_table', 2),
(25, '2022_11_28_141041_create_layanans_table', 2),
(26, '2022_11_29_055901_create_link_terkaits_table', 3),
(27, '2022_11_29_203216_create_dataindeks_table', 4),
(28, '2022_11_30_063318_create_visitors_table', 5),
(29, '2022_12_01_062057_create_pengunjungs_table', 6),
(30, '2022_12_01_062702_create_perjanjians_table', 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pdfs`
--

CREATE TABLE `pdfs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengumumen`
--

CREATE TABLE `pengumumen` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_media` int(11) DEFAULT NULL,
  `id_pdf` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `view` bigint(20) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pengumumen`
--

INSERT INTO `pengumumen` (`id`, `title`, `slug`, `content`, `id_media`, `id_pdf`, `status`, `view`, `created_at`, `updated_at`) VALUES
(1, 'Contoh Pengumuman', 'contoh-pengumuman', '<p>ini contoh pengumuman</p>', 1, 1, 1, 5, '2022-11-16 11:15:47', '2022-11-16 22:06:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengunjungs`
--

CREATE TABLE `pengunjungs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kartu_identitas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jam` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tujuan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `komentar` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `pengunjungs`
--

INSERT INTO `pengunjungs` (`id`, `nama`, `alamat`, `kartu_identitas`, `tanggal`, `jam`, `tujuan`, `komentar`, `created_at`, `updated_at`) VALUES
(3, 'Berita Duka', 'Ciamisw', 'bukutamu/pengunjung/bd29854437aa3d48d191d224ac131eb8_20221201065341.jpg', '2022-12-01', '06:52', 's', 'sssssss', '2022-11-30 23:53:41', '2022-12-03 07:16:18');

-- --------------------------------------------------------

--
-- Struktur dari tabel `perjanjians`
--

CREATE TABLE `perjanjians` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `kartu_identitas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jam` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tujuan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `komentar` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `perjanjians`
--

INSERT INTO `perjanjians` (`id`, `nama`, `alamat`, `kartu_identitas`, `tanggal`, `jam`, `tujuan`, `status`, `komentar`, `created_at`, `updated_at`) VALUES
(1, 'Bunga', 'Ciamisw', 'bukutamu/perjanjian/bd29854437aa3d48d191d224ac131eb8_20221201065641.jpg', '2022-12-01', '06:57', 'TES', '1', 'Komentar', '2022-11-30 23:56:41', '2022-12-03 07:11:02');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pesans`
--

CREATE TABLE `pesans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `perihal` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `val` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `settings`
--

INSERT INTO `settings` (`id`, `name`, `val`, `group`, `created_at`, `updated_at`) VALUES
(1, 'title', 'company profile', 'default', '2022-11-16 11:23:21', '2022-11-22 22:56:53'),
(2, 'tagline', 'Karantina Pertanian Mamuju', 'default', '2022-11-16 11:23:21', '2022-12-05 04:21:36'),
(3, 'description', NULL, 'default', '2022-11-16 11:23:21', '2022-11-22 22:56:53'),
(4, 'keyword', 'Karantina Pertanian Mamuju', 'default', '2022-11-16 11:23:21', '2022-12-05 04:21:36'),
(5, 'phone', '(0426) – 2321867', 'default', '2022-11-16 11:23:21', '2022-12-05 00:08:27'),
(6, 'email', 'skpmamuju@gmail.com', 'default', '2022-11-16 11:23:21', '2022-12-05 00:08:27'),
(7, 'address', 'Jl. H. Abd. Malik Pattana Endeng KM.1, Kec. Simboro, Kab. Mamuju – Sulawesi Barat , KP. 91512', 'default', '2022-11-16 11:23:21', '2022-12-05 00:08:27'),
(8, 'facebook', 'https://www.facebook.com/stasiunkarantinapertanian.mamuju', 'default', '2022-11-16 11:23:21', '2022-12-05 00:08:27'),
(9, 'twitter', 'https://twitter.com/karantinamamuju', 'default', '2022-11-16 11:23:21', '2022-12-05 00:08:27'),
(10, 'instagram', 'https://www.instagram.com/karantinapertanianmamuju/', 'default', '2022-11-16 11:23:21', '2022-12-05 00:08:27'),
(11, 'yt_channel', 'https://www.youtube.com/channel/UCySK4aXVPCPrmhAZafBxI5w', 'default', '2022-11-16 11:23:21', '2022-12-05 00:08:27'),
(12, 'header_script', NULL, 'default', '2022-11-16 11:23:21', '2022-11-16 11:23:21'),
(13, 'menu_top', 'beranda', 'default', '2022-11-16 11:23:21', '2022-11-16 11:23:21'),
(14, 'menu_bottom', NULL, 'default', '2022-11-16 11:23:21', '2022-11-16 17:48:48'),
(15, 'site_link', NULL, 'default', '2022-11-16 11:23:21', '2022-11-16 11:23:21'),
(16, 'slideshow', NULL, 'default', '2022-11-16 11:23:21', '2022-11-16 11:23:21'),
(17, 'logo', 'assets/img_ewll4htNGul9LYc_16112022192437.jpg', 'default', '2022-11-16 11:24:37', '2022-11-16 11:24:37'),
(18, 'logo_footer', 'assets/img_tOVJT7Ydu5bOsDH_05122022080922.png', 'default', '2022-12-05 00:09:22', '2022-12-05 00:09:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sliders`
--

CREATE TABLE `sliders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_media` bigint(20) NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `id_media`, `desc`, `order`, `created_at`, `updated_at`) VALUES
(1, 'a', 3, '[\"q\",\"q\",\"q\"]', 2, '2022-11-16 13:47:01', '2022-12-05 04:24:34'),
(2, 'Karantina Pertanian Mamuju', 6, '[null,null,null]', 1, '2022-12-05 04:24:09', '2022-12-05 04:24:09');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `terms`
--

CREATE TABLE `terms` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_post` int(11) NOT NULL,
  `id_terms` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'admin@gmail.com', '2022-11-15 12:04:35', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `visitors`
--

CREATE TABLE `visitors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `visitors`
--

INSERT INTO `visitors` (`id`, `ip`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', '2022-11-29 23:40:31', '2022-11-29 23:40:31'),
(2, '127.0.0.1', '2022-11-29 23:40:35', '2022-11-29 23:40:35'),
(3, '127.0.0.1', '2022-11-29 23:40:38', '2022-11-29 23:40:38'),
(4, '127.0.0.1', '2022-11-29 23:43:01', '2022-11-29 23:43:01'),
(5, '127.0.0.1', '2022-11-29 23:43:02', '2022-11-29 23:43:02'),
(6, '127.0.0.1', '2022-11-29 23:43:04', '2022-11-29 23:43:04'),
(7, '127.0.0.1', '2022-11-29 23:43:10', '2022-11-29 23:43:10'),
(8, '127.0.0.1', '2022-11-29 23:43:13', '2022-11-29 23:43:13'),
(9, '127.0.0.1', '2022-11-29 23:45:55', '2022-11-29 23:45:55'),
(10, '127.0.0.1', '2022-11-29 23:45:56', '2022-11-29 23:45:56'),
(11, '127.0.0.1', '2022-11-29 23:45:58', '2022-11-29 23:45:58'),
(12, '127.0.0.1', '2022-11-29 23:46:10', '2022-11-29 23:46:10'),
(13, '127.0.0.1', '2022-11-29 23:46:11', '2022-11-29 23:46:11'),
(14, '127.0.0.1', '2022-11-29 23:46:12', '2022-11-29 23:46:12'),
(15, '127.0.0.1', '2022-11-29 23:46:25', '2022-11-29 23:46:25'),
(16, '127.0.0.1', '2022-11-29 23:46:26', '2022-11-29 23:46:26'),
(17, '127.0.0.1', '2022-11-29 23:46:28', '2022-11-29 23:46:28'),
(18, '127.0.0.1', '2022-11-30 00:00:30', '2022-11-30 00:00:30'),
(19, '127.0.0.1', '2022-11-30 00:00:32', '2022-11-30 00:00:32'),
(20, '127.0.0.1', '2022-11-30 00:00:33', '2022-11-30 00:00:33'),
(21, '127.0.0.1', '2022-11-30 00:00:36', '2022-11-30 00:00:36'),
(22, '127.0.0.1', '2022-11-30 00:00:37', '2022-11-30 00:00:37'),
(23, '127.0.0.1', '2022-11-30 00:02:14', '2022-11-30 00:02:14'),
(24, '127.0.0.1', '2022-11-30 00:02:15', '2022-11-30 00:02:15'),
(25, '127.0.0.1', '2022-11-30 00:02:26', '2022-11-30 00:02:26'),
(26, '127.0.0.1', '2022-11-30 00:02:27', '2022-11-30 00:02:27'),
(27, '127.0.0.1', '2022-11-30 00:02:33', '2022-11-30 00:02:33'),
(28, '127.0.0.1', '2022-11-30 00:02:34', '2022-11-30 00:02:34'),
(29, '127.0.0.1', '2022-11-30 00:48:21', '2022-11-30 00:48:21'),
(30, '127.0.0.1', '2022-11-30 00:48:23', '2022-11-30 00:48:23'),
(31, '127.0.0.1', '2022-11-30 00:48:26', '2022-11-30 00:48:26'),
(32, '127.0.0.1', '2022-11-30 00:50:43', '2022-11-30 00:50:43'),
(33, '127.0.0.1', '2022-11-30 00:50:45', '2022-11-30 00:50:45'),
(34, '127.0.0.1', '2022-11-30 00:50:46', '2022-11-30 00:50:46'),
(35, '127.0.0.1', '2022-11-30 00:54:39', '2022-11-30 00:54:39'),
(36, '127.0.0.1', '2022-11-30 00:54:40', '2022-11-30 00:54:40'),
(37, '127.0.0.1', '2022-11-30 00:54:42', '2022-11-30 00:54:42'),
(38, '127.0.0.1', '2022-11-30 00:55:14', '2022-11-30 00:55:14'),
(39, '127.0.0.1', '2022-11-30 00:55:15', '2022-11-30 00:55:15'),
(40, '127.0.0.1', '2022-11-30 00:55:17', '2022-11-30 00:55:17'),
(41, '127.0.0.1', '2022-11-30 00:55:31', '2022-11-30 00:55:31'),
(42, '127.0.0.1', '2022-11-30 00:55:33', '2022-11-30 00:55:33'),
(43, '127.0.0.1', '2022-11-30 00:55:35', '2022-11-30 00:55:35'),
(44, '127.0.0.1', '2022-11-30 00:55:52', '2022-11-30 00:55:52'),
(45, '127.0.0.1', '2022-11-30 00:55:53', '2022-11-30 00:55:53'),
(46, '127.0.0.1', '2022-11-30 00:55:55', '2022-11-30 00:55:55'),
(47, '127.0.0.1', '2022-11-30 00:56:08', '2022-11-30 00:56:08'),
(48, '127.0.0.1', '2022-11-30 00:56:09', '2022-11-30 00:56:09'),
(49, '127.0.0.1', '2022-11-30 00:56:11', '2022-11-30 00:56:11'),
(50, '127.0.0.1', '2022-11-30 01:03:42', '2022-11-30 01:03:42'),
(51, '127.0.0.1', '2022-11-30 01:03:59', '2022-11-30 01:03:59'),
(52, '127.0.0.1', '2022-11-30 01:04:01', '2022-11-30 01:04:01'),
(53, '127.0.0.1', '2022-11-30 01:05:12', '2022-11-30 01:05:12'),
(54, '127.0.0.1', '2022-11-30 01:05:14', '2022-11-30 01:05:14'),
(55, '127.0.0.1', '2022-11-30 01:05:19', '2022-11-30 01:05:19'),
(56, '127.0.0.1', '2022-11-30 01:11:54', '2022-11-30 01:11:54'),
(57, '127.0.0.1', '2022-11-30 01:11:56', '2022-11-30 01:11:56'),
(58, '127.0.0.1', '2022-11-30 01:13:02', '2022-11-30 01:13:02'),
(59, '127.0.0.1', '2022-11-30 01:13:04', '2022-11-30 01:13:04'),
(60, '127.0.0.1', '2022-11-30 01:13:07', '2022-11-30 01:13:07'),
(61, '127.0.0.1', '2022-11-30 01:13:09', '2022-11-30 01:13:09'),
(62, '127.0.0.1', '2022-11-30 01:13:13', '2022-11-30 01:13:13'),
(63, '127.0.0.1', '2022-11-30 01:13:14', '2022-11-30 01:13:14'),
(64, '127.0.0.1', '2022-11-30 01:19:16', '2022-11-30 01:19:16'),
(65, '127.0.0.1', '2022-11-30 01:19:18', '2022-11-30 01:19:18'),
(66, '127.0.0.1', '2022-11-30 01:19:21', '2022-11-30 01:19:21'),
(67, '127.0.0.1', '2022-11-30 01:19:23', '2022-11-30 01:19:23'),
(68, '127.0.0.1', '2022-11-30 01:43:13', '2022-11-30 01:43:13'),
(69, '127.0.0.1', '2022-11-30 01:43:16', '2022-11-30 01:43:16'),
(70, '127.0.0.1', '2022-11-30 01:43:17', '2022-11-30 01:43:17'),
(71, '127.0.0.1', '2022-11-30 01:43:20', '2022-11-30 01:43:20'),
(72, '127.0.0.1', '2022-11-30 23:33:50', '2022-11-30 23:33:50'),
(73, '127.0.0.1', '2022-11-30 23:34:00', '2022-11-30 23:34:00'),
(74, '127.0.0.1', '2022-11-30 23:34:03', '2022-11-30 23:34:03'),
(75, '127.0.0.1', '2022-11-30 23:34:37', '2022-11-30 23:34:37'),
(76, '127.0.0.1', '2022-11-30 23:34:40', '2022-11-30 23:34:40'),
(77, '127.0.0.1', '2022-11-30 23:35:12', '2022-11-30 23:35:12'),
(78, '127.0.0.1', '2022-11-30 23:35:13', '2022-11-30 23:35:13'),
(79, '127.0.0.1', '2022-11-30 23:36:35', '2022-11-30 23:36:35'),
(80, '127.0.0.1', '2022-11-30 23:36:41', '2022-11-30 23:36:41'),
(81, '127.0.0.1', '2022-11-30 23:38:03', '2022-11-30 23:38:03'),
(82, '127.0.0.1', '2022-11-30 23:38:05', '2022-11-30 23:38:05'),
(83, '127.0.0.1', '2022-11-30 23:39:12', '2022-11-30 23:39:12'),
(84, '127.0.0.1', '2022-11-30 23:39:13', '2022-11-30 23:39:13'),
(85, '127.0.0.1', '2022-11-30 23:40:40', '2022-11-30 23:40:40'),
(86, '127.0.0.1', '2022-11-30 23:40:41', '2022-11-30 23:40:41'),
(87, '127.0.0.1', '2022-11-30 23:41:15', '2022-11-30 23:41:15'),
(88, '127.0.0.1', '2022-11-30 23:41:17', '2022-11-30 23:41:17'),
(89, '127.0.0.1', '2022-11-30 23:41:53', '2022-11-30 23:41:53'),
(90, '127.0.0.1', '2022-11-30 23:41:55', '2022-11-30 23:41:55'),
(91, '127.0.0.1', '2022-11-30 23:42:05', '2022-11-30 23:42:05'),
(92, '127.0.0.1', '2022-11-30 23:46:00', '2022-11-30 23:46:00'),
(93, '127.0.0.1', '2022-11-30 23:46:01', '2022-11-30 23:46:01'),
(94, '127.0.0.1', '2022-11-30 23:46:59', '2022-11-30 23:46:59'),
(95, '127.0.0.1', '2022-11-30 23:47:01', '2022-11-30 23:47:01'),
(96, '127.0.0.1', '2022-11-30 23:47:03', '2022-11-30 23:47:03'),
(97, '127.0.0.1', '2022-11-30 23:47:22', '2022-11-30 23:47:22'),
(98, '127.0.0.1', '2022-11-30 23:47:23', '2022-11-30 23:47:23'),
(99, '127.0.0.1', '2022-11-30 23:48:13', '2022-11-30 23:48:13'),
(100, '127.0.0.1', '2022-11-30 23:48:15', '2022-11-30 23:48:15'),
(101, '127.0.0.1', '2022-11-30 23:53:44', '2022-11-30 23:53:44'),
(102, '127.0.0.1', '2022-11-30 23:53:49', '2022-11-30 23:53:49'),
(103, '127.0.0.1', '2022-11-30 23:56:06', '2022-11-30 23:56:06'),
(104, '127.0.0.1', '2022-11-30 23:56:07', '2022-11-30 23:56:07'),
(105, '127.0.0.1', '2022-11-30 23:56:43', '2022-11-30 23:56:43'),
(106, '127.0.0.1', '2022-11-30 23:56:48', '2022-11-30 23:56:48'),
(107, '127.0.0.1', '2022-11-30 23:57:18', '2022-11-30 23:57:18'),
(108, '127.0.0.1', '2022-11-30 23:57:26', '2022-11-30 23:57:26'),
(109, '127.0.0.1', '2022-11-30 23:57:27', '2022-11-30 23:57:27'),
(110, '127.0.0.1', '2022-12-01 00:03:19', '2022-12-01 00:03:19'),
(111, '127.0.0.1', '2022-12-01 00:03:27', '2022-12-01 00:03:27'),
(112, '127.0.0.1', '2022-12-01 00:06:25', '2022-12-01 00:06:25'),
(113, '127.0.0.1', '2022-12-01 00:06:27', '2022-12-01 00:06:27'),
(114, '127.0.0.1', '2022-12-01 00:06:31', '2022-12-01 00:06:31'),
(115, '127.0.0.1', '2022-12-01 00:06:34', '2022-12-01 00:06:34'),
(116, '127.0.0.1', '2022-12-01 00:06:51', '2022-12-01 00:06:51'),
(117, '127.0.0.1', '2022-12-01 00:06:54', '2022-12-01 00:06:54'),
(118, '127.0.0.1', '2022-12-01 00:06:56', '2022-12-01 00:06:56'),
(119, '127.0.0.1', '2022-12-01 00:06:57', '2022-12-01 00:06:57'),
(120, '127.0.0.1', '2022-12-01 00:08:12', '2022-12-01 00:08:12'),
(121, '127.0.0.1', '2022-12-01 00:08:14', '2022-12-01 00:08:14'),
(122, '127.0.0.1', '2022-12-01 00:08:16', '2022-12-01 00:08:16'),
(123, '127.0.0.1', '2022-12-01 00:08:18', '2022-12-01 00:08:18'),
(124, '127.0.0.1', '2022-12-01 00:55:54', '2022-12-01 00:55:54'),
(125, '127.0.0.1', '2022-12-01 00:55:57', '2022-12-01 00:55:57'),
(126, '127.0.0.1', '2022-12-01 01:04:08', '2022-12-01 01:04:08'),
(127, '127.0.0.1', '2022-12-01 01:04:10', '2022-12-01 01:04:10'),
(128, '127.0.0.1', '2022-12-01 01:11:29', '2022-12-01 01:11:29'),
(129, '127.0.0.1', '2022-12-01 01:11:31', '2022-12-01 01:11:31'),
(130, '127.0.0.1', '2022-12-01 01:12:26', '2022-12-01 01:12:26'),
(131, '127.0.0.1', '2022-12-01 01:12:28', '2022-12-01 01:12:28'),
(132, '127.0.0.1', '2022-12-01 02:55:37', '2022-12-01 02:55:37'),
(133, '127.0.0.1', '2022-12-01 02:55:40', '2022-12-01 02:55:40'),
(134, '127.0.0.1', '2022-12-01 02:55:42', '2022-12-01 02:55:42'),
(135, '127.0.0.1', '2022-12-01 02:57:05', '2022-12-01 02:57:05'),
(136, '127.0.0.1', '2022-12-01 02:57:07', '2022-12-01 02:57:07'),
(137, '127.0.0.1', '2022-12-01 03:04:30', '2022-12-01 03:04:30'),
(138, '127.0.0.1', '2022-12-01 03:04:31', '2022-12-01 03:04:31'),
(139, '127.0.0.1', '2022-12-01 03:04:33', '2022-12-01 03:04:33'),
(140, '127.0.0.1', '2022-12-01 03:04:43', '2022-12-01 03:04:43'),
(141, '127.0.0.1', '2022-12-01 03:04:44', '2022-12-01 03:04:44'),
(142, '127.0.0.1', '2022-12-01 03:04:48', '2022-12-01 03:04:48'),
(143, '127.0.0.1', '2022-12-01 03:04:50', '2022-12-01 03:04:50'),
(144, '127.0.0.1', '2022-12-01 03:04:53', '2022-12-01 03:04:53'),
(145, '127.0.0.1', '2022-12-01 03:04:57', '2022-12-01 03:04:57'),
(146, '127.0.0.1', '2022-12-01 03:04:59', '2022-12-01 03:04:59'),
(147, '127.0.0.1', '2022-12-01 03:05:01', '2022-12-01 03:05:01'),
(148, '127.0.0.1', '2022-12-01 03:05:02', '2022-12-01 03:05:02'),
(149, '127.0.0.1', '2022-12-01 03:05:04', '2022-12-01 03:05:04'),
(150, '127.0.0.1', '2022-12-01 03:05:06', '2022-12-01 03:05:06'),
(151, '127.0.0.1', '2022-12-02 08:40:24', '2022-12-02 08:40:24'),
(152, '127.0.0.1', '2022-12-02 08:40:36', '2022-12-02 08:40:36'),
(153, '127.0.0.1', '2022-12-02 08:40:38', '2022-12-02 08:40:38'),
(154, '127.0.0.1', '2022-12-02 08:43:05', '2022-12-02 08:43:05'),
(155, '127.0.0.1', '2022-12-02 08:53:56', '2022-12-02 08:53:56'),
(156, '127.0.0.1', '2022-12-02 08:53:58', '2022-12-02 08:53:58'),
(157, '127.0.0.1', '2022-12-02 08:55:04', '2022-12-02 08:55:04'),
(158, '127.0.0.1', '2022-12-02 08:55:06', '2022-12-02 08:55:06'),
(159, '127.0.0.1', '2022-12-02 09:01:13', '2022-12-02 09:01:13'),
(160, '127.0.0.1', '2022-12-02 09:01:16', '2022-12-02 09:01:16'),
(161, '127.0.0.1', '2022-12-02 09:01:59', '2022-12-02 09:01:59'),
(162, '127.0.0.1', '2022-12-02 09:02:00', '2022-12-02 09:02:00'),
(163, '127.0.0.1', '2022-12-02 09:02:09', '2022-12-02 09:02:09'),
(164, '127.0.0.1', '2022-12-02 09:02:11', '2022-12-02 09:02:11'),
(165, '127.0.0.1', '2022-12-02 09:02:15', '2022-12-02 09:02:15'),
(166, '127.0.0.1', '2022-12-02 09:02:16', '2022-12-02 09:02:16'),
(167, '127.0.0.1', '2022-12-02 09:02:38', '2022-12-02 09:02:38'),
(168, '127.0.0.1', '2022-12-02 09:02:40', '2022-12-02 09:02:40'),
(169, '127.0.0.1', '2022-12-02 09:03:11', '2022-12-02 09:03:11'),
(170, '127.0.0.1', '2022-12-02 09:03:15', '2022-12-02 09:03:15'),
(171, '127.0.0.1', '2022-12-02 09:03:25', '2022-12-02 09:03:25'),
(172, '127.0.0.1', '2022-12-02 09:03:28', '2022-12-02 09:03:28'),
(173, '127.0.0.1', '2022-12-02 09:03:35', '2022-12-02 09:03:35'),
(174, '127.0.0.1', '2022-12-02 09:03:37', '2022-12-02 09:03:37'),
(175, '127.0.0.1', '2022-12-02 09:04:03', '2022-12-02 09:04:03'),
(176, '127.0.0.1', '2022-12-02 09:04:04', '2022-12-02 09:04:04'),
(177, '127.0.0.1', '2022-12-02 09:10:10', '2022-12-02 09:10:10'),
(178, '127.0.0.1', '2022-12-02 09:10:28', '2022-12-02 09:10:28'),
(179, '127.0.0.1', '2022-12-02 09:10:30', '2022-12-02 09:10:30'),
(180, '127.0.0.1', '2022-12-02 09:17:40', '2022-12-02 09:17:40'),
(181, '127.0.0.1', '2022-12-02 09:17:54', '2022-12-02 09:17:54'),
(182, '127.0.0.1', '2022-12-02 09:17:55', '2022-12-02 09:17:55'),
(183, '127.0.0.1', '2022-12-02 09:18:04', '2022-12-02 09:18:04'),
(184, '127.0.0.1', '2022-12-02 09:55:12', '2022-12-02 09:55:12'),
(185, '127.0.0.1', '2022-12-02 09:55:14', '2022-12-02 09:55:14'),
(186, '127.0.0.1', '2022-12-02 09:55:44', '2022-12-02 09:55:44'),
(187, '127.0.0.1', '2022-12-02 10:22:23', '2022-12-02 10:22:23'),
(188, '127.0.0.1', '2022-12-02 10:22:24', '2022-12-02 10:22:24'),
(189, '127.0.0.1', '2022-12-02 10:22:30', '2022-12-02 10:22:30'),
(190, '127.0.0.1', '2022-12-02 10:22:31', '2022-12-02 10:22:31'),
(191, '127.0.0.1', '2022-12-02 10:22:36', '2022-12-02 10:22:36'),
(192, '127.0.0.1', '2022-12-02 10:22:37', '2022-12-02 10:22:37'),
(193, '127.0.0.1', '2022-12-02 10:23:48', '2022-12-02 10:23:48'),
(194, '127.0.0.1', '2022-12-02 10:23:50', '2022-12-02 10:23:50'),
(195, '127.0.0.1', '2022-12-02 10:26:03', '2022-12-02 10:26:03'),
(196, '127.0.0.1', '2022-12-02 10:26:05', '2022-12-02 10:26:05'),
(197, '127.0.0.1', '2022-12-02 10:28:58', '2022-12-02 10:28:58'),
(198, '127.0.0.1', '2022-12-02 10:30:02', '2022-12-02 10:30:02'),
(199, '127.0.0.1', '2022-12-02 10:31:07', '2022-12-02 10:31:07'),
(200, '127.0.0.1', '2022-12-02 10:31:09', '2022-12-02 10:31:09'),
(201, '127.0.0.1', '2022-12-02 10:33:47', '2022-12-02 10:33:47'),
(202, '127.0.0.1', '2022-12-02 10:33:49', '2022-12-02 10:33:49'),
(203, '127.0.0.1', '2022-12-02 10:42:37', '2022-12-02 10:42:37'),
(204, '127.0.0.1', '2022-12-02 10:42:39', '2022-12-02 10:42:39'),
(205, '127.0.0.1', '2022-12-02 10:44:14', '2022-12-02 10:44:14'),
(206, '127.0.0.1', '2022-12-02 10:44:15', '2022-12-02 10:44:15'),
(207, '127.0.0.1', '2022-12-02 10:48:19', '2022-12-02 10:48:19'),
(208, '127.0.0.1', '2022-12-02 10:48:21', '2022-12-02 10:48:21'),
(209, '127.0.0.1', '2022-12-02 10:48:41', '2022-12-02 10:48:41'),
(210, '127.0.0.1', '2022-12-02 10:48:43', '2022-12-02 10:48:43'),
(211, '127.0.0.1', '2022-12-02 10:49:27', '2022-12-02 10:49:27'),
(212, '127.0.0.1', '2022-12-02 10:49:30', '2022-12-02 10:49:30'),
(213, '127.0.0.1', '2022-12-02 10:49:31', '2022-12-02 10:49:31'),
(214, '127.0.0.1', '2022-12-02 10:50:01', '2022-12-02 10:50:01'),
(215, '127.0.0.1', '2022-12-02 10:50:54', '2022-12-02 10:50:54'),
(216, '127.0.0.1', '2022-12-02 10:50:56', '2022-12-02 10:50:56'),
(217, '127.0.0.1', '2022-12-02 10:50:57', '2022-12-02 10:50:57'),
(218, '127.0.0.1', '2022-12-02 10:57:19', '2022-12-02 10:57:19'),
(219, '127.0.0.1', '2022-12-02 10:57:20', '2022-12-02 10:57:20'),
(220, '127.0.0.1', '2022-12-02 10:57:29', '2022-12-02 10:57:29'),
(221, '127.0.0.1', '2022-12-02 10:57:30', '2022-12-02 10:57:30'),
(222, '127.0.0.1', '2022-12-03 07:00:19', '2022-12-03 07:00:19'),
(223, '127.0.0.1', '2022-12-03 07:00:27', '2022-12-03 07:00:27'),
(224, '127.0.0.1', '2022-12-03 07:00:28', '2022-12-03 07:00:28'),
(225, '127.0.0.1', '2022-12-03 07:00:57', '2022-12-03 07:00:57'),
(226, '127.0.0.1', '2022-12-03 07:01:04', '2022-12-03 07:01:04'),
(227, '127.0.0.1', '2022-12-03 07:01:06', '2022-12-03 07:01:06'),
(228, '127.0.0.1', '2022-12-03 07:01:09', '2022-12-03 07:01:09'),
(229, '127.0.0.1', '2022-12-03 07:01:10', '2022-12-03 07:01:10'),
(230, '127.0.0.1', '2022-12-03 07:01:12', '2022-12-03 07:01:12'),
(231, '127.0.0.1', '2022-12-03 07:01:14', '2022-12-03 07:01:14'),
(232, '127.0.0.1', '2022-12-03 07:04:31', '2022-12-03 07:04:31'),
(233, '127.0.0.1', '2022-12-03 07:04:32', '2022-12-03 07:04:32'),
(234, '127.0.0.1', '2022-12-03 07:05:24', '2022-12-03 07:05:24'),
(235, '127.0.0.1', '2022-12-03 07:05:25', '2022-12-03 07:05:25'),
(236, '127.0.0.1', '2022-12-03 07:05:42', '2022-12-03 07:05:42'),
(237, '127.0.0.1', '2022-12-03 07:05:43', '2022-12-03 07:05:43'),
(238, '127.0.0.1', '2022-12-03 07:06:20', '2022-12-03 07:06:20'),
(239, '127.0.0.1', '2022-12-03 07:06:21', '2022-12-03 07:06:21'),
(240, '127.0.0.1', '2022-12-03 07:06:55', '2022-12-03 07:06:55'),
(241, '127.0.0.1', '2022-12-03 07:06:56', '2022-12-03 07:06:56'),
(242, '127.0.0.1', '2022-12-03 07:08:03', '2022-12-03 07:08:03'),
(243, '127.0.0.1', '2022-12-03 07:08:05', '2022-12-03 07:08:05'),
(244, '127.0.0.1', '2022-12-03 07:10:02', '2022-12-03 07:10:02'),
(245, '127.0.0.1', '2022-12-03 07:10:03', '2022-12-03 07:10:03'),
(246, '127.0.0.1', '2022-12-03 07:11:03', '2022-12-03 07:11:03'),
(247, '127.0.0.1', '2022-12-03 07:11:05', '2022-12-03 07:11:05'),
(248, '127.0.0.1', '2022-12-03 07:12:44', '2022-12-03 07:12:44'),
(249, '127.0.0.1', '2022-12-03 07:12:46', '2022-12-03 07:12:46'),
(250, '127.0.0.1', '2022-12-03 07:13:02', '2022-12-03 07:13:02'),
(251, '127.0.0.1', '2022-12-03 07:13:04', '2022-12-03 07:13:04'),
(252, '127.0.0.1', '2022-12-03 07:13:59', '2022-12-03 07:13:59'),
(253, '127.0.0.1', '2022-12-03 07:14:01', '2022-12-03 07:14:01'),
(254, '127.0.0.1', '2022-12-03 07:14:15', '2022-12-03 07:14:15'),
(255, '127.0.0.1', '2022-12-03 07:14:17', '2022-12-03 07:14:17'),
(256, '127.0.0.1', '2022-12-03 07:15:51', '2022-12-03 07:15:51'),
(257, '127.0.0.1', '2022-12-03 07:15:53', '2022-12-03 07:15:53'),
(258, '127.0.0.1', '2022-12-03 07:16:08', '2022-12-03 07:16:08'),
(259, '127.0.0.1', '2022-12-03 07:16:10', '2022-12-03 07:16:10'),
(260, '127.0.0.1', '2022-12-03 07:16:12', '2022-12-03 07:16:12'),
(261, '127.0.0.1', '2022-12-03 07:16:13', '2022-12-03 07:16:13'),
(262, '127.0.0.1', '2022-12-03 07:16:19', '2022-12-03 07:16:19'),
(263, '127.0.0.1', '2022-12-03 07:16:21', '2022-12-03 07:16:21'),
(264, '127.0.0.1', '2022-12-03 07:16:27', '2022-12-03 07:16:27'),
(265, '127.0.0.1', '2022-12-03 07:16:30', '2022-12-03 07:16:30'),
(266, '127.0.0.1', '2022-12-03 07:16:31', '2022-12-03 07:16:31'),
(267, '127.0.0.1', '2022-12-03 07:17:30', '2022-12-03 07:17:30'),
(268, '127.0.0.1', '2022-12-03 07:17:31', '2022-12-03 07:17:31'),
(269, '127.0.0.1', '2022-12-03 07:19:35', '2022-12-03 07:19:35'),
(270, '127.0.0.1', '2022-12-03 07:19:38', '2022-12-03 07:19:38'),
(271, '127.0.0.1', '2022-12-03 07:21:42', '2022-12-03 07:21:42'),
(272, '127.0.0.1', '2022-12-03 07:21:46', '2022-12-03 07:21:46'),
(273, '127.0.0.1', '2022-12-03 07:22:18', '2022-12-03 07:22:18'),
(274, '127.0.0.1', '2022-12-03 07:22:21', '2022-12-03 07:22:21'),
(275, '127.0.0.1', '2022-12-03 07:22:25', '2022-12-03 07:22:25'),
(276, '127.0.0.1', '2022-12-03 07:22:28', '2022-12-03 07:22:28'),
(277, '127.0.0.1', '2022-12-03 07:22:58', '2022-12-03 07:22:58'),
(278, '127.0.0.1', '2022-12-03 07:23:00', '2022-12-03 07:23:00'),
(279, '127.0.0.1', '2022-12-03 07:23:15', '2022-12-03 07:23:15'),
(280, '127.0.0.1', '2022-12-03 07:23:18', '2022-12-03 07:23:18'),
(281, '127.0.0.1', '2022-12-03 07:23:40', '2022-12-03 07:23:40'),
(282, '127.0.0.1', '2022-12-03 07:23:42', '2022-12-03 07:23:42'),
(283, '127.0.0.1', '2022-12-03 07:24:02', '2022-12-03 07:24:02'),
(284, '127.0.0.1', '2022-12-03 07:24:04', '2022-12-03 07:24:04'),
(285, '127.0.0.1', '2022-12-03 07:24:32', '2022-12-03 07:24:32'),
(286, '127.0.0.1', '2022-12-03 07:24:33', '2022-12-03 07:24:33'),
(287, '127.0.0.1', '2022-12-03 07:24:38', '2022-12-03 07:24:38'),
(288, '127.0.0.1', '2022-12-03 07:24:39', '2022-12-03 07:24:39'),
(289, '127.0.0.1', '2022-12-03 07:24:55', '2022-12-03 07:24:55'),
(290, '127.0.0.1', '2022-12-03 07:24:55', '2022-12-03 07:24:55'),
(291, '127.0.0.1', '2022-12-03 07:25:11', '2022-12-03 07:25:11'),
(292, '127.0.0.1', '2022-12-03 07:25:12', '2022-12-03 07:25:12'),
(293, '127.0.0.1', '2022-12-03 07:27:32', '2022-12-03 07:27:32'),
(294, '127.0.0.1', '2022-12-03 07:27:35', '2022-12-03 07:27:35'),
(295, '127.0.0.1', '2022-12-03 07:27:45', '2022-12-03 07:27:45'),
(296, '127.0.0.1', '2022-12-03 07:27:48', '2022-12-03 07:27:48'),
(297, '127.0.0.1', '2022-12-03 07:28:08', '2022-12-03 07:28:08'),
(298, '127.0.0.1', '2022-12-03 07:28:11', '2022-12-03 07:28:11'),
(299, '127.0.0.1', '2022-12-03 07:28:16', '2022-12-03 07:28:16'),
(300, '127.0.0.1', '2022-12-03 07:28:17', '2022-12-03 07:28:17'),
(301, '127.0.0.1', '2022-12-03 07:28:22', '2022-12-03 07:28:22'),
(302, '127.0.0.1', '2022-12-04 23:32:03', '2022-12-04 23:32:03'),
(303, '127.0.0.1', '2022-12-04 23:32:05', '2022-12-04 23:32:05'),
(304, '127.0.0.1', '2022-12-04 23:32:06', '2022-12-04 23:32:06'),
(305, '127.0.0.1', '2022-12-04 23:32:07', '2022-12-04 23:32:07'),
(306, '127.0.0.1', '2022-12-04 23:36:14', '2022-12-04 23:36:14'),
(307, '127.0.0.1', '2022-12-04 23:38:40', '2022-12-04 23:38:40'),
(308, '127.0.0.1', '2022-12-04 23:38:41', '2022-12-04 23:38:41'),
(309, '127.0.0.1', '2022-12-04 23:38:42', '2022-12-04 23:38:42'),
(310, '127.0.0.1', '2022-12-04 23:38:42', '2022-12-04 23:38:42'),
(311, '127.0.0.1', '2022-12-04 23:39:03', '2022-12-04 23:39:03'),
(312, '127.0.0.1', '2022-12-04 23:39:04', '2022-12-04 23:39:04'),
(313, '127.0.0.1', '2022-12-04 23:39:05', '2022-12-04 23:39:05'),
(314, '127.0.0.1', '2022-12-04 23:39:21', '2022-12-04 23:39:21'),
(315, '127.0.0.1', '2022-12-04 23:39:23', '2022-12-04 23:39:23'),
(316, '127.0.0.1', '2022-12-04 23:39:24', '2022-12-04 23:39:24'),
(317, '127.0.0.1', '2022-12-04 23:40:14', '2022-12-04 23:40:14'),
(318, '127.0.0.1', '2022-12-04 23:40:14', '2022-12-04 23:40:14'),
(319, '127.0.0.1', '2022-12-04 23:40:23', '2022-12-04 23:40:23'),
(320, '127.0.0.1', '2022-12-04 23:40:24', '2022-12-04 23:40:24'),
(321, '127.0.0.1', '2022-12-04 23:40:24', '2022-12-04 23:40:24'),
(322, '127.0.0.1', '2022-12-04 23:40:45', '2022-12-04 23:40:45'),
(323, '127.0.0.1', '2022-12-04 23:40:46', '2022-12-04 23:40:46'),
(324, '127.0.0.1', '2022-12-04 23:40:46', '2022-12-04 23:40:46'),
(325, '127.0.0.1', '2022-12-04 23:40:52', '2022-12-04 23:40:52'),
(326, '127.0.0.1', '2022-12-04 23:40:52', '2022-12-04 23:40:52'),
(327, '127.0.0.1', '2022-12-04 23:40:53', '2022-12-04 23:40:53'),
(328, '127.0.0.1', '2022-12-04 23:40:56', '2022-12-04 23:40:56'),
(329, '127.0.0.1', '2022-12-04 23:40:57', '2022-12-04 23:40:57'),
(330, '127.0.0.1', '2022-12-04 23:40:57', '2022-12-04 23:40:57'),
(331, '127.0.0.1', '2022-12-04 23:41:12', '2022-12-04 23:41:12'),
(332, '127.0.0.1', '2022-12-04 23:41:13', '2022-12-04 23:41:13'),
(333, '127.0.0.1', '2022-12-04 23:41:13', '2022-12-04 23:41:13'),
(334, '127.0.0.1', '2022-12-04 23:41:21', '2022-12-04 23:41:21'),
(335, '127.0.0.1', '2022-12-04 23:41:22', '2022-12-04 23:41:22'),
(336, '127.0.0.1', '2022-12-04 23:41:22', '2022-12-04 23:41:22'),
(337, '127.0.0.1', '2022-12-04 23:41:23', '2022-12-04 23:41:23'),
(338, '127.0.0.1', '2022-12-04 23:41:30', '2022-12-04 23:41:30'),
(339, '127.0.0.1', '2022-12-04 23:41:31', '2022-12-04 23:41:31'),
(340, '127.0.0.1', '2022-12-04 23:41:31', '2022-12-04 23:41:31'),
(341, '127.0.0.1', '2022-12-04 23:41:38', '2022-12-04 23:41:38'),
(342, '127.0.0.1', '2022-12-04 23:41:39', '2022-12-04 23:41:39'),
(343, '127.0.0.1', '2022-12-04 23:41:39', '2022-12-04 23:41:39'),
(344, '127.0.0.1', '2022-12-04 23:41:41', '2022-12-04 23:41:41'),
(345, '127.0.0.1', '2022-12-04 23:41:41', '2022-12-04 23:41:41'),
(346, '127.0.0.1', '2022-12-04 23:41:42', '2022-12-04 23:41:42'),
(347, '127.0.0.1', '2022-12-04 23:41:49', '2022-12-04 23:41:49'),
(348, '127.0.0.1', '2022-12-04 23:41:49', '2022-12-04 23:41:49'),
(349, '127.0.0.1', '2022-12-04 23:41:50', '2022-12-04 23:41:50'),
(350, '127.0.0.1', '2022-12-04 23:41:51', '2022-12-04 23:41:51'),
(351, '127.0.0.1', '2022-12-04 23:41:52', '2022-12-04 23:41:52'),
(352, '127.0.0.1', '2022-12-04 23:41:52', '2022-12-04 23:41:52'),
(353, '127.0.0.1', '2022-12-04 23:41:57', '2022-12-04 23:41:57'),
(354, '127.0.0.1', '2022-12-04 23:41:58', '2022-12-04 23:41:58'),
(355, '127.0.0.1', '2022-12-04 23:41:58', '2022-12-04 23:41:58'),
(356, '127.0.0.1', '2022-12-04 23:42:09', '2022-12-04 23:42:09'),
(357, '127.0.0.1', '2022-12-04 23:42:10', '2022-12-04 23:42:10'),
(358, '127.0.0.1', '2022-12-04 23:42:10', '2022-12-04 23:42:10'),
(359, '127.0.0.1', '2022-12-04 23:42:11', '2022-12-04 23:42:11'),
(360, '127.0.0.1', '2022-12-04 23:42:12', '2022-12-04 23:42:12'),
(361, '127.0.0.1', '2022-12-04 23:42:13', '2022-12-04 23:42:13'),
(362, '127.0.0.1', '2022-12-04 23:42:23', '2022-12-04 23:42:23'),
(363, '127.0.0.1', '2022-12-04 23:42:23', '2022-12-04 23:42:23'),
(364, '127.0.0.1', '2022-12-04 23:42:24', '2022-12-04 23:42:24'),
(365, '127.0.0.1', '2022-12-04 23:42:27', '2022-12-04 23:42:27'),
(366, '127.0.0.1', '2022-12-04 23:42:28', '2022-12-04 23:42:28'),
(367, '127.0.0.1', '2022-12-04 23:42:28', '2022-12-04 23:42:28'),
(368, '127.0.0.1', '2022-12-04 23:42:55', '2022-12-04 23:42:55'),
(369, '127.0.0.1', '2022-12-04 23:43:06', '2022-12-04 23:43:06'),
(370, '127.0.0.1', '2022-12-04 23:43:07', '2022-12-04 23:43:07'),
(371, '127.0.0.1', '2022-12-04 23:43:07', '2022-12-04 23:43:07'),
(372, '127.0.0.1', '2022-12-04 23:43:09', '2022-12-04 23:43:09'),
(373, '127.0.0.1', '2022-12-04 23:43:10', '2022-12-04 23:43:10'),
(374, '127.0.0.1', '2022-12-04 23:43:11', '2022-12-04 23:43:11'),
(375, '127.0.0.1', '2022-12-04 23:43:11', '2022-12-04 23:43:11'),
(376, '127.0.0.1', '2022-12-04 23:43:31', '2022-12-04 23:43:31'),
(377, '127.0.0.1', '2022-12-04 23:43:32', '2022-12-04 23:43:32'),
(378, '127.0.0.1', '2022-12-04 23:43:32', '2022-12-04 23:43:32'),
(379, '127.0.0.1', '2022-12-05 00:06:49', '2022-12-05 00:06:49'),
(380, '127.0.0.1', '2022-12-05 00:06:51', '2022-12-05 00:06:51'),
(381, '127.0.0.1', '2022-12-05 00:06:51', '2022-12-05 00:06:51'),
(382, '127.0.0.1', '2022-12-05 00:08:27', '2022-12-05 00:08:27'),
(383, '127.0.0.1', '2022-12-05 00:08:28', '2022-12-05 00:08:28'),
(384, '127.0.0.1', '2022-12-05 00:08:28', '2022-12-05 00:08:28'),
(385, '127.0.0.1', '2022-12-05 00:08:30', '2022-12-05 00:08:30'),
(386, '127.0.0.1', '2022-12-05 00:08:32', '2022-12-05 00:08:32'),
(387, '127.0.0.1', '2022-12-05 00:08:33', '2022-12-05 00:08:33'),
(388, '127.0.0.1', '2022-12-05 00:09:23', '2022-12-05 00:09:23'),
(389, '127.0.0.1', '2022-12-05 00:09:24', '2022-12-05 00:09:24'),
(390, '127.0.0.1', '2022-12-05 00:09:30', '2022-12-05 00:09:30'),
(391, '127.0.0.1', '2022-12-05 00:09:31', '2022-12-05 00:09:31'),
(392, '127.0.0.1', '2022-12-05 00:09:32', '2022-12-05 00:09:32'),
(393, '127.0.0.1', '2022-12-05 00:09:42', '2022-12-05 00:09:42'),
(394, '127.0.0.1', '2022-12-05 00:09:43', '2022-12-05 00:09:43'),
(395, '127.0.0.1', '2022-12-05 00:09:47', '2022-12-05 00:09:47'),
(396, '127.0.0.1', '2022-12-05 00:09:48', '2022-12-05 00:09:48'),
(397, '127.0.0.1', '2022-12-05 00:10:20', '2022-12-05 00:10:20'),
(398, '127.0.0.1', '2022-12-05 00:10:20', '2022-12-05 00:10:20'),
(399, '127.0.0.1', '2022-12-05 00:10:23', '2022-12-05 00:10:23'),
(400, '127.0.0.1', '2022-12-05 00:10:24', '2022-12-05 00:10:24'),
(401, '127.0.0.1', '2022-12-05 00:11:03', '2022-12-05 00:11:03'),
(402, '127.0.0.1', '2022-12-05 00:11:03', '2022-12-05 00:11:03'),
(403, '127.0.0.1', '2022-12-05 00:11:06', '2022-12-05 00:11:06'),
(404, '127.0.0.1', '2022-12-05 00:11:06', '2022-12-05 00:11:06'),
(405, '127.0.0.1', '2022-12-05 00:11:51', '2022-12-05 00:11:51'),
(406, '127.0.0.1', '2022-12-05 00:11:52', '2022-12-05 00:11:52'),
(407, '127.0.0.1', '2022-12-05 00:12:10', '2022-12-05 00:12:10'),
(408, '127.0.0.1', '2022-12-05 00:12:11', '2022-12-05 00:12:11'),
(409, '127.0.0.1', '2022-12-05 00:12:12', '2022-12-05 00:12:12'),
(410, '127.0.0.1', '2022-12-05 00:12:20', '2022-12-05 00:12:20'),
(411, '127.0.0.1', '2022-12-05 00:12:20', '2022-12-05 00:12:20'),
(412, '127.0.0.1', '2022-12-05 00:12:29', '2022-12-05 00:12:29'),
(413, '127.0.0.1', '2022-12-05 00:12:30', '2022-12-05 00:12:30'),
(414, '127.0.0.1', '2022-12-05 00:12:49', '2022-12-05 00:12:49'),
(415, '127.0.0.1', '2022-12-05 00:12:50', '2022-12-05 00:12:50'),
(416, '127.0.0.1', '2022-12-05 00:12:51', '2022-12-05 00:12:51'),
(417, '127.0.0.1', '2022-12-05 00:12:55', '2022-12-05 00:12:55'),
(418, '127.0.0.1', '2022-12-05 00:12:57', '2022-12-05 00:12:57'),
(419, '127.0.0.1', '2022-12-05 00:13:11', '2022-12-05 00:13:11'),
(420, '127.0.0.1', '2022-12-05 00:13:12', '2022-12-05 00:13:12'),
(421, '127.0.0.1', '2022-12-05 00:13:13', '2022-12-05 00:13:13'),
(422, '127.0.0.1', '2022-12-05 00:14:01', '2022-12-05 00:14:01'),
(423, '127.0.0.1', '2022-12-05 00:14:01', '2022-12-05 00:14:01'),
(424, '127.0.0.1', '2022-12-05 00:14:04', '2022-12-05 00:14:04'),
(425, '127.0.0.1', '2022-12-05 00:14:05', '2022-12-05 00:14:05'),
(426, '127.0.0.1', '2022-12-05 00:15:32', '2022-12-05 00:15:32'),
(427, '127.0.0.1', '2022-12-05 00:15:33', '2022-12-05 00:15:33'),
(428, '127.0.0.1', '2022-12-05 00:15:34', '2022-12-05 00:15:34'),
(429, '127.0.0.1', '2022-12-05 00:15:46', '2022-12-05 00:15:46'),
(430, '127.0.0.1', '2022-12-05 00:15:47', '2022-12-05 00:15:47'),
(431, '127.0.0.1', '2022-12-05 00:16:06', '2022-12-05 00:16:06'),
(432, '127.0.0.1', '2022-12-05 00:16:07', '2022-12-05 00:16:07'),
(433, '127.0.0.1', '2022-12-05 00:17:28', '2022-12-05 00:17:28'),
(434, '127.0.0.1', '2022-12-05 00:17:31', '2022-12-05 00:17:31'),
(435, '127.0.0.1', '2022-12-05 00:17:31', '2022-12-05 00:17:31'),
(436, '127.0.0.1', '2022-12-05 00:18:18', '2022-12-05 00:18:18'),
(437, '127.0.0.1', '2022-12-05 00:18:20', '2022-12-05 00:18:20'),
(438, '127.0.0.1', '2022-12-05 00:18:21', '2022-12-05 00:18:21'),
(439, '127.0.0.1', '2022-12-05 00:18:31', '2022-12-05 00:18:31'),
(440, '127.0.0.1', '2022-12-05 00:18:32', '2022-12-05 00:18:32'),
(441, '127.0.0.1', '2022-12-05 00:18:43', '2022-12-05 00:18:43'),
(442, '127.0.0.1', '2022-12-05 00:18:43', '2022-12-05 00:18:43'),
(443, '127.0.0.1', '2022-12-05 00:19:16', '2022-12-05 00:19:16'),
(444, '127.0.0.1', '2022-12-05 00:19:16', '2022-12-05 00:19:16'),
(445, '127.0.0.1', '2022-12-05 00:19:27', '2022-12-05 00:19:27'),
(446, '127.0.0.1', '2022-12-05 00:19:28', '2022-12-05 00:19:28'),
(447, '127.0.0.1', '2022-12-05 00:20:16', '2022-12-05 00:20:16'),
(448, '127.0.0.1', '2022-12-05 00:20:16', '2022-12-05 00:20:16'),
(449, '127.0.0.1', '2022-12-05 00:20:56', '2022-12-05 00:20:56'),
(450, '127.0.0.1', '2022-12-05 00:20:56', '2022-12-05 00:20:56'),
(451, '127.0.0.1', '2022-12-05 00:22:05', '2022-12-05 00:22:05'),
(452, '127.0.0.1', '2022-12-05 00:22:05', '2022-12-05 00:22:05'),
(453, '127.0.0.1', '2022-12-05 00:22:33', '2022-12-05 00:22:33'),
(454, '127.0.0.1', '2022-12-05 00:22:34', '2022-12-05 00:22:34'),
(455, '127.0.0.1', '2022-12-05 00:24:59', '2022-12-05 00:24:59'),
(456, '127.0.0.1', '2022-12-05 00:24:59', '2022-12-05 00:24:59'),
(457, '127.0.0.1', '2022-12-05 00:25:08', '2022-12-05 00:25:08'),
(458, '127.0.0.1', '2022-12-05 00:25:10', '2022-12-05 00:25:10'),
(459, '127.0.0.1', '2022-12-05 00:25:10', '2022-12-05 00:25:10'),
(460, '127.0.0.1', '2022-12-05 00:25:14', '2022-12-05 00:25:14'),
(461, '127.0.0.1', '2022-12-05 00:25:15', '2022-12-05 00:25:15'),
(462, '127.0.0.1', '2022-12-05 00:25:16', '2022-12-05 00:25:16'),
(463, '127.0.0.1', '2022-12-05 00:48:29', '2022-12-05 00:48:29'),
(464, '127.0.0.1', '2022-12-05 00:48:31', '2022-12-05 00:48:31'),
(465, '127.0.0.1', '2022-12-05 00:48:31', '2022-12-05 00:48:31'),
(466, '127.0.0.1', '2022-12-05 00:52:25', '2022-12-05 00:52:25'),
(467, '127.0.0.1', '2022-12-05 00:52:26', '2022-12-05 00:52:26'),
(468, '127.0.0.1', '2022-12-05 00:53:50', '2022-12-05 00:53:50'),
(469, '127.0.0.1', '2022-12-05 00:53:50', '2022-12-05 00:53:50'),
(470, '127.0.0.1', '2022-12-05 00:54:11', '2022-12-05 00:54:11'),
(471, '127.0.0.1', '2022-12-05 00:54:11', '2022-12-05 00:54:11'),
(472, '127.0.0.1', '2022-12-05 00:54:13', '2022-12-05 00:54:13'),
(473, '127.0.0.1', '2022-12-05 00:54:14', '2022-12-05 00:54:14'),
(474, '127.0.0.1', '2022-12-05 00:54:15', '2022-12-05 00:54:15'),
(475, '127.0.0.1', '2022-12-05 00:54:16', '2022-12-05 00:54:16'),
(476, '127.0.0.1', '2022-12-05 00:54:42', '2022-12-05 00:54:42'),
(477, '127.0.0.1', '2022-12-05 00:54:42', '2022-12-05 00:54:42'),
(478, '127.0.0.1', '2022-12-05 00:55:16', '2022-12-05 00:55:16'),
(479, '127.0.0.1', '2022-12-05 00:55:17', '2022-12-05 00:55:17'),
(480, '127.0.0.1', '2022-12-05 00:55:38', '2022-12-05 00:55:38'),
(481, '127.0.0.1', '2022-12-05 00:55:38', '2022-12-05 00:55:38'),
(482, '127.0.0.1', '2022-12-05 00:56:04', '2022-12-05 00:56:04'),
(483, '127.0.0.1', '2022-12-05 00:56:05', '2022-12-05 00:56:05'),
(484, '127.0.0.1', '2022-12-05 00:56:05', '2022-12-05 00:56:05'),
(485, '127.0.0.1', '2022-12-05 00:56:16', '2022-12-05 00:56:16'),
(486, '127.0.0.1', '2022-12-05 00:56:17', '2022-12-05 00:56:17'),
(487, '127.0.0.1', '2022-12-05 00:56:37', '2022-12-05 00:56:37'),
(488, '127.0.0.1', '2022-12-05 00:56:38', '2022-12-05 00:56:38'),
(489, '127.0.0.1', '2022-12-05 00:56:38', '2022-12-05 00:56:38'),
(490, '127.0.0.1', '2022-12-05 00:59:25', '2022-12-05 00:59:25'),
(491, '127.0.0.1', '2022-12-05 00:59:26', '2022-12-05 00:59:26'),
(492, '127.0.0.1', '2022-12-05 00:59:27', '2022-12-05 00:59:27'),
(493, '127.0.0.1', '2022-12-05 01:01:32', '2022-12-05 01:01:32'),
(494, '127.0.0.1', '2022-12-05 01:01:34', '2022-12-05 01:01:34'),
(495, '127.0.0.1', '2022-12-05 01:01:34', '2022-12-05 01:01:34'),
(496, '127.0.0.1', '2022-12-05 01:03:09', '2022-12-05 01:03:09'),
(497, '127.0.0.1', '2022-12-05 01:03:09', '2022-12-05 01:03:09'),
(498, '127.0.0.1', '2022-12-05 01:03:25', '2022-12-05 01:03:25'),
(499, '127.0.0.1', '2022-12-05 01:03:26', '2022-12-05 01:03:26'),
(500, '127.0.0.1', '2022-12-05 01:03:28', '2022-12-05 01:03:28'),
(501, '127.0.0.1', '2022-12-05 01:03:30', '2022-12-05 01:03:30'),
(502, '127.0.0.1', '2022-12-05 01:03:30', '2022-12-05 01:03:30'),
(503, '127.0.0.1', '2022-12-05 01:28:44', '2022-12-05 01:28:44'),
(504, '127.0.0.1', '2022-12-05 01:28:50', '2022-12-05 01:28:50'),
(505, '127.0.0.1', '2022-12-05 01:28:51', '2022-12-05 01:28:51'),
(506, '127.0.0.1', '2022-12-05 01:31:10', '2022-12-05 01:31:10'),
(507, '127.0.0.1', '2022-12-05 01:31:10', '2022-12-05 01:31:10'),
(508, '127.0.0.1', '2022-12-05 01:31:13', '2022-12-05 01:31:13'),
(509, '127.0.0.1', '2022-12-05 01:31:14', '2022-12-05 01:31:14'),
(510, '127.0.0.1', '2022-12-05 04:18:37', '2022-12-05 04:18:37'),
(511, '127.0.0.1', '2022-12-05 04:18:39', '2022-12-05 04:18:39'),
(512, '127.0.0.1', '2022-12-05 04:18:40', '2022-12-05 04:18:40'),
(513, '127.0.0.1', '2022-12-05 04:18:50', '2022-12-05 04:18:50'),
(514, '127.0.0.1', '2022-12-05 04:18:51', '2022-12-05 04:18:51'),
(515, '127.0.0.1', '2022-12-05 04:18:52', '2022-12-05 04:18:52'),
(516, '127.0.0.1', '2022-12-05 04:19:30', '2022-12-05 04:19:30'),
(517, '127.0.0.1', '2022-12-05 04:19:30', '2022-12-05 04:19:30'),
(518, '127.0.0.1', '2022-12-05 04:19:44', '2022-12-05 04:19:44'),
(519, '127.0.0.1', '2022-12-05 04:19:44', '2022-12-05 04:19:44'),
(520, '127.0.0.1', '2022-12-05 04:20:04', '2022-12-05 04:20:04'),
(521, '127.0.0.1', '2022-12-05 04:20:04', '2022-12-05 04:20:04'),
(522, '127.0.0.1', '2022-12-05 04:20:07', '2022-12-05 04:20:07'),
(523, '127.0.0.1', '2022-12-05 04:20:07', '2022-12-05 04:20:07'),
(524, '127.0.0.1', '2022-12-05 04:21:37', '2022-12-05 04:21:37'),
(525, '127.0.0.1', '2022-12-05 04:21:38', '2022-12-05 04:21:38'),
(526, '127.0.0.1', '2022-12-05 04:21:41', '2022-12-05 04:21:41'),
(527, '127.0.0.1', '2022-12-05 04:21:42', '2022-12-05 04:21:42'),
(528, '127.0.0.1', '2022-12-05 04:21:43', '2022-12-05 04:21:43'),
(529, '127.0.0.1', '2022-12-05 04:23:12', '2022-12-05 04:23:12'),
(530, '127.0.0.1', '2022-12-05 04:23:13', '2022-12-05 04:23:13'),
(531, '127.0.0.1', '2022-12-05 04:24:10', '2022-12-05 04:24:10'),
(532, '127.0.0.1', '2022-12-05 04:24:10', '2022-12-05 04:24:10'),
(533, '127.0.0.1', '2022-12-05 04:24:13', '2022-12-05 04:24:13'),
(534, '127.0.0.1', '2022-12-05 04:24:15', '2022-12-05 04:24:15'),
(535, '127.0.0.1', '2022-12-05 04:24:16', '2022-12-05 04:24:16'),
(536, '127.0.0.1', '2022-12-05 04:24:28', '2022-12-05 04:24:28'),
(537, '127.0.0.1', '2022-12-05 04:24:29', '2022-12-05 04:24:29'),
(538, '127.0.0.1', '2022-12-05 04:24:35', '2022-12-05 04:24:35'),
(539, '127.0.0.1', '2022-12-05 04:24:36', '2022-12-05 04:24:36'),
(540, '127.0.0.1', '2022-12-05 04:24:36', '2022-12-05 04:24:36'),
(541, '127.0.0.1', '2022-12-05 04:24:38', '2022-12-05 04:24:38'),
(542, '127.0.0.1', '2022-12-05 04:24:39', '2022-12-05 04:24:39'),
(543, '127.0.0.1', '2022-12-05 04:25:37', '2022-12-05 04:25:37'),
(544, '127.0.0.1', '2022-12-05 04:25:37', '2022-12-05 04:25:37'),
(545, '127.0.0.1', '2022-12-05 04:25:38', '2022-12-05 04:25:38'),
(546, '127.0.0.1', '2022-12-05 04:26:53', '2022-12-05 04:26:53'),
(547, '127.0.0.1', '2022-12-05 04:26:54', '2022-12-05 04:26:54'),
(548, '127.0.0.1', '2022-12-05 04:26:57', '2022-12-05 04:26:57'),
(549, '127.0.0.1', '2022-12-05 04:26:58', '2022-12-05 04:26:58'),
(550, '127.0.0.1', '2022-12-05 04:27:50', '2022-12-05 04:27:50'),
(551, '127.0.0.1', '2022-12-05 04:27:50', '2022-12-05 04:27:50'),
(552, '127.0.0.1', '2022-12-05 04:28:03', '2022-12-05 04:28:03'),
(553, '127.0.0.1', '2022-12-05 04:28:04', '2022-12-05 04:28:04'),
(554, '127.0.0.1', '2022-12-05 04:28:07', '2022-12-05 04:28:07'),
(555, '127.0.0.1', '2022-12-05 04:28:08', '2022-12-05 04:28:08'),
(556, '127.0.0.1', '2022-12-05 04:28:47', '2022-12-05 04:28:47'),
(557, '127.0.0.1', '2022-12-05 04:28:47', '2022-12-05 04:28:47'),
(558, '127.0.0.1', '2022-12-05 04:28:49', '2022-12-05 04:28:49'),
(559, '127.0.0.1', '2022-12-05 04:28:50', '2022-12-05 04:28:50'),
(560, '127.0.0.1', '2022-12-05 04:28:51', '2022-12-05 04:28:51'),
(561, '127.0.0.1', '2022-12-05 04:29:41', '2022-12-05 04:29:41'),
(562, '127.0.0.1', '2022-12-05 04:29:42', '2022-12-05 04:29:42'),
(563, '127.0.0.1', '2022-12-05 04:29:43', '2022-12-05 04:29:43'),
(564, '127.0.0.1', '2022-12-05 04:30:24', '2022-12-05 04:30:24'),
(565, '127.0.0.1', '2022-12-05 04:30:26', '2022-12-05 04:30:26'),
(566, '127.0.0.1', '2022-12-05 04:30:26', '2022-12-05 04:30:26'),
(567, '127.0.0.1', '2022-12-05 04:30:40', '2022-12-05 04:30:40'),
(568, '127.0.0.1', '2022-12-05 04:30:41', '2022-12-05 04:30:41'),
(569, '127.0.0.1', '2022-12-05 04:30:42', '2022-12-05 04:30:42'),
(570, '127.0.0.1', '2022-12-05 04:30:55', '2022-12-05 04:30:55'),
(571, '127.0.0.1', '2022-12-05 04:30:55', '2022-12-05 04:30:55'),
(572, '127.0.0.1', '2022-12-05 04:31:13', '2022-12-05 04:31:13'),
(573, '127.0.0.1', '2022-12-05 04:31:14', '2022-12-05 04:31:14'),
(574, '127.0.0.1', '2022-12-05 04:33:05', '2022-12-05 04:33:05'),
(575, '127.0.0.1', '2022-12-05 04:33:05', '2022-12-05 04:33:05'),
(576, '127.0.0.1', '2022-12-05 04:33:12', '2022-12-05 04:33:12'),
(577, '127.0.0.1', '2022-12-05 04:33:14', '2022-12-05 04:33:14'),
(578, '127.0.0.1', '2022-12-05 04:33:14', '2022-12-05 04:33:14'),
(579, '127.0.0.1', '2022-12-05 04:33:19', '2022-12-05 04:33:19'),
(580, '127.0.0.1', '2022-12-05 04:33:20', '2022-12-05 04:33:20'),
(581, '127.0.0.1', '2022-12-05 04:33:21', '2022-12-05 04:33:21'),
(582, '127.0.0.1', '2022-12-05 04:33:51', '2022-12-05 04:33:51'),
(583, '127.0.0.1', '2022-12-05 04:33:51', '2022-12-05 04:33:51'),
(584, '127.0.0.1', '2022-12-05 04:33:55', '2022-12-05 04:33:55'),
(585, '127.0.0.1', '2022-12-05 04:34:03', '2022-12-05 04:34:03'),
(586, '127.0.0.1', '2022-12-05 04:34:04', '2022-12-05 04:34:04'),
(587, '127.0.0.1', '2022-12-05 04:34:10', '2022-12-05 04:34:10'),
(588, '127.0.0.1', '2022-12-05 04:34:10', '2022-12-05 04:34:10'),
(589, '127.0.0.1', '2022-12-05 04:35:19', '2022-12-05 04:35:19'),
(590, '127.0.0.1', '2022-12-05 04:35:20', '2022-12-05 04:35:20'),
(591, '127.0.0.1', '2022-12-05 04:35:21', '2022-12-05 04:35:21'),
(592, '127.0.0.1', '2022-12-05 04:35:22', '2022-12-05 04:35:22'),
(593, '127.0.0.1', '2022-12-05 04:35:22', '2022-12-05 04:35:22'),
(594, '127.0.0.1', '2022-12-05 04:35:53', '2022-12-05 04:35:53'),
(595, '127.0.0.1', '2022-12-05 04:35:54', '2022-12-05 04:35:54'),
(596, '127.0.0.1', '2022-12-05 04:38:44', '2022-12-05 04:38:44'),
(597, '127.0.0.1', '2022-12-05 04:38:44', '2022-12-05 04:38:44'),
(598, '127.0.0.1', '2022-12-05 04:38:50', '2022-12-05 04:38:50'),
(599, '127.0.0.1', '2022-12-05 04:38:50', '2022-12-05 04:38:50'),
(600, '127.0.0.1', '2022-12-05 04:41:41', '2022-12-05 04:41:41'),
(601, '127.0.0.1', '2022-12-05 04:41:41', '2022-12-05 04:41:41'),
(602, '127.0.0.1', '2022-12-05 04:41:44', '2022-12-05 04:41:44'),
(603, '127.0.0.1', '2022-12-05 04:41:44', '2022-12-05 04:41:44'),
(604, '127.0.0.1', '2022-12-05 04:42:07', '2022-12-05 04:42:07'),
(605, '127.0.0.1', '2022-12-05 04:42:07', '2022-12-05 04:42:07'),
(606, '127.0.0.1', '2022-12-05 04:42:10', '2022-12-05 04:42:10'),
(607, '127.0.0.1', '2022-12-05 04:42:11', '2022-12-05 04:42:11'),
(608, '127.0.0.1', '2022-12-05 04:42:57', '2022-12-05 04:42:57'),
(609, '127.0.0.1', '2022-12-05 04:42:58', '2022-12-05 04:42:58'),
(610, '127.0.0.1', '2022-12-05 04:46:44', '2022-12-05 04:46:44'),
(611, '127.0.0.1', '2022-12-05 04:46:44', '2022-12-05 04:46:44'),
(612, '127.0.0.1', '2022-12-05 04:46:48', '2022-12-05 04:46:48'),
(613, '127.0.0.1', '2022-12-05 04:46:50', '2022-12-05 04:46:50'),
(614, '127.0.0.1', '2022-12-05 04:46:50', '2022-12-05 04:46:50'),
(615, '127.0.0.1', '2022-12-05 04:47:13', '2022-12-05 04:47:13'),
(616, '127.0.0.1', '2022-12-05 04:47:14', '2022-12-05 04:47:14'),
(617, '127.0.0.1', '2022-12-05 04:47:17', '2022-12-05 04:47:17'),
(618, '127.0.0.1', '2022-12-05 04:47:17', '2022-12-05 04:47:17'),
(619, '127.0.0.1', '2022-12-05 04:47:26', '2022-12-05 04:47:26'),
(620, '127.0.0.1', '2022-12-05 04:47:26', '2022-12-05 04:47:26'),
(621, '127.0.0.1', '2022-12-05 04:47:29', '2022-12-05 04:47:29'),
(622, '127.0.0.1', '2022-12-05 04:47:31', '2022-12-05 04:47:31'),
(623, '127.0.0.1', '2022-12-05 04:47:31', '2022-12-05 04:47:31'),
(624, '127.0.0.1', '2022-12-05 04:47:35', '2022-12-05 04:47:35'),
(625, '127.0.0.1', '2022-12-05 04:47:37', '2022-12-05 04:47:37'),
(626, '127.0.0.1', '2022-12-05 04:49:09', '2022-12-05 04:49:09'),
(627, '127.0.0.1', '2022-12-05 04:49:09', '2022-12-05 04:49:09'),
(628, '127.0.0.1', '2022-12-05 04:49:29', '2022-12-05 04:49:29'),
(629, '127.0.0.1', '2022-12-05 04:49:29', '2022-12-05 04:49:29'),
(630, '127.0.0.1', '2022-12-05 04:49:32', '2022-12-05 04:49:32'),
(631, '127.0.0.1', '2022-12-05 04:49:33', '2022-12-05 04:49:33'),
(632, '127.0.0.1', '2022-12-05 04:49:34', '2022-12-05 04:49:34'),
(633, '127.0.0.1', '2022-12-05 04:49:43', '2022-12-05 04:49:43'),
(634, '127.0.0.1', '2022-12-05 04:49:43', '2022-12-05 04:49:43'),
(635, '127.0.0.1', '2022-12-05 04:49:46', '2022-12-05 04:49:46'),
(636, '127.0.0.1', '2022-12-05 04:49:46', '2022-12-05 04:49:46'),
(637, '127.0.0.1', '2022-12-05 04:49:54', '2022-12-05 04:49:54'),
(638, '127.0.0.1', '2022-12-05 04:49:55', '2022-12-05 04:49:55'),
(639, '127.0.0.1', '2022-12-05 04:49:57', '2022-12-05 04:49:57'),
(640, '127.0.0.1', '2022-12-05 04:49:58', '2022-12-05 04:49:58'),
(641, '127.0.0.1', '2022-12-05 04:49:59', '2022-12-05 04:49:59'),
(642, '127.0.0.1', '2022-12-05 04:50:03', '2022-12-05 04:50:03'),
(643, '127.0.0.1', '2022-12-05 04:50:04', '2022-12-05 04:50:04'),
(644, '127.0.0.1', '2022-12-05 04:52:13', '2022-12-05 04:52:13'),
(645, '127.0.0.1', '2022-12-05 04:52:13', '2022-12-05 04:52:13'),
(646, '127.0.0.1', '2022-12-05 04:52:14', '2022-12-05 04:52:14'),
(647, '127.0.0.1', '2022-12-05 04:52:15', '2022-12-05 04:52:15'),
(648, '127.0.0.1', '2022-12-05 04:52:16', '2022-12-05 04:52:16'),
(649, '127.0.0.1', '2022-12-05 04:53:35', '2022-12-05 04:53:35'),
(650, '127.0.0.1', '2022-12-05 04:53:35', '2022-12-05 04:53:35'),
(651, '127.0.0.1', '2022-12-05 04:54:15', '2022-12-05 04:54:15'),
(652, '127.0.0.1', '2022-12-05 04:54:16', '2022-12-05 04:54:16'),
(653, '127.0.0.1', '2022-12-05 04:54:26', '2022-12-05 04:54:26'),
(654, '127.0.0.1', '2022-12-05 04:54:26', '2022-12-05 04:54:26'),
(655, '127.0.0.1', '2022-12-05 04:54:34', '2022-12-05 04:54:34'),
(656, '127.0.0.1', '2022-12-05 04:54:35', '2022-12-05 04:54:35'),
(657, '127.0.0.1', '2022-12-05 04:54:38', '2022-12-05 04:54:38'),
(658, '127.0.0.1', '2022-12-05 04:54:38', '2022-12-05 04:54:38'),
(659, '127.0.0.1', '2022-12-05 04:54:46', '2022-12-05 04:54:46'),
(660, '127.0.0.1', '2022-12-05 04:54:47', '2022-12-05 04:54:47'),
(661, '127.0.0.1', '2022-12-05 04:54:48', '2022-12-05 04:54:48'),
(662, '127.0.0.1', '2022-12-05 04:55:02', '2022-12-05 04:55:02'),
(663, '127.0.0.1', '2022-12-05 04:55:02', '2022-12-05 04:55:02'),
(664, '127.0.0.1', '2022-12-05 04:55:26', '2022-12-05 04:55:26'),
(665, '127.0.0.1', '2022-12-05 04:55:26', '2022-12-05 04:55:26'),
(666, '127.0.0.1', '2022-12-05 04:55:30', '2022-12-05 04:55:30'),
(667, '127.0.0.1', '2022-12-05 04:55:31', '2022-12-05 04:55:31'),
(668, '127.0.0.1', '2022-12-05 04:55:56', '2022-12-05 04:55:56'),
(669, '127.0.0.1', '2022-12-05 04:55:56', '2022-12-05 04:55:56'),
(670, '127.0.0.1', '2022-12-05 04:55:59', '2022-12-05 04:55:59'),
(671, '127.0.0.1', '2022-12-05 04:56:00', '2022-12-05 04:56:00'),
(672, '127.0.0.1', '2022-12-05 04:56:05', '2022-12-05 04:56:05'),
(673, '127.0.0.1', '2022-12-05 04:56:06', '2022-12-05 04:56:06'),
(674, '127.0.0.1', '2022-12-05 04:56:09', '2022-12-05 04:56:09'),
(675, '127.0.0.1', '2022-12-05 04:56:10', '2022-12-05 04:56:10'),
(676, '127.0.0.1', '2022-12-05 04:56:53', '2022-12-05 04:56:53'),
(677, '127.0.0.1', '2022-12-05 04:56:53', '2022-12-05 04:56:53'),
(678, '127.0.0.1', '2022-12-05 04:56:55', '2022-12-05 04:56:55'),
(679, '127.0.0.1', '2022-12-05 04:56:57', '2022-12-05 04:56:57'),
(680, '127.0.0.1', '2022-12-05 04:56:57', '2022-12-05 04:56:57'),
(681, '127.0.0.1', '2022-12-05 04:57:01', '2022-12-05 04:57:01'),
(682, '127.0.0.1', '2022-12-05 04:57:02', '2022-12-05 04:57:02'),
(683, '127.0.0.1', '2022-12-05 04:57:13', '2022-12-05 04:57:13'),
(684, '127.0.0.1', '2022-12-05 04:57:13', '2022-12-05 04:57:13'),
(685, '127.0.0.1', '2022-12-05 04:57:14', '2022-12-05 04:57:14'),
(686, '127.0.0.1', '2022-12-05 04:57:17', '2022-12-05 04:57:17'),
(687, '127.0.0.1', '2022-12-05 04:57:18', '2022-12-05 04:57:18'),
(688, '127.0.0.1', '2022-12-05 04:57:27', '2022-12-05 04:57:27'),
(689, '127.0.0.1', '2022-12-05 04:57:27', '2022-12-05 04:57:27'),
(690, '127.0.0.1', '2022-12-05 04:57:44', '2022-12-05 04:57:44'),
(691, '127.0.0.1', '2022-12-05 04:57:44', '2022-12-05 04:57:44'),
(692, '127.0.0.1', '2022-12-05 04:57:48', '2022-12-05 04:57:48'),
(693, '127.0.0.1', '2022-12-05 04:57:49', '2022-12-05 04:57:49'),
(694, '127.0.0.1', '2022-12-05 04:57:50', '2022-12-05 04:57:50'),
(695, '127.0.0.1', '2022-12-05 04:58:56', '2022-12-05 04:58:56'),
(696, '127.0.0.1', '2022-12-05 04:58:57', '2022-12-05 04:58:57'),
(697, '127.0.0.1', '2022-12-05 04:58:59', '2022-12-05 04:58:59'),
(698, '127.0.0.1', '2022-12-05 04:59:00', '2022-12-05 04:59:00'),
(699, '127.0.0.1', '2022-12-05 05:00:08', '2022-12-05 05:00:08'),
(700, '127.0.0.1', '2022-12-05 05:00:18', '2022-12-05 05:00:18'),
(701, '127.0.0.1', '2022-12-05 05:00:18', '2022-12-05 05:00:18'),
(702, '127.0.0.1', '2022-12-05 05:00:22', '2022-12-05 05:00:22'),
(703, '127.0.0.1', '2022-12-05 05:00:24', '2022-12-05 05:00:24'),
(704, '127.0.0.1', '2022-12-05 05:00:25', '2022-12-05 05:00:25'),
(705, '127.0.0.1', '2022-12-05 05:00:49', '2022-12-05 05:00:49'),
(706, '127.0.0.1', '2022-12-05 05:00:50', '2022-12-05 05:00:50'),
(707, '127.0.0.1', '2022-12-05 05:01:39', '2022-12-05 05:01:39'),
(708, '127.0.0.1', '2022-12-05 05:01:39', '2022-12-05 05:01:39'),
(709, '127.0.0.1', '2022-12-05 05:01:41', '2022-12-05 05:01:41'),
(710, '127.0.0.1', '2022-12-05 05:01:43', '2022-12-05 05:01:43'),
(711, '127.0.0.1', '2022-12-05 05:01:43', '2022-12-05 05:01:43'),
(712, '127.0.0.1', '2022-12-05 05:02:03', '2022-12-05 05:02:03'),
(713, '127.0.0.1', '2022-12-05 05:02:04', '2022-12-05 05:02:04'),
(714, '127.0.0.1', '2022-12-05 05:02:48', '2022-12-05 05:02:48'),
(715, '127.0.0.1', '2022-12-05 05:02:49', '2022-12-05 05:02:49'),
(716, '127.0.0.1', '2022-12-05 05:02:51', '2022-12-05 05:02:51'),
(717, '127.0.0.1', '2022-12-05 05:02:53', '2022-12-05 05:02:53'),
(718, '127.0.0.1', '2022-12-05 05:02:53', '2022-12-05 05:02:53'),
(719, '127.0.0.1', '2022-12-05 05:03:05', '2022-12-05 05:03:05'),
(720, '127.0.0.1', '2022-12-05 05:03:05', '2022-12-05 05:03:05'),
(721, '127.0.0.1', '2022-12-05 05:03:42', '2022-12-05 05:03:42'),
(722, '127.0.0.1', '2022-12-05 05:03:42', '2022-12-05 05:03:42'),
(723, '127.0.0.1', '2022-12-05 05:03:45', '2022-12-05 05:03:45'),
(724, '127.0.0.1', '2022-12-05 05:03:45', '2022-12-05 05:03:45'),
(725, '127.0.0.1', '2022-12-05 05:04:16', '2022-12-05 05:04:16'),
(726, '127.0.0.1', '2022-12-05 05:04:16', '2022-12-05 05:04:16'),
(727, '127.0.0.1', '2022-12-05 05:04:19', '2022-12-05 05:04:19'),
(728, '127.0.0.1', '2022-12-05 05:04:20', '2022-12-05 05:04:20'),
(729, '127.0.0.1', '2022-12-05 05:04:21', '2022-12-05 05:04:21'),
(730, '127.0.0.1', '2022-12-05 05:06:38', '2022-12-05 05:06:38'),
(731, '127.0.0.1', '2022-12-05 05:06:40', '2022-12-05 05:06:40'),
(732, '127.0.0.1', '2022-12-05 05:06:40', '2022-12-05 05:06:40'),
(733, '127.0.0.1', '2022-12-05 05:07:03', '2022-12-05 05:07:03'),
(734, '127.0.0.1', '2022-12-05 05:07:05', '2022-12-05 05:07:05'),
(735, '127.0.0.1', '2022-12-05 05:07:06', '2022-12-05 05:07:06'),
(736, '127.0.0.1', '2022-12-05 05:07:28', '2022-12-05 05:07:28'),
(737, '127.0.0.1', '2022-12-05 05:07:30', '2022-12-05 05:07:30'),
(738, '127.0.0.1', '2022-12-05 05:07:30', '2022-12-05 05:07:30'),
(739, '127.0.0.1', '2022-12-05 05:08:04', '2022-12-05 05:08:04'),
(740, '127.0.0.1', '2022-12-05 05:08:05', '2022-12-05 05:08:05'),
(741, '127.0.0.1', '2022-12-05 05:08:06', '2022-12-05 05:08:06'),
(742, '127.0.0.1', '2022-12-05 05:08:26', '2022-12-05 05:08:26'),
(743, '127.0.0.1', '2022-12-05 05:08:27', '2022-12-05 05:08:27'),
(744, '127.0.0.1', '2022-12-05 05:08:28', '2022-12-05 05:08:28'),
(745, '127.0.0.1', '2022-12-05 05:08:50', '2022-12-05 05:08:50'),
(746, '127.0.0.1', '2022-12-05 05:08:51', '2022-12-05 05:08:51'),
(747, '127.0.0.1', '2022-12-05 05:08:52', '2022-12-05 05:08:52'),
(748, '127.0.0.1', '2022-12-05 05:09:07', '2022-12-05 05:09:07'),
(749, '127.0.0.1', '2022-12-05 05:09:08', '2022-12-05 05:09:08'),
(750, '127.0.0.1', '2022-12-05 05:09:09', '2022-12-05 05:09:09'),
(751, '127.0.0.1', '2022-12-05 05:09:17', '2022-12-05 05:09:17'),
(752, '127.0.0.1', '2022-12-05 05:09:18', '2022-12-05 05:09:18'),
(753, '127.0.0.1', '2022-12-05 05:09:19', '2022-12-05 05:09:19'),
(754, '127.0.0.1', '2022-12-05 05:16:38', '2022-12-05 05:16:38'),
(755, '127.0.0.1', '2022-12-05 05:16:40', '2022-12-05 05:16:40'),
(756, '127.0.0.1', '2022-12-05 05:16:41', '2022-12-05 05:16:41'),
(757, '127.0.0.1', '2022-12-05 05:17:02', '2022-12-05 05:17:02'),
(758, '127.0.0.1', '2022-12-05 05:17:04', '2022-12-05 05:17:04'),
(759, '127.0.0.1', '2022-12-05 05:17:04', '2022-12-05 05:17:04'),
(760, '127.0.0.1', '2022-12-05 05:17:25', '2022-12-05 05:17:25'),
(761, '127.0.0.1', '2022-12-05 05:17:25', '2022-12-05 05:17:25'),
(762, '127.0.0.1', '2022-12-05 05:17:43', '2022-12-05 05:17:43'),
(763, '127.0.0.1', '2022-12-05 05:17:43', '2022-12-05 05:17:43'),
(764, '127.0.0.1', '2022-12-05 05:17:46', '2022-12-05 05:17:46'),
(765, '127.0.0.1', '2022-12-05 05:17:46', '2022-12-05 05:17:46'),
(766, '127.0.0.1', '2022-12-05 05:17:54', '2022-12-05 05:17:54'),
(767, '127.0.0.1', '2022-12-05 05:17:54', '2022-12-05 05:17:54'),
(768, '127.0.0.1', '2022-12-05 05:17:56', '2022-12-05 05:17:56'),
(769, '127.0.0.1', '2022-12-05 05:17:57', '2022-12-05 05:17:57'),
(770, '127.0.0.1', '2022-12-05 05:18:06', '2022-12-05 05:18:06'),
(771, '127.0.0.1', '2022-12-05 05:18:07', '2022-12-05 05:18:07'),
(772, '127.0.0.1', '2022-12-05 05:18:10', '2022-12-05 05:18:10'),
(773, '127.0.0.1', '2022-12-05 05:18:11', '2022-12-05 05:18:11'),
(774, '127.0.0.1', '2022-12-05 05:18:19', '2022-12-05 05:18:19'),
(775, '127.0.0.1', '2022-12-05 05:18:20', '2022-12-05 05:18:20'),
(776, '127.0.0.1', '2022-12-05 05:18:23', '2022-12-05 05:18:23'),
(777, '127.0.0.1', '2022-12-05 05:18:23', '2022-12-05 05:18:23'),
(778, '127.0.0.1', '2022-12-05 05:18:30', '2022-12-05 05:18:30'),
(779, '127.0.0.1', '2022-12-05 05:18:30', '2022-12-05 05:18:30'),
(780, '127.0.0.1', '2022-12-05 05:18:32', '2022-12-05 05:18:32'),
(781, '127.0.0.1', '2022-12-05 05:18:33', '2022-12-05 05:18:33');
INSERT INTO `visitors` (`id`, `ip`, `created_at`, `updated_at`) VALUES
(782, '127.0.0.1', '2022-12-05 05:18:41', '2022-12-05 05:18:41'),
(783, '127.0.0.1', '2022-12-05 05:18:42', '2022-12-05 05:18:42'),
(784, '127.0.0.1', '2022-12-05 05:18:43', '2022-12-05 05:18:43'),
(785, '127.0.0.1', '2022-12-05 05:18:44', '2022-12-05 05:18:44'),
(786, '127.0.0.1', '2022-12-05 05:18:51', '2022-12-05 05:18:51'),
(787, '127.0.0.1', '2022-12-05 05:18:52', '2022-12-05 05:18:52'),
(788, '127.0.0.1', '2022-12-05 05:18:55', '2022-12-05 05:18:55'),
(789, '127.0.0.1', '2022-12-05 05:18:56', '2022-12-05 05:18:56'),
(790, '127.0.0.1', '2022-12-05 05:19:04', '2022-12-05 05:19:04'),
(791, '127.0.0.1', '2022-12-05 05:19:04', '2022-12-05 05:19:04'),
(792, '127.0.0.1', '2022-12-05 05:19:07', '2022-12-05 05:19:07'),
(793, '127.0.0.1', '2022-12-05 05:19:07', '2022-12-05 05:19:07'),
(794, '127.0.0.1', '2022-12-05 05:19:15', '2022-12-05 05:19:15'),
(795, '127.0.0.1', '2022-12-05 05:19:16', '2022-12-05 05:19:16'),
(796, '127.0.0.1', '2022-12-05 05:19:17', '2022-12-05 05:19:17'),
(797, '127.0.0.1', '2022-12-05 05:19:18', '2022-12-05 05:19:18'),
(798, '127.0.0.1', '2022-12-05 05:19:25', '2022-12-05 05:19:25'),
(799, '127.0.0.1', '2022-12-05 05:19:25', '2022-12-05 05:19:25'),
(800, '127.0.0.1', '2022-12-05 05:19:27', '2022-12-05 05:19:27'),
(801, '127.0.0.1', '2022-12-05 05:19:27', '2022-12-05 05:19:27'),
(802, '127.0.0.1', '2022-12-05 05:19:35', '2022-12-05 05:19:35'),
(803, '127.0.0.1', '2022-12-05 05:19:35', '2022-12-05 05:19:35'),
(804, '127.0.0.1', '2022-12-05 05:19:36', '2022-12-05 05:19:36'),
(805, '127.0.0.1', '2022-12-05 05:19:37', '2022-12-05 05:19:37'),
(806, '127.0.0.1', '2022-12-05 05:19:38', '2022-12-05 05:19:38'),
(807, '127.0.0.1', '2022-12-05 05:19:51', '2022-12-05 05:19:51'),
(808, '127.0.0.1', '2022-12-05 05:19:52', '2022-12-05 05:19:52'),
(809, '127.0.0.1', '2022-12-05 05:20:04', '2022-12-05 05:20:04'),
(810, '127.0.0.1', '2022-12-05 05:20:04', '2022-12-05 05:20:04'),
(811, '127.0.0.1', '2022-12-05 05:20:08', '2022-12-05 05:20:08'),
(812, '127.0.0.1', '2022-12-05 05:20:09', '2022-12-05 05:20:09'),
(813, '127.0.0.1', '2022-12-05 05:20:12', '2022-12-05 05:20:12'),
(814, '127.0.0.1', '2022-12-05 05:20:12', '2022-12-05 05:20:12'),
(815, '127.0.0.1', '2022-12-05 05:20:20', '2022-12-05 05:20:20'),
(816, '127.0.0.1', '2022-12-05 05:20:21', '2022-12-05 05:20:21'),
(817, '127.0.0.1', '2022-12-05 05:20:26', '2022-12-05 05:20:26'),
(818, '127.0.0.1', '2022-12-05 05:20:27', '2022-12-05 05:20:27'),
(819, '127.0.0.1', '2022-12-05 05:20:28', '2022-12-05 05:20:28'),
(820, '127.0.0.1', '2022-12-05 05:20:55', '2022-12-05 05:20:55'),
(821, '127.0.0.1', '2022-12-05 05:20:56', '2022-12-05 05:20:56'),
(822, '127.0.0.1', '2022-12-05 05:21:08', '2022-12-05 05:21:08'),
(823, '127.0.0.1', '2022-12-05 05:21:16', '2022-12-05 05:21:16'),
(824, '127.0.0.1', '2022-12-05 05:21:16', '2022-12-05 05:21:16'),
(825, '127.0.0.1', '2022-12-05 05:21:17', '2022-12-05 05:21:17'),
(826, '127.0.0.1', '2022-12-05 05:21:19', '2022-12-05 05:21:19'),
(827, '127.0.0.1', '2022-12-05 05:21:19', '2022-12-05 05:21:19'),
(828, '127.0.0.1', '2022-12-05 05:31:15', '2022-12-05 05:31:15'),
(829, '127.0.0.1', '2022-12-05 05:31:16', '2022-12-05 05:31:16'),
(830, '127.0.0.1', '2022-12-05 05:31:25', '2022-12-05 05:31:25'),
(831, '127.0.0.1', '2022-12-05 05:31:26', '2022-12-05 05:31:26'),
(832, '127.0.0.1', '2022-12-05 05:31:47', '2022-12-05 05:31:47'),
(833, '127.0.0.1', '2022-12-05 07:21:53', '2022-12-05 07:21:53'),
(834, '127.0.0.1', '2022-12-05 07:21:58', '2022-12-05 07:21:58'),
(835, '127.0.0.1', '2022-12-05 07:22:55', '2022-12-05 07:22:55'),
(836, '127.0.0.1', '2022-12-05 07:22:59', '2022-12-05 07:22:59'),
(837, '127.0.0.1', '2022-12-05 07:23:03', '2022-12-05 07:23:03'),
(838, '127.0.0.1', '2022-12-05 07:23:14', '2022-12-05 07:23:14'),
(839, '127.0.0.1', '2022-12-05 07:23:17', '2022-12-05 07:23:17'),
(840, '127.0.0.1', '2022-12-05 07:23:49', '2022-12-05 07:23:49'),
(841, '127.0.0.1', '2022-12-05 07:23:52', '2022-12-05 07:23:52'),
(842, '127.0.0.1', '2022-12-05 07:24:04', '2022-12-05 07:24:04'),
(843, '127.0.0.1', '2022-12-05 07:24:08', '2022-12-05 07:24:08'),
(844, '127.0.0.1', '2022-12-05 07:24:26', '2022-12-05 07:24:26'),
(845, '127.0.0.1', '2022-12-05 07:24:30', '2022-12-05 07:24:30'),
(846, '127.0.0.1', '2022-12-05 07:24:42', '2022-12-05 07:24:42'),
(847, '127.0.0.1', '2022-12-05 07:24:47', '2022-12-05 07:24:47'),
(848, '127.0.0.1', '2022-12-05 07:25:29', '2022-12-05 07:25:29'),
(849, '127.0.0.1', '2022-12-05 07:25:33', '2022-12-05 07:25:33'),
(850, '127.0.0.1', '2022-12-05 07:26:08', '2022-12-05 07:26:08'),
(851, '127.0.0.1', '2022-12-05 07:26:13', '2022-12-05 07:26:13'),
(852, '127.0.0.1', '2022-12-05 07:26:55', '2022-12-05 07:26:55'),
(853, '127.0.0.1', '2022-12-05 07:27:03', '2022-12-05 07:27:03'),
(854, '127.0.0.1', '2022-12-05 07:27:43', '2022-12-05 07:27:43'),
(855, '127.0.0.1', '2022-12-05 07:27:47', '2022-12-05 07:27:47'),
(856, '127.0.0.1', '2022-12-05 07:28:26', '2022-12-05 07:28:26'),
(857, '127.0.0.1', '2022-12-05 07:28:31', '2022-12-05 07:28:31'),
(858, '127.0.0.1', '2022-12-05 07:28:33', '2022-12-05 07:28:33'),
(859, '127.0.0.1', '2022-12-05 07:29:19', '2022-12-05 07:29:19'),
(860, '127.0.0.1', '2022-12-05 07:29:21', '2022-12-05 07:29:21'),
(861, '127.0.0.1', '2022-12-05 07:29:50', '2022-12-05 07:29:50'),
(862, '127.0.0.1', '2022-12-05 07:29:52', '2022-12-05 07:29:52'),
(863, '127.0.0.1', '2022-12-05 07:31:05', '2022-12-05 07:31:05'),
(864, '127.0.0.1', '2022-12-05 07:31:08', '2022-12-05 07:31:08'),
(865, '127.0.0.1', '2022-12-05 07:32:37', '2022-12-05 07:32:37'),
(866, '127.0.0.1', '2022-12-05 07:34:01', '2022-12-05 07:34:01'),
(867, '127.0.0.1', '2022-12-05 07:34:03', '2022-12-05 07:34:03'),
(868, '127.0.0.1', '2022-12-05 07:34:05', '2022-12-05 07:34:05'),
(869, '127.0.0.1', '2022-12-05 07:35:08', '2022-12-05 07:35:08'),
(870, '127.0.0.1', '2022-12-05 07:35:10', '2022-12-05 07:35:10'),
(871, '127.0.0.1', '2022-12-05 07:35:12', '2022-12-05 07:35:12'),
(872, '127.0.0.1', '2022-12-05 07:36:32', '2022-12-05 07:36:32'),
(873, '127.0.0.1', '2022-12-05 07:36:35', '2022-12-05 07:36:35'),
(874, '127.0.0.1', '2022-12-05 07:36:38', '2022-12-05 07:36:38'),
(875, '127.0.0.1', '2022-12-05 07:36:46', '2022-12-05 07:36:46'),
(876, '127.0.0.1', '2022-12-05 07:37:27', '2022-12-05 07:37:27'),
(877, '127.0.0.1', '2022-12-05 07:37:30', '2022-12-05 07:37:30'),
(878, '127.0.0.1', '2022-12-05 07:40:34', '2022-12-05 07:40:34'),
(879, '127.0.0.1', '2022-12-05 07:41:03', '2022-12-05 07:41:03'),
(880, '127.0.0.1', '2022-12-05 07:41:04', '2022-12-05 07:41:04'),
(881, '127.0.0.1', '2022-12-05 07:41:06', '2022-12-05 07:41:06'),
(882, '127.0.0.1', '2022-12-05 07:43:12', '2022-12-05 07:43:12'),
(883, '127.0.0.1', '2022-12-05 07:43:14', '2022-12-05 07:43:14'),
(884, '127.0.0.1', '2022-12-05 07:43:16', '2022-12-05 07:43:16'),
(885, '127.0.0.1', '2022-12-05 07:45:26', '2022-12-05 07:45:26'),
(886, '127.0.0.1', '2022-12-05 07:45:27', '2022-12-05 07:45:27'),
(887, '127.0.0.1', '2022-12-05 07:45:29', '2022-12-05 07:45:29'),
(888, '127.0.0.1', '2022-12-05 07:46:56', '2022-12-05 07:46:56'),
(889, '127.0.0.1', '2022-12-05 07:46:56', '2022-12-05 07:46:56'),
(890, '127.0.0.1', '2022-12-05 07:46:58', '2022-12-05 07:46:58'),
(891, '127.0.0.1', '2022-12-05 07:48:18', '2022-12-05 07:48:18'),
(892, '127.0.0.1', '2022-12-05 07:48:57', '2022-12-05 07:48:57'),
(893, '127.0.0.1', '2022-12-05 07:50:05', '2022-12-05 07:50:05'),
(894, '127.0.0.1', '2022-12-05 07:50:06', '2022-12-05 07:50:06'),
(895, '127.0.0.1', '2022-12-05 07:50:08', '2022-12-05 07:50:08'),
(896, '127.0.0.1', '2022-12-05 07:50:26', '2022-12-05 07:50:26'),
(897, '127.0.0.1', '2022-12-05 07:52:30', '2022-12-05 07:52:30'),
(898, '127.0.0.1', '2022-12-05 07:52:32', '2022-12-05 07:52:32'),
(899, '127.0.0.1', '2022-12-05 07:52:34', '2022-12-05 07:52:34'),
(900, '127.0.0.1', '2022-12-05 07:52:53', '2022-12-05 07:52:53'),
(901, '127.0.0.1', '2022-12-05 07:52:55', '2022-12-05 07:52:55'),
(902, '127.0.0.1', '2022-12-05 07:53:03', '2022-12-05 07:53:03'),
(903, '127.0.0.1', '2022-12-05 07:53:07', '2022-12-05 07:53:07'),
(904, '127.0.0.1', '2022-12-05 07:56:46', '2022-12-05 07:56:46'),
(905, '127.0.0.1', '2022-12-05 07:56:55', '2022-12-05 07:56:55'),
(906, '127.0.0.1', '2022-12-05 07:56:57', '2022-12-05 07:56:57'),
(907, '127.0.0.1', '2022-12-05 07:57:02', '2022-12-05 07:57:02'),
(908, '127.0.0.1', '2022-12-05 07:57:03', '2022-12-05 07:57:03'),
(909, '127.0.0.1', '2022-12-05 07:59:41', '2022-12-05 07:59:41'),
(910, '127.0.0.1', '2022-12-05 07:59:42', '2022-12-05 07:59:42'),
(911, '127.0.0.1', '2022-12-05 07:59:57', '2022-12-05 07:59:57'),
(912, '127.0.0.1', '2022-12-05 07:59:59', '2022-12-05 07:59:59'),
(913, '127.0.0.1', '2022-12-05 08:00:22', '2022-12-05 08:00:22'),
(914, '127.0.0.1', '2022-12-05 08:00:24', '2022-12-05 08:00:24'),
(915, '127.0.0.1', '2022-12-05 08:00:34', '2022-12-05 08:00:34'),
(916, '127.0.0.1', '2022-12-05 08:00:36', '2022-12-05 08:00:36'),
(917, '127.0.0.1', '2022-12-05 08:01:01', '2022-12-05 08:01:01'),
(918, '127.0.0.1', '2022-12-05 08:01:30', '2022-12-05 08:01:30'),
(919, '127.0.0.1', '2022-12-05 08:01:35', '2022-12-05 08:01:35'),
(920, '127.0.0.1', '2022-12-05 08:02:02', '2022-12-05 08:02:02'),
(921, '127.0.0.1', '2022-12-05 08:03:25', '2022-12-05 08:03:25'),
(922, '127.0.0.1', '2022-12-05 08:03:36', '2022-12-05 08:03:36'),
(923, '127.0.0.1', '2022-12-23 12:08:14', '2022-12-23 12:08:14'),
(924, '127.0.0.1', '2022-12-23 12:08:16', '2022-12-23 12:08:16'),
(925, '127.0.0.1', '2022-12-23 12:08:20', '2022-12-23 12:08:20'),
(926, '127.0.0.1', '2022-12-23 12:08:24', '2022-12-23 12:08:24'),
(927, '127.0.0.1', '2022-12-23 12:08:36', '2022-12-23 12:08:36'),
(928, '127.0.0.1', '2022-12-23 12:08:40', '2022-12-23 12:08:40'),
(929, '127.0.0.1', '2022-12-23 12:09:31', '2022-12-23 12:09:31'),
(930, '127.0.0.1', '2022-12-23 12:09:34', '2022-12-23 12:09:34'),
(931, '127.0.0.1', '2022-12-23 12:11:15', '2022-12-23 12:11:15'),
(932, '127.0.0.1', '2022-12-23 12:11:17', '2022-12-23 12:11:17'),
(933, '127.0.0.1', '2022-12-23 12:12:38', '2022-12-23 12:12:38'),
(934, '127.0.0.1', '2022-12-23 12:12:47', '2022-12-23 12:12:47'),
(935, '127.0.0.1', '2022-12-23 12:14:55', '2022-12-23 12:14:55'),
(936, '127.0.0.1', '2022-12-23 12:14:58', '2022-12-23 12:14:58'),
(937, '127.0.0.1', '2022-12-23 12:16:04', '2022-12-23 12:16:04'),
(938, '127.0.0.1', '2022-12-23 12:16:07', '2022-12-23 12:16:07'),
(939, '127.0.0.1', '2022-12-23 12:22:45', '2022-12-23 12:22:45'),
(940, '127.0.0.1', '2022-12-23 12:22:48', '2022-12-23 12:22:48'),
(941, '127.0.0.1', '2022-12-23 12:22:50', '2022-12-23 12:22:50'),
(942, '127.0.0.1', '2022-12-23 12:26:43', '2022-12-23 12:26:43'),
(943, '127.0.0.1', '2022-12-23 12:26:46', '2022-12-23 12:26:46'),
(944, '127.0.0.1', '2022-12-23 12:26:48', '2022-12-23 12:26:48'),
(945, '127.0.0.1', '2022-12-23 12:27:20', '2022-12-23 12:27:20'),
(946, '127.0.0.1', '2022-12-23 12:27:22', '2022-12-23 12:27:22'),
(947, '127.0.0.1', '2022-12-23 12:27:24', '2022-12-23 12:27:24'),
(948, '127.0.0.1', '2022-12-23 12:27:40', '2022-12-23 12:27:40'),
(949, '127.0.0.1', '2023-01-02 03:19:50', '2023-01-02 03:19:50'),
(950, '127.0.0.1', '2023-01-02 03:19:57', '2023-01-02 03:19:57'),
(951, '127.0.0.1', '2023-01-02 03:19:58', '2023-01-02 03:19:58'),
(952, '127.0.0.1', '2023-01-02 03:20:38', '2023-01-02 03:20:38'),
(953, '127.0.0.1', '2023-01-02 03:20:38', '2023-01-02 03:20:38'),
(954, '127.0.0.1', '2023-01-02 03:20:53', '2023-01-02 03:20:53'),
(955, '127.0.0.1', '2023-01-02 03:20:54', '2023-01-02 03:20:54'),
(956, '127.0.0.1', '2023-01-02 03:21:04', '2023-01-02 03:21:04'),
(957, '127.0.0.1', '2023-01-02 03:21:05', '2023-01-02 03:21:05'),
(958, '127.0.0.1', '2023-01-02 03:21:21', '2023-01-02 03:21:21'),
(959, '127.0.0.1', '2023-01-02 03:21:25', '2023-01-02 03:21:25'),
(960, '127.0.0.1', '2023-01-02 03:21:27', '2023-01-02 03:21:27'),
(961, '127.0.0.1', '2023-01-02 03:21:28', '2023-01-02 03:21:28'),
(962, '127.0.0.1', '2023-01-02 03:21:36', '2023-01-02 03:21:36'),
(963, '127.0.0.1', '2023-01-02 03:21:37', '2023-01-02 03:21:37'),
(964, '127.0.0.1', '2023-01-02 03:21:44', '2023-01-02 03:21:44'),
(965, '127.0.0.1', '2023-01-02 03:21:45', '2023-01-02 03:21:45'),
(966, '127.0.0.1', '2023-01-02 03:37:06', '2023-01-02 03:37:06'),
(967, '127.0.0.1', '2023-01-02 03:37:07', '2023-01-02 03:37:07'),
(968, '127.0.0.1', '2023-01-02 03:37:20', '2023-01-02 03:37:20'),
(969, '127.0.0.1', '2023-01-02 03:37:23', '2023-01-02 03:37:23'),
(970, '127.0.0.1', '2023-01-02 03:37:26', '2023-01-02 03:37:26'),
(971, '127.0.0.1', '2023-01-02 03:37:27', '2023-01-02 03:37:27'),
(972, '127.0.0.1', '2023-01-02 03:41:23', '2023-01-02 03:41:23'),
(973, '127.0.0.1', '2023-01-02 03:41:28', '2023-01-02 03:41:28'),
(974, '127.0.0.1', '2023-01-02 03:41:37', '2023-01-02 03:41:37'),
(975, '127.0.0.1', '2023-01-02 03:42:13', '2023-01-02 03:42:13'),
(976, '127.0.0.1', '2023-01-02 03:43:02', '2023-01-02 03:43:02'),
(977, '127.0.0.1', '2023-01-02 03:44:49', '2023-01-02 03:44:49'),
(978, '127.0.0.1', '2023-01-02 03:44:53', '2023-01-02 03:44:53'),
(979, '127.0.0.1', '2023-01-02 03:44:54', '2023-01-02 03:44:54');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `agendas`
--
ALTER TABLE `agendas`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `dataindeks`
--
ALTER TABLE `dataindeks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `dataindeks_id_media_foreign` (`id_media`);

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `gallery_media`
--
ALTER TABLE `gallery_media`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `halamen`
--
ALTER TABLE `halamen`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `iklans`
--
ALTER TABLE `iklans`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `layanans`
--
ALTER TABLE `layanans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `layanans_id_media_foreign` (`id_media`);

--
-- Indeks untuk tabel `layouts`
--
ALTER TABLE `layouts`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `link_terkaits`
--
ALTER TABLE `link_terkaits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `link_terkaits_id_media_foreign` (`id_media`);

--
-- Indeks untuk tabel `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menus_parent_id_foreign` (`parent_id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `pdfs`
--
ALTER TABLE `pdfs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pengumumen`
--
ALTER TABLE `pengumumen`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pengunjungs`
--
ALTER TABLE `pengunjungs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `perjanjians`
--
ALTER TABLE `perjanjians`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pesans`
--
ALTER TABLE `pesans`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_slug_unique` (`slug`);

--
-- Indeks untuk tabel `terms`
--
ALTER TABLE `terms`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indeks untuk tabel `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `agendas`
--
ALTER TABLE `agendas`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `berita`
--
ALTER TABLE `berita`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `dataindeks`
--
ALTER TABLE `dataindeks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `gallery_media`
--
ALTER TABLE `gallery_media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `halamen`
--
ALTER TABLE `halamen`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `iklans`
--
ALTER TABLE `iklans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `layanans`
--
ALTER TABLE `layanans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `layouts`
--
ALTER TABLE `layouts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `link_terkaits`
--
ALTER TABLE `link_terkaits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `media`
--
ALTER TABLE `media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `menus`
--
ALTER TABLE `menus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT untuk tabel `pdfs`
--
ALTER TABLE `pdfs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `pengumumen`
--
ALTER TABLE `pengumumen`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `pengunjungs`
--
ALTER TABLE `pengunjungs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `perjanjians`
--
ALTER TABLE `perjanjians`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `pesans`
--
ALTER TABLE `pesans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT untuk tabel `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `terms`
--
ALTER TABLE `terms`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=980;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `dataindeks`
--
ALTER TABLE `dataindeks`
  ADD CONSTRAINT `dataindeks_id_media_foreign` FOREIGN KEY (`id_media`) REFERENCES `media` (`id`) ON DELETE SET NULL;

--
-- Ketidakleluasaan untuk tabel `layanans`
--
ALTER TABLE `layanans`
  ADD CONSTRAINT `layanans_id_media_foreign` FOREIGN KEY (`id_media`) REFERENCES `media` (`id`) ON DELETE SET NULL;

--
-- Ketidakleluasaan untuk tabel `link_terkaits`
--
ALTER TABLE `link_terkaits`
  ADD CONSTRAINT `link_terkaits_id_media_foreign` FOREIGN KEY (`id_media`) REFERENCES `media` (`id`) ON DELETE SET NULL;

--
-- Ketidakleluasaan untuk tabel `menus`
--
ALTER TABLE `menus`
  ADD CONSTRAINT `menus_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
