<?php

namespace App\Exports;

use App\Models\Pengunjung;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class PengunjungExport implements FromView
{
    public function view(): View
    {
        return view('panel.pengunjung.excel', [
            'data' => Pengunjung::all()
        ]);
    }
}
