<?php

namespace App\Exports;

use App\Models\Pengunjung;
use App\Models\Perjanjian;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class PerjanjianExport implements FromView
{
    public function view(): View
    {
        return view('panel.perjanjian.excel', [
            'data' => Perjanjian::all()
        ]);
    }
}
