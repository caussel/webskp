<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Layanan extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $appends = ['media_path'];

    public function Image()
    {
        return $this->belongsTo(Media::class,'id_media');
    }

    public function media(){
        return $this->belongsTo(Media::class,'id_media','id');
    }


    public function getMediaPathAttribute()
    {
        return asset("/files/" . $this->Image->path);
    }
}
