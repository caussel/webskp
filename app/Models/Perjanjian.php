<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Perjanjian extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $appends = ['status_name'];

    public function getStatusNameAttribute()
    {
        if($this->status == 0){
            return 'Belum Dikonfirmasi';
        }else{
            return 'Sudah Dikonfirmasi';
        }
    }
}
