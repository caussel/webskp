<?php

namespace App\Http\Controllers\Admin;

use App\Exports\PengunjungExport;
use App\Http\Controllers\Controller;
use App\Models\Pengunjung;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
class PengunjungController extends Controller
{
    public function index()
    {
        $getModul = Pengunjung::paginate(10)->onEachSide(2);
        return view('panel.pengunjung.index',['data'=>$getModul]);
    }

    public function edit($id)
    {
        $getModul = Pengunjung::find($id);
        return view('panel.pengunjung.edit',['data'=>$getModul]);
    }

    public function destroy($id)
    {
        $modul = Pengunjung::find($id);
        if ($modul->delete()) {
            return redirect()->back()->with('message', 'Pengunjung telah dihapus');
        }
    }

    public function update(Request $request,$id)
    {
        $getModul = Pengunjung::where('id',$id)->update([
            'komentar' => $request->komentar,
        ]);
        return redirect()->route('pengunjung.index')->with('message','Berhasil mengupdate Pengunjung');
    }

    public function pdf()
    {
        $getModul = Pengunjung::all();
        return view('panel.pengunjung.pdf',['data'=>$getModul]);
    }

    public function excel()
    {
        return Excel::download(new PengunjungExport, 'rekap_pengunjung.xlsx');
    }
}
