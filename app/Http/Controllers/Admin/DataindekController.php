<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Dataindek;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
class DataindekController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

     
    public function index()
    {
        $getModul = Dataindek::paginate(10)->onEachSide(2);
        return view('panel.dataindek.index',['data'=>$getModul]);
    }

   
    public function create()
    {
        return view('panel.dataindek.create');
    }

    public function store(Request $request)
    {
        $data = $request->except('_token');
        Dataindek::create($data);
        return redirect()->route('dataindek.index')->with('message', 'Data Indeks telah ditambah');
    }

    public function edit($id)
    {
        $getModul = Dataindek::find($id);
        return view('panel.dataindek.edit',['data'=>$getModul]);
    }

    public function update(Request $request,$id)
    {
        $data = $request->except('_token','_method');
        Dataindek::where('id',$id)->update($data);
        return redirect()->route('dataindek.index')->with('message', 'Data Indeks telah diupdate');
    }

    public function destroy($id)
    {
        $modul = Dataindek::find($id);
        if ($modul->delete()) {
            return redirect()->back()->with('message', 'Data Indeks telah dihapus');
        }
    }
}
