<?php

namespace App\Http\Controllers\Admin;

use App\Exports\PerjanjianExport;
use App\Http\Controllers\Controller;
use App\Models\Pengunjung;
use App\Models\Perjanjian;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
class PerjanjianController extends Controller
{
    public function index()
    {
        $getModul = Perjanjian::paginate(10)->onEachSide(2);
        return view('panel.perjanjian.index',['data'=>$getModul]);
    }

    public function edit($id)
    {
        $getModul = Perjanjian::find($id);
        return view('panel.perjanjian.edit',['data'=>$getModul]);
    }

    public function update(Request $request,$id)
    {
        $getModul = Perjanjian::where('id',$id)->update([
            'komentar' => $request->komentar,
            'status' => $request->status
        ]);
        return redirect()->route('perjanjian.index')->with('message','Berhasil mengupdate perjanjian');
    }

    public function pdf()
    {
        $getModul = Perjanjian::all();
        return view('panel.perjanjian.pdf',['data'=>$getModul]);
    }

    public function excel()
    {
        return Excel::download(new PerjanjianExport, 'rekap_perjanjian.xlsx');
    }

    public function destroy($id)
    {
        $modul = Perjanjian::find($id);
        if ($modul->delete()) {
            return redirect()->back()->with('message', 'Perjanjian telah dihapus');
        }
    }
}
