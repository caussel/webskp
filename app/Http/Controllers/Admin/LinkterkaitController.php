<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\LinkTerkait;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
class LinkterkaitController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

     
    public function index()
    {
        $getModul = LinkTerkait::paginate(10)->onEachSide(2);
        return view('panel.link_terkait.index',['data'=>$getModul]);
    }

   
    public function create()
    {
        return view('panel.link_terkait.create');
    }

    public function store(Request $request)
    {
        $data = $request->except('_token');
        LinkTerkait::create($data);
        return redirect()->route('linkterkait.index')->with('message', 'Link Terkait telah ditambah');
    }

    public function edit($id)
    {
        $getModul = LinkTerkait::find($id);
        return view('panel.link_terkait.edit',['data'=>$getModul]);
    }

    public function update(Request $request,$id)
    {
        $data = $request->except('_token','_method');
        LinkTerkait::where('id',$id)->update($data);
        return redirect()->route('linkterkait.index')->with('message', 'Link Terkait telah diupdate');
    }

    public function destroy($id)
    {
        $modul = LinkTerkait::find($id);
        if ($modul->delete()) {
            return redirect()->back()->with('message', 'Link Terkait telah dihapus');
        }
    }
}
