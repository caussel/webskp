<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
class FaqController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

   
    public function index()
    {
        $getModul = Faq::paginate(10)->onEachSide(2);
        return view('panel.faq.index',['data'=>$getModul]);
    }

    public function create()
    {
        return view('panel.faq.create');
    }

    public function store(Request $request)
    {
        $data = $request->except('_token');
        $data['slug'] = Str::slug($data['title']);
        Faq::create($data);
        return redirect()->route('faq.index')->with('message', 'Faq telah ditambah');
    }

    public function edit($id)
    {
        $getModul = Faq::find($id);
        return view('panel.faq.edit',['data'=>$getModul]);
    }

 
    public function update(Request $request, $id)
    {
        $data = $request->except('_token','_method');
        $data['slug'] = Str::slug($data['title']);
        Faq::where('id',$id)->update($data);
        return redirect()->route('faq.index')->with('message', 'Faq telah diupdate');
    }
 
    public function destroy($id)
    {
        $modul = Faq::find($id);
        if ($modul->delete()) {
            return redirect()->back()->with('message', 'Faq telah dihapus');
        }
    }
}
