<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Layanan;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
class LayananController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

     
    public function index()
    {
        $getModul = Layanan::paginate(10)->onEachSide(2);
        return view('panel.layanan.index',['data'=>$getModul]);
    }

   
    public function create()
    {
        return view('panel.layanan.create');
    }

    public function store(Request $request)
    {
        $data = $request->except('_token');
        Layanan::create($data);
        return redirect()->route('layanan.index')->with('message', 'Layanan telah ditambah');
    }

    public function edit($id)
    {
        $getModul = Layanan::find($id);
        return view('panel.layanan.edit',['data'=>$getModul]);
    }

    public function update(Request $request,$id)
    {
        $data = $request->except('_token','_method');
        Layanan::where('id',$id)->update($data);
        return redirect()->route('layanan.index')->with('message', 'Layanan telah diupdate');
    }

    public function destroy($id)
    {
        $modul = Layanan::find($id);
        if ($modul->delete()) {
            return redirect()->back()->with('message', 'Layanan telah dihapus');
        }
    }
}
