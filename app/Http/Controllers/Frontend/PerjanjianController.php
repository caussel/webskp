<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Perjanjian;
use Illuminate\Http\Request;

class PerjanjianController extends Controller
{
    public function index()
    {
        return view('home.bukutamu.perjanjian');
    }

    public function store(Request $request)
    {
        $data = $request->except('_token','kartu_identitas');
        $media = $request->file('kartu_identitas');
        $ext = $media->guessClientExtension();
        $mimeType = $media->getClientMimeType();
        $newdate = date("YmdHis");
        $origName = preg_replace('/\\.[^.\\s]{3,4}$/', '', $media->getClientOriginalName());
        $origName = str_replace(" ","-",$origName);
        $data['kartu_identitas'] = $media->storeAs("bukutamu/perjanjian", $origName . "_" . $newdate . "." . $ext, 'uploadfile');
        Perjanjian::create($data);
        return back()->with('message','Berhasil Menyimpan Data');
    }
}
